/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.formulariosintomas.listeners;

/**
 *
 * @author jsuspes
 */
public class ContextoAplicacionIceberg {

    public static final ContextoAplicacionIceberg contextoAplicacion = new ContextoAplicacionIceberg();
    private String rutaContexto = "";

    private ContextoAplicacionIceberg() {
    }

    public static ContextoAplicacionIceberg getInstance() {
        return contextoAplicacion;
    }

    public String getRutaContexto() {
        return rutaContexto;
    }

    public void setRutaContexto(String rutaContexto) {
        this.rutaContexto = rutaContexto;
    }
}
