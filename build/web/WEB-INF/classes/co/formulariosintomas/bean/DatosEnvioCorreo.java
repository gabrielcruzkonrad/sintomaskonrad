/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.formulariosintomas.bean;

/**
 *
 * @author cesar.chacon
 */
public class DatosEnvioCorreo {

    private String idEstudiante;
    private String idEstado;
    private String periodoIngreso;
    private String estado;
    private String nombre;
    private String docEst;
    private String nombrePrograma;
    private int cupo;
    private String directorPrograma;
    private String email;
    private String carta;
    private String fecha;
    private DatosCuerpoCorreo cuerpoCorreo;

    public String getIdEstudiante() {
        return idEstudiante;
    }

    public void setIdEstudiante(String idEstudiante) {
        this.idEstudiante = idEstudiante;
    }

    public String getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(String idEstado) {
        this.idEstado = idEstado;
    }

    public String getPeriodoIngreso() {
        return periodoIngreso;
    }

    public void setPeriodoIngreso(String periodoIngreso) {
        this.periodoIngreso = periodoIngreso;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDocEst() {
        return docEst;
    }

    public void setDocEst(String docEst) {
        this.docEst = docEst;
    }

    public String getNombrePrograma() {
        return nombrePrograma;
    }

    public void setNombrePrograma(String nombrePrograma) {
        this.nombrePrograma = nombrePrograma;
    }

    public int getCupo() {
        return cupo;
    }

    public void setCupo(int cupo) {
        this.cupo = cupo;
    }

    public String getDirectorPrograma() {
        return directorPrograma;
    }

    public void setDirectorPrograma(String directorPrograma) {
        this.directorPrograma = directorPrograma;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCarta() {
        return carta;
    }

    public void setCarta(String carta) {
        this.carta = carta;
    }

    public DatosCuerpoCorreo getCuerpoCorreo() {
        return cuerpoCorreo;
    }

    public void setCuerpoCorreo(DatosCuerpoCorreo cuerpoCorreo) {
        this.cuerpoCorreo = cuerpoCorreo;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

}
