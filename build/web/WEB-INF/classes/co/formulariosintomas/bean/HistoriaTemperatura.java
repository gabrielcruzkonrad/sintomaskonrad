/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.formulariosintomas.bean;

import java.util.Date;

/**
 *
 * @author David
 */
public class HistoriaTemperatura {

    private Long idHistoriaTemperatura;
    private Respuesta idRespuesta;
    private Double temperatura;
    private Date fechacreacion;
    private String temperaturaExcel;
    
    //para tabla
    private String strDate;

    public HistoriaTemperatura() {
        this.fechacreacion = new Date();
        this.idRespuesta = new Respuesta();
        this.temperaturaExcel = "";
    }

    public HistoriaTemperatura(UsuarioFichet usuario, Respuesta respuesta){
        this.idRespuesta = respuesta;
        this.temperatura = Double.parseDouble(usuario.getTemperature());
        this.fechacreacion = usuario.getFechaMarcacion();
    }
    
    public Long getIdHistoriaTemperatura() {
        return idHistoriaTemperatura;
    }

    public void setIdHistoriaTemperatura(Long idHistoriaTemperatura) {
        this.idHistoriaTemperatura = idHistoriaTemperatura;
    }

    public Double getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(Double temperatura) {
        this.temperatura = temperatura;
    }

    public Date getFechacreacion() {
        return fechacreacion;
    }

    public void setFechacreacion(Date fechacreacion) {
        this.fechacreacion = fechacreacion;
    }

    public Respuesta getIdRespuesta() {
        return idRespuesta;
    }

    public void setIdRespuesta(Respuesta idRespuesta) {
        this.idRespuesta = idRespuesta;
    }

    public String getStrDate() {
        return strDate;
    }

    public void setStrDate(String strDate) {
        this.strDate = strDate;
    }

    public String getTemperaturaExcel() {
        return temperaturaExcel;
    }

    public void setTemperaturaExcel(String temperaturaExcel) {
        this.temperaturaExcel = temperaturaExcel;
    }

}
