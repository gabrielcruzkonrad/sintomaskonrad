/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.formulariosintomas.bean;

import java.util.Date;

/**
 *
 * @author David
 */
public class Pregunta {

    private Long idPregunta;
    private String tipoPregunta;
    private String estado;
    private Date fechaCreacion;
    private Date fechaModificacion;
    private String enunciado;
    private String codigo;
    private Encuesta secEncuesta;

    public Pregunta() {
        this.fechaCreacion = new Date();
        this.fechaModificacion = new Date();
        this.secEncuesta = new Encuesta();
    }

    public Long getIdPregunta() {
        return idPregunta;
    }

    public void setIdPregunta(Long idPregunta) {
        this.idPregunta = idPregunta;
    }

    public String getTipoPregunta() {
        return tipoPregunta;
    }

    public void setTipoPregunta(String tipoPregunta) {
        this.tipoPregunta = tipoPregunta;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public String getEnunciado() {
        return enunciado;
    }

    public void setEnunciado(String enunciado) {
        this.enunciado = enunciado;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Encuesta getSecEncuesta() {
        return secEncuesta;
    }

    public void setSecEncuesta(Encuesta secEncuesta) {
        this.secEncuesta = secEncuesta;
    }

}
