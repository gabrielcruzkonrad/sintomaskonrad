/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.formulariosintomas.bean;

import java.util.Date;

/**
 *
 * @author David
 */
public class Usuario {

    private Long idUsuario;
    private String usuario;
    private String email;
    private String contrasena;
    private String estado;
    private Date fechaCreacion;
    private Date fechaModificacion;
    private String identificacion;
    private String tipoUsuario;

    //error
    private String errorUsuario;

    //Manejo de Roles
    private boolean rolUsuario;
    private boolean rolAdministrador;
    private boolean rolConsulta;

    //contrasena
    private String nuevaContrasena;
    private String confirmaContrasena;

    //tipo
    private String tipoidentificacion;
    private TipoIdentificacion tipoIdentificacionC;
    
    public Usuario() {
        this.fechaCreacion = new Date();
        this.fechaModificacion = new Date();
        this.tipoIdentificacionC = new TipoIdentificacion();
    }

    public Usuario(String nombres, String apellidos, String email) {
        this.usuario = nombres + " " + apellidos;
        this.email = email;
    }

    public Long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Date getFecha_modificacion() {
        return fechaModificacion;
    }

    public void setFecha_modificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getErrorUsuario() {
        return errorUsuario;
    }

    public void setErrorUsuario(String errorUsuario) {
        this.errorUsuario = errorUsuario;
    }

    public String getTipoUsuario() {
        return tipoUsuario;
    }

    public void setTipoUsuario(String tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }

    public boolean isRolUsuario() {
        return rolUsuario;
    }

    public void setRolUsuario(boolean rolUsuario) {
        this.rolUsuario = rolUsuario;
    }

    public boolean isRolAdministrador() {
        return rolAdministrador;
    }

    public void setRolAdministrador(boolean rolAdministrador) {
        this.rolAdministrador = rolAdministrador;
    }

    public boolean isRolConsulta() {
        return rolConsulta;
    }

    public void setRolConsulta(boolean rolConsulta) {
        this.rolConsulta = rolConsulta;
    }

    public String getNuevaContrasena() {
        return nuevaContrasena;
    }

    public void setNuevaContrasena(String nuevaContrasena) {
        this.nuevaContrasena = nuevaContrasena;
    }

    public String getConfirmaContrasena() {
        return confirmaContrasena;
    }

    public void setConfirmaContrasena(String confirmaContrasena) {
        this.confirmaContrasena = confirmaContrasena;
    }

    public String getTipoidentificacion() {
        return tipoidentificacion;
    }

    public void setTipoidentificacion(String tipoidentificacion) {
        this.tipoidentificacion = tipoidentificacion;
    }

    public TipoIdentificacion getTipoIdentificacionC() {
        return tipoIdentificacionC;
    }

    public void setTipoIdentificacionC(TipoIdentificacion tipoIdentificacionC) {
        this.tipoIdentificacionC = tipoIdentificacionC;
    }

    
}
