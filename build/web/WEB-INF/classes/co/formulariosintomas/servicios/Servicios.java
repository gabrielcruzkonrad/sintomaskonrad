/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.formulariosintomas.servicios;

import co.formulariosintomas.bean.DetalleRespuesta;
import co.formulariosintomas.bean.EncabezadoRecomendaciones;
import co.formulariosintomas.bean.HistoriaTemperatura;
import co.formulariosintomas.bean.ModeloAlternacia;
import co.formulariosintomas.bean.OpcionPregunta;
import co.formulariosintomas.bean.Parametro;
import co.formulariosintomas.bean.Pregunta;
import co.formulariosintomas.bean.Recomendaciones;
import co.formulariosintomas.bean.Respuesta;
import co.formulariosintomas.bean.RespuestaRecomendacion;
import co.formulariosintomas.bean.Rol;
import co.formulariosintomas.bean.TipoIdentificacion;
import co.formulariosintomas.bean.Usuario;
import co.formulariosintomas.bean.UsuarioRol;
import java.util.Date;
import java.util.List;

/**
 *
 * @author David
 */
public interface Servicios {

    public List<Pregunta> obtenerPregunta();

    public List<OpcionPregunta> obtenerOpcionPregunta();

    public List<Rol> obtenerRol();

    public List<Recomendaciones> obtenerRecomendaciones();

    public List<RespuestaRecomendacion> obtenerRespuestaRecomendacion(String idRespuesta);

    public List<DetalleRespuesta> obtenerDetalleRespuesta(String idRespuesta);

    public List<Parametro> obtenerParametros();

    public void guardarUsuario(UsuarioRol usuario);

    public void guardarUsuarioRol(UsuarioRol usuario);

    public List<Respuesta> guardarRespuestaUsuario(UsuarioRol usuario, String toString, Date fechaIngreso, Respuesta resp);

    public boolean guardarDeatlleRespuesta(DetalleRespuesta detalle);

    public boolean guardarRespuestaRecomendacion(RespuestaRecomendacion respuestaRecomendacion);

    public boolean guardarPersonaExterna(UsuarioRol usuario);

    public Usuario validarPersonaExterna(Usuario usuario);

    public boolean guardarTemperatura(HistoriaTemperatura temperatura);

    public UsuarioRol obtenerUsuario(UsuarioRol usuario);

    public boolean obtenerUsuarioExterno(UsuarioRol usuario);

    public Usuario obtenerUsuarioPorCedula(String identificacion);

    public List<UsuarioRol> obtenerUsuarioRol(UsuarioRol usuario);

    public List<Respuesta> obtenerRespuestaUsuario(UsuarioRol usuario);

    public List<Respuesta> obtenerRespuestaUsuarioAll();

    public List<UsuarioRol> obtenerUsuarioRolporId(UsuarioRol usuario);
    
    public List<Respuesta> obtenerRespuestaUsuarioByDocumento(String documento);
    
    public List<UsuarioRol> obtenerUsuarioRolAll();
    
    public List<HistoriaTemperatura> obtenerTemperaturaByDocumento(String doc);
    
    public List<HistoriaTemperatura> obtenerTemperaturaByFecha(HistoriaTemperatura his);
    
    public boolean insertarRolPersona(UsuarioRol usuario);

    public boolean actualizarRol(Long idUsuario, String metodo);
    
    public List<Respuesta> obtenerUsuarioAll();
    
    public List<RespuestaRecomendacion> obtenerEstadoRiesgo(String idRespuesta);
    
    public ModeloAlternacia obtenerEstudiantesModeloAlternacia(String email);
    
    public List<Respuesta> obtenerTemperaturaAll(Respuesta r);
    
    public List<Respuesta> obtenerTemperaturaAllMenu();
    
    public boolean cambiarContraseña(Usuario contrasena);
    
    public List<RespuestaRecomendacion> obtenerPersonasDia();
    
    public List<TipoIdentificacion> obtenerTipoIdentifacion();
    
    public boolean actualizarProcesoActivacion(Respuesta respuesta);
    
    public List<RespuestaRecomendacion> obtenerPersonasDiaActivar();
    
    public List<OpcionPregunta> obtenerOpcionPreguntaList();
    
    public List<EncabezadoRecomendaciones> obtenerEncabezadoRecomendaciones();
    
    public List<Recomendaciones> obtenerRecomendacionesPorIdEncabezado(String idEncabezado);
}
