/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.formulariosintomas.mb;

import co.formulariosintomas.auth.AuthHelper;
import co.formulariosintomas.bean.DetalleRespuesta;
import co.formulariosintomas.bean.EncabezadoRecomendaciones;
import co.formulariosintomas.bean.HistoriaTemperatura;
import co.formulariosintomas.bean.MenuView;
import co.formulariosintomas.bean.ModeloAlternacia;
import co.formulariosintomas.bean.OpcionPregunta;
import co.formulariosintomas.bean.Parametro;
import co.formulariosintomas.bean.Pregunta;
import co.formulariosintomas.bean.Recomendaciones;
import co.formulariosintomas.bean.Respuesta;
import co.formulariosintomas.bean.RespuestaRecomendacion;
import co.formulariosintomas.bean.Rol;
import co.formulariosintomas.bean.TipoIdentificacion;
import co.formulariosintomas.bean.Usuario;
import co.formulariosintomas.bean.UsuarioRol;
import co.formulariosintomas.constantes.Constantes;
import co.formulariosintomas.constantes.ErroresComunes;
import co.formulariosintomas.servicios.Servicios;
import co.formulariosintomas.servicios.ServiciosImplementacion;
import co.formulariosintomas.utils.Singleton;
import co.formulariosintomas.utils.Utilidades;
import com.microsoft.aad.adal4j.AuthenticationContext;
import com.microsoft.aad.adal4j.AuthenticationResult;
import com.microsoft.aad.adal4j.ClientCredential;
import com.nimbusds.oauth2.sdk.AuthorizationCode;
import com.nimbusds.openid.connect.sdk.AuthenticationErrorResponse;
import com.nimbusds.openid.connect.sdk.AuthenticationResponse;
import com.nimbusds.openid.connect.sdk.AuthenticationResponseParser;
import com.nimbusds.openid.connect.sdk.AuthenticationSuccessResponse;
import java.io.IOException;
import java.net.URI;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Level;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.naming.ServiceUnavailableException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

/**
 *
 * @author David Chacon
 */
@ManagedBean
@ViewScoped
@WebServlet(name = "FormularioSintomasMB")
@Controller
public class FormularioSintomasMB {

    /**
     * Creates a new instance of FormularioSintomasMB
     */
    private final static Logger log = Logger.getLogger(FormularioSintomasMB.class);
    private MenuView menu;

    //render
    private boolean menuRendered;
    private boolean formularioRendered;
    private boolean formularioAdministradorRendered;
    private boolean botonRegresar;
    private boolean formularioRegistroEnviados;
    private boolean consultarRegistros;
    private boolean botonGuardarFormulario;
    private boolean formularioRecomendacionRendered;
    private boolean recomendacionesRiesgo;
    private boolean recomendacionesNormal;
    private boolean formularioTablaIndividualRendered;
    private boolean loginTipoUsuarioRendered;
    private boolean habilitarLabelCedulaRendered;
    private Date fechaRegistro;
    private String colorRiesgo;
    private String tipoUsuario;
    private boolean externo;
    private String nombreTablaIndividual;
    private boolean pintarTituloformulario;
    private boolean menuAdministrador;
    private boolean menuUsuario;
    private boolean menuConsulta;
    private boolean botonAdministrador;
    private boolean botonUsuario;
    private boolean botonConsulta;
    private boolean tablaMenuPrincipalUsuario;
    private boolean tablaMenuPrincipalAdministardor;
    private String nombreEncabezado;
    private Date fechaIngresoUsuario;
    private Date fechaMinDate;
    private Date fechaMaxDate;
    private boolean renderedNombreRecomendaciones;
    private boolean renderedRecomendacionesCodigo;
    private String fechatexto;
    private boolean autorizo;
    private String diaObtenido;
    //boolean menu administrador
    private boolean menuAdministradorOpcionUno;
    private boolean menuAdministradorOpcionDos;
    private boolean variosRoles;

    //Principales
    private Usuario usuario;
    private Usuario usuarioTemp;
    private Servicios servicios;
    private UsuarioRol usuarioRol;
    private UsuarioRol usuarioRolTemp;
    private DetalleRespuesta detalleRespuesta;
    private RespuestaRecomendacion respuestaRecomendacion;

    //Listados
    private List<Pregunta> ListPreguntas;
    private List<Rol> ListRoles;
    private List<OpcionPregunta> ListOpcionPregunta;
    private List<OpcionPregunta> ListOpcionPintar; // lo que se muestra al usuario
    private List<Recomendaciones> ListOpcionPintarRecomendaciones; // lo que se muestra al usuario
    private List<EncabezadoRecomendaciones> listEncabezadoRecomendaciones;
    private List<Respuesta> ListRespuestaUsuario;
    private List<Respuesta> ListHistoriaTemperatura;
    private List<Recomendaciones> ListRecomendacion;
    private List<RespuestaRecomendacion> listRespuestaRecomendacion;
    private List<DetalleRespuesta> listDetalleRespuesta;
    private List<DetalleRespuesta> listDetalleRespuestaTemp;
    private List<Long> pintarPreguntar;
    private List<UsuarioRol> listUsuarioRol;
    private List<UsuarioRol> listUsuarioRolTempAdministrador;
    private List<Parametro> parametros;
    private HashMap<String, Object> parametrosDB;

    //tabla
    private List<Respuesta> listTablaResultados;
    private List<Respuesta> listTablaResultadosFiltro;
    private List<Respuesta> listTablaResultadosMenu;
    private List<Respuesta> listTablaResultadosFiltroMenu;
    private List<Respuesta> listTablaResultadoAdministrador;
    private List<Respuesta> listTablaResultadoAdministradorFiltro;

    //Constnates
    private String cedula;
    private static final String tiempo = "TIEMPO";
    private String adm = Constantes.TIPOADMINISTRADOR;
    private static final int dias = 1;
    private HashMap<String, String> datosPersonas;
    private String segundoCorreo;
    //respuesta
    private HashMap<String, Object> respuestasObtenidas;
    private Object respuestaValor;
    private String[] respuestaMultiple;
    private List<String> pintarRespuestaMultiple;
//    HttpSession session;
    HttpServletRequest request;
    WebService webService;

    //menu Seguridad
    private boolean menuSeguridadRendered;
    private String labelNumeroDocumentoTabla;
    private boolean tablaBuscarNumeroDocumento;
    private boolean tablaSeguridad;
    private int columnasLoginTipoUsuario;
    ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();

    //Tabla manejo de roles
    private boolean menuManejoRolRedered;
    private List<UsuarioRol> listTablaManejoRol;
    private List<UsuarioRol> listTablaManejoRolFiltro;

    //temperatura
    private boolean renderedTemperatura;
    private boolean tablaTemperatura;
    private boolean registroTemperatura;
    private double temperaturaUsuario;
    private List<HistoriaTemperatura> tempRegistroTemperatura;
    private boolean botonRegresarTablaTemperatura;
    private String docTemperatura;
    private List<Respuesta> usuarioTemperatura;
    private List<Respuesta> usuarioTemperaturaFiltro;
    private List<Respuesta> usuarioTemperautratemp;

    private List<Respuesta> listTablaResultadosTemperatura = new ArrayList<>();
    private List<Respuesta> listTablaResultadosTemperaturaFiltro = new ArrayList<>();
    private List<Respuesta> listTablaResultadoTemperaturaAdministrador = new ArrayList<>();
    private List<Respuesta> listTablaResultadoTemperaturaAdministradorFiltro = new ArrayList<>();
    private List<Respuesta> listTablaResultadosTemperaturaFiltroMenu = new ArrayList<>();

    //cmapo validacion calendario
    private boolean renderedCalendarioFormulario;
    private String presencial;
    private String tipoError;

    //Contraseña
    private String nuevaContrasena;
    private String confirmaContrasena;
    private String identificacionContrasena;
    private boolean cambioContrasena;
    private boolean rederedContrasena;
    private boolean rederedActivacion;
    private boolean activarPersonas;
    private List<RespuestaRecomendacion> personas;
    private List<RespuestaRecomendacion> personasTabla;
    private List<RespuestaRecomendacion> personasTablaFiltro;
    private List<RespuestaRecomendacion> personasTablaFiltrotemp;
    private List<TipoIdentificacion> tipoIdentificacion;
    Singleton sessionS;
    private String idp;
    private boolean display;
    private boolean rederedRegistroActivacionuno;
    private boolean rederedRegistroActivaciondos;
    private boolean renderedActivarCambioContrasena;

    private List<Respuesta> ListHistoriaTemperaturaMenuP = new ArrayList<>();
    private List<Respuesta> listTablaResultadosTemperaturaMenuP = new ArrayList<>();
    private List<Respuesta> listTablaResultadosTemperaturaFiltroMenuP = new ArrayList<>();
    private List<Respuesta> listTablaResultadoTemperaturaAdministradorMenuP = new ArrayList<>();
    private List<Respuesta> listTablaResultadoTemperaturaAdministradorFiltroMenuP = new ArrayList<>();
    private List<Respuesta> listTablaResultadosTemperaturaFiltroMenuMenuP = new ArrayList<>();

    private SimpleDateFormat tabla = new SimpleDateFormat(Constantes.FORMATOFECHAHORASEG);
    private SimpleDateFormat tablaI = new SimpleDateFormat(Constantes.FORMATOFECHAREC);
    private Date fechaInicioRegistro;
    private Date fechaFinalRegistro;
    private String fSystem;
    private boolean contenidoDescargar;
    private int[] ordenarPreguntas = {7, 8, 3, 4, 5};
    private boolean otraVinculacion;
    private String guardarOtraVinculacion;

    @PostConstruct
    public void init() {
        try {
            this.IniciarAtributos();
            this.obtenerListados();
            this.variosRoles = false;
//            this.webService.activarPersonasNoche();
//            this.webService.verificacionTemperatura();
            System.setProperty("https.protocols", "TLSv1.2,TLSv1.1,SSLv3");
            System.setProperty("file.encoding", "UTF-8");
            request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
//            this.session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
//            this.session.setMaxInactiveInterval(1800);
            try {
                this.usuarioRol = (UsuarioRol) sessionS.getSession().get(request.getParameter("idp")).get(Constantes.PERSONASESSION);
            } catch (Exception e) {
                this.usuarioRol = null;
            }
            if (this.usuarioRol == null) {
                this.usuarioRol = this.usuarioRol == null ? new UsuarioRol() : this.usuarioRol;
                if (request.getParameter("tipo") != null && request.getParameter("tipo").equalsIgnoreCase(Constantes.TIPOUSUARIO)) {
                    this.usuarioRol.getIdUsuario().setIdUsuario(Long.parseLong(request.getParameter("id")));
                    this.idp = request.getParameter("idp");
                    this.listUsuarioRol = this.servicios.obtenerUsuarioRolporId(this.usuarioRol);
                    this.usuarioRol.setIdUsuario(this.listUsuarioRol.get(0).getIdUsuario());
                    sessionS.getSession().put(this.usuarioRol.getIdUsuario().getIdentificacion(), new HashMap<>());
                    sessionS.getSession().get(this.usuarioRol.getIdUsuario().getIdentificacion()).put(Constantes.EXTERNO, true);
                    sessionS.getSession().get(this.usuarioRol.getIdUsuario().getIdentificacion()).put(Constantes.INICIOPAGINA, false);
                    sessionS.getSession().get(this.usuarioRol.getIdUsuario().getIdentificacion()).put(Constantes.PERSONASESSION, this.usuarioRol);
                    if (!(this.listUsuarioRol.get(0).getIdUsuarioRol() == 0)) {
                        this.cargarMenuUnicoRol(false);
                    } else {
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", this.datosPersonas.get(Constantes.MENSAJE)));
                        ec.redirect(ec.getRequestContextPath() + "/faces/secciones/errorLoginMicrosoft.xhtml");
                    }
                } else if (!AuthHelper.isAuthenticated(request)) {
                    if (AuthHelper.containsAuthenticationData(request)) {
                        log.info("Entre a validar MS");
                        if (processAuthenticationData(request, Constantes.INICIOPRINCIPAL, Constantes.INICIOPRINCIPAL)) {
                            sessionS.getSession().put(this.usuarioRol.getIdUsuario().getIdentificacion(), new HashMap<>());
                            sessionS.getSession().get(this.usuarioRol.getIdUsuario().getIdentificacion()).put(Constantes.EXTERNO, false);
                            sessionS.getSession().get(this.usuarioRol.getIdUsuario().getIdentificacion()).put(Constantes.INICIOPAGINA, false);
                            sessionS.getSession().get(this.usuarioRol.getIdUsuario().getIdentificacion()).put(Constantes.PERSONASESSION, this.usuarioRol);
                            this.cargarMenuUnicoRol(true);
                        } else if (this.tipoError.equalsIgnoreCase(Constantes.SI)) {
                            ec.redirect(ec.getRequestContextPath() + "/faces/vistas/login.xhtml");
                        } else {
                            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", this.datosPersonas.get(Constantes.MENSAJE)));
                            ec.redirect(ec.getRequestContextPath() + "/faces/secciones/errorLoginMicrosoftIden.xhtml");
                        }
                    } else {
                        // not authenticated
                        ec.redirect(ec.getRequestContextPath() + "/faces/vistas/login.xhtml");
                    }
                }
            } else {
                try {
                    if (this.idp != null || this.usuarioRol.getIdUsuario().getIdentificacion() != null) {
                        this.idp = request.getParameter("idp");
                        this.variosRoles = (boolean) sessionS.getSession().get(request.getParameter("idp")).get("variosRoles");
                        this.tipoUsuario = (String) sessionS.getSession().get(request.getParameter("idp")).get(Constantes.TIPOUSUARIOSESSION);
                        this.usuarioRol = (UsuarioRol) sessionS.getSession().get(request.getParameter("idp")).get(Constantes.PERSONASESSION);
                        if (this.tipoUsuario == null) {
                            this.cargarMenuUnicoRol(false);
                        } else {
                            this.habilitarMenuUsuario(this.tipoUsuario);
                        }
//                        if (!sessionS.getListado().containsKey("listadoAdministrador")) {
//                            sessionS.getListado().put("listadoAdministrador", new ArrayList<>());
//                            this.mostrarRegistrosAdministrador();
//                        } else {
//                            this.ListHistoriaTemperatura = this.servicios.obtenerTemperaturaAll();
//                            if (this.sessionS.getListado().get("listadoAdministrador").size() != this.ListHistoriaTemperatura.size()) {
//                                this.mostrarRegistrosAdministrador();
//                            }
//                        }
                    } else {
                        ec.redirect(ec.getRequestContextPath() + "/faces/vistas/login.xhtml");
                    }
                } catch (NullPointerException e) {
                    ec.redirect(ec.getRequestContextPath() + "/faces/vistas/login.xhtml");
                    log.info(ErroresComunes.ERRORLOGIN);
                } catch (Exception ex) {
                    java.util.logging.Logger.getLogger(FormularioSintomasMB.class.getName()).log(Level.SEVERE, null, ex);
                }

            }

        } catch (IOException | NumberFormatException ex) {
            try {
                log.info("Error al inicio FormularioSintomasMB 264 " + ex.getMessage());
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", "Error inesperado Comuniquese a la Universidad."));
                ec.redirect(ec.getRequestContextPath() + "/faces/secciones/errorLoginMicrosoft.xhtml");
            } catch (IOException ex1) {
                log.info(ErroresComunes.ERRORLOGININDEFINIDO + ex1.getMessage());
            }
        }
    }

    private void IniciarAtributos() {
        try {
            sessionS = Singleton.getConfigurador();
            servicios = new ServiciosImplementacion();
            webService = new WebService();
            datosPersonas = new HashMap<>();
            personas = new ArrayList<>();
            personasTabla = new ArrayList<>();
            usuarioRol = new UsuarioRol();
            usuarioTemp = new Usuario();
            display = false;
            detalleRespuesta = new DetalleRespuesta();
            respuestaRecomendacion = new RespuestaRecomendacion();
            listUsuarioRolTempAdministrador = new ArrayList<>();
            listRespuestaRecomendacion = new ArrayList<>();
            ListRespuestaUsuario = new ArrayList<>();
            personasTablaFiltrotemp = new ArrayList<>();
            ListPreguntas = new ArrayList<>();
            ListRoles = new ArrayList<>();
            parametros = new ArrayList<>();
            ListOpcionPregunta = new ArrayList<>();
            ListRecomendacion = new ArrayList<>();
            listTablaResultados = new ArrayList<>();
            pintarPreguntar = new ArrayList<>();
            pintarPreguntar = new ArrayList<>();
            listTablaResultadosFiltro = new ArrayList<>();
            listTablaResultadosMenu = new ArrayList<>();
            listTablaResultadosFiltroMenu = new ArrayList<>();
            listTablaManejoRol = new ArrayList<>();
            listTablaManejoRolFiltro = new ArrayList<>();
            respuestasObtenidas = new HashMap<>();
            parametrosDB = new HashMap<>();
            respuestaValor = new Object();
            formularioRendered = false;
            externo = false;
            pintarTituloformulario = true;
            botonRegresar = false;
            botonGuardarFormulario = false;
            formularioRegistroEnviados = false;
            consultarRegistros = false;
            formularioRecomendacionRendered = false;
            recomendacionesNormal = false;
            recomendacionesRiesgo = false;
            contenidoDescargar = false;
            loginTipoUsuarioRendered = false;
            habilitarLabelCedulaRendered = false;
            menuAdministrador = false;
            menuUsuario = false;
            fechaMaxDate = new Date();
            fechaMinDate = new Date();
            tablaMenuPrincipalAdministardor = false;
            tablaMenuPrincipalUsuario = false;
            menuAdministradorOpcionUno = false;
            menuAdministradorOpcionDos = false;
            renderedActivarCambioContrasena = false;
            renderedNombreRecomendaciones = false;
            listTablaResultadoAdministrador = new ArrayList<>();
            listTablaResultadoAdministradorFiltro = new ArrayList<>();
            renderedRecomendacionesCodigo = false;

            //menu seguridad
            menuSeguridadRendered = true;
            labelNumeroDocumentoTabla = "";
            tablaBuscarNumeroDocumento = false;
            tablaSeguridad = false;
            menuConsulta = false;
            botonAdministrador = false;
            botonUsuario = false;
            botonConsulta = false;
            columnasLoginTipoUsuario = 0;

            //manejo tabla rol
            menuManejoRolRedered = false;
            segundoCorreo = "";

            //temperatura
            renderedTemperatura = false;
            tablaTemperatura = false;
            registroTemperatura = false;
            botonRegresarTablaTemperatura = false;
            ListHistoriaTemperatura = new ArrayList<>();

            //validacion calendario
            renderedCalendarioFormulario = true;
            tipoError = "";

            //Contrasena
            cambioContrasena = false;
            rederedContrasena = false;
            tipoIdentificacion = new ArrayList<>();
            personasTablaFiltro = new ArrayList<>();
            rederedActivacion = false;
            activarPersonas = false;
            rederedRegistroActivacionuno = true;
            rederedRegistroActivaciondos = false;
            fSystem = tablaI.format(new Date());
            fechaInicioRegistro = tabla.parse(fSystem + " 00:00:00");
            fechaFinalRegistro = tabla.parse(fSystem + " 23:59:00");
            identificacionContrasena = new String();
            otraVinculacion = false;
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", ErroresComunes.ERRORINDEFINDIO));
            log.info(ErroresComunes.ERRORINDEFINDIO);
        }

    }

    private void obtenerListados() {
        try {
            ListPreguntas = servicios.obtenerPregunta();
            ListRoles = servicios.obtenerRol();
            ListOpcionPregunta = servicios.obtenerOpcionPregunta();
            parametros = servicios.obtenerParametros();
            tipoIdentificacion = servicios.obtenerTipoIdentifacion();
            if (!sessionS.getListado().containsKey("listadoAdministradorMenu")) {
                sessionS.getListado().put("listadoAdministradorMenu", new ArrayList<>());
                this.mostrarRegistrosAdministradorMenu();
            }
            this.cargarParametrosDB();
            this.cargarFechas();
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", ErroresComunes.ERRORINDEFINDIO));
            log.info(ErroresComunes.ERRORINDEFINDIO);
        }

    }

    private void cargarMenuUnicoRol(boolean entre) {
        try {
            if (this.listUsuarioRol == null) {
                this.listUsuarioRol = this.servicios.obtenerUsuarioRol(this.usuarioRol);
            }
            if (this.listUsuarioRol.size() > 1) {
                this.variosRoles = true;
                this.listUsuarioRol.forEach((UsuarioRol tmp) -> {
                    switch (tmp.getIdRol().getRol()) {
                        case Constantes.TIPOADMINISTRADOR: {
                            this.botonAdministrador = true;
                            this.columnasLoginTipoUsuario++;
                            break;
                        }
                        case Constantes.TIPOUSUARIO: {
                            this.botonUsuario = true;
                            this.columnasLoginTipoUsuario++;
                            break;
                        }
                        case Constantes.TIPOCONSULTA: {
                            this.botonConsulta = true;
                            this.columnasLoginTipoUsuario++;
                            break;
                        }
                    }
                });
                this.habilitarLoginTipoUsuario();
            } else {
                if (this.listUsuarioRol.get(0).getIdRol().getConsulta().equalsIgnoreCase("S") && this.listUsuarioRol.get(0).getIdRol().getAdministrador().equalsIgnoreCase("S")) {
                    this.tipoUsuario = Constantes.TIPOADMINISTRADOR;
                    this.menuAdministrador = true;
                    this.tablaMenuPrincipalAdministardor = true;
                    this.mostrarRegistrosAdministradorMenu();
                    this.listPersonasPActivar();
                    this.obtenerRolesPersona();
                } else if (this.listUsuarioRol.get(0).getIdRol().getConsulta().equalsIgnoreCase("S")) {
                    this.tipoUsuario = Constantes.TIPOCONSULTA;
                    this.menuConsulta = true;
                    this.tablaMenuPrincipalAdministardor = true;
                    this.mostrarRegistrosAdministradorMenu();
                    this.listPersonasPActivar();
                } else if (this.listUsuarioRol.get(0).getIdRol().getAdministrador().equalsIgnoreCase("N") && this.listUsuarioRol.get(0).getIdRol().getConsulta().equalsIgnoreCase("N")) {
                    this.tipoUsuario = Constantes.TIPOUSUARIO;
                    this.menuUsuario = true;
                    this.mostrarRegistro();
                    this.tablaMenuPrincipalUsuario = true;
                }
                this.menuRendered = true;
            }
            sessionS.getSession().get(this.usuarioRol.getIdUsuario().getIdentificacion()).put("variosRoles", this.variosRoles);
            if (!sessionS.getSession().get(this.usuarioRol.getIdUsuario().getIdentificacion()).containsKey(Constantes.PERSONASESSION)) {
                sessionS.getSession().get(this.usuarioRol.getIdUsuario().getIdentificacion()).put(Constantes.PERSONASESSION, this.usuarioRol);
            }
            sessionS.getSession().get(this.usuarioRol.getIdUsuario().getIdentificacion()).put(Constantes.TIPOUSUARIOSESSION, this.tipoUsuario);
            if (entre) {
                this.idp = this.usuarioRol.getIdUsuario().getIdentificacion();
                ec.redirect(ec.getRequestContextPath() + "/faces/vistas/formularioSintomas.xhtml?idp=" + this.usuarioRol.getIdUsuario().getIdentificacion());
            }
        } catch (IOException e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", ErroresComunes.ERRORCARGARMENU));
            log.info(ErroresComunes.ERRORCARGARMENU);
        }

    }

    private void cargarParametrosDB() {
        for (int i = 0; i < parametros.size(); i++) {
            this.parametrosDB.put(parametros.get(i).getParametro(), parametros.get(i).getValor());
        }
    }

    private void cargarFechas() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());

        //Le cambiamos la hora y minutos
        cal.set(Calendar.HOUR, 24);
        Date tempDate = cal.getTime();
        fechaMaxDate = tempDate;
    }

    private boolean processAuthenticationData(HttpServletRequest httpRequest, String currentUri, String fullUrl) //Constantes.INICIOPRINCIPAL
    {
        try {
            HashMap<String, String> params = new HashMap<>();
            for (String key : httpRequest.getParameterMap().keySet()) {
                params.put(key, httpRequest.getParameterMap().get(key)[0]);
            }

            AuthenticationResponse authResponse = AuthenticationResponseParser.parse(new URI(fullUrl), params);
            if (AuthHelper.isAuthenticationSuccessful(authResponse)) {
                log.info("Obtener Datos personas");
                AuthenticationSuccessResponse oidcResponse = (AuthenticationSuccessResponse) authResponse;
                // validate that OIDC Auth Response matches Code Flow (contains only requested artifacts)

                AuthenticationResult authData = getAccessToken(oidcResponse.getAuthorizationCode(), currentUri);
                usuario = new Usuario(authData.getUserInfo().getGivenName(), authData.getUserInfo().getFamilyName(),
                        authData.getUserInfo().getDisplayableId());
                this.usuarioRol.setIdUsuario(usuario);
                this.listUsuarioRol = new ArrayList<>();
                this.listUsuarioRol = this.servicios.obtenerUsuarioRol(this.usuarioRol);
                if (this.listUsuarioRol.isEmpty()) {
                    this.datosPersonas = this.webService.ObtenerDatosPersonaServicio(usuario.getEmail());
                    if (this.datosPersonas.containsKey(Constantes.IDENTIFICACION)) {
                        Usuario temp = servicios.obtenerUsuarioPorCedula(this.datosPersonas.get(Constantes.IDENTIFICACION));
                        if (temp.getIdUsuario() != 0L) {
                            this.segundoCorreo = usuario.getEmail();
                            this.usuarioRol.setIdUsuario(temp);
                            this.usuarioRol = this.servicios.obtenerUsuario(usuarioRol);
                            this.listUsuarioRol = this.servicios.obtenerUsuarioRol(usuarioRol);
                        } else {
                            try {
                                this.usuarioRol.setIdUsuario(usuario);
                                this.usuarioRol.setIdRol(ListRoles.get(0));
                                this.usuarioRol.getIdUsuario().setEstado(Constantes.ESTADOACTIVO);
                                this.usuarioRol.setEstado(Constantes.ESTADOACTIVO);
                                this.usuarioRol.getIdUsuario().setIdentificacion(this.datosPersonas.get(Constantes.IDENTIFICACION));
                                this.sacarTipoIdentificacion(this.datosPersonas.get(Constantes.TIPOIDENTIFICACION));
                                this.usuarioRol.getIdUsuario().setTipoUsuario(this.datosPersonas.get(Constantes.TIPOPERSONA));
                                this.servicios.guardarUsuario(this.usuarioRol);
                                this.listUsuarioRol = this.servicios.obtenerUsuarioRol(this.usuarioRol);
                            } catch (Exception e) {
                                ec.redirect(ec.getRequestContextPath() + "/faces/secciones/errorLoginMicrosoftIden.xhtml");
                                log.info(ErroresComunes.ERRORMS + e.getMessage());
                            }
                        }
                    } else {
                        return false;
                    }
                } else {
                    this.usuarioRol = this.servicios.obtenerUsuario(usuarioRol);
                    this.listUsuarioRol = this.servicios.obtenerUsuarioRol(usuarioRol);
                }

            } else {
                AuthenticationErrorResponse oidcResponse = (AuthenticationErrorResponse) authResponse;
                throw new Exception(String.format("Request for auth code failed: %s - %s",
                        oidcResponse.getErrorObject().getCode(),
                        oidcResponse.getErrorObject().getDescription()));
            }
        } catch (Throwable e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", ErroresComunes.ERRORMS));
            log.info(ErroresComunes.ERRORMS + e.getMessage());
            this.tipoError = Constantes.SI;
            return false;
        }

        return true;
    }

    public AuthenticationResult getAccessToken(
            AuthorizationCode authorizationCode, String currentUri)
            throws Throwable {
        String authCode = authorizationCode.getValue();
        ClientCredential credential = new ClientCredential(Constantes.CLIENDID,
                Constantes.CLIENTSECRET);
        AuthenticationContext context;
        AuthenticationResult result = null;
        ExecutorService service = null;
        try {
            service = Executors.newFixedThreadPool(1);
            context = new AuthenticationContext(Constantes.AUTHORITY + Constantes.TENANT + "/", true,
                    service);
            Future<AuthenticationResult> future = context
                    .acquireTokenByAuthorizationCode(authCode, new URI(
                            currentUri), credential, null);
            result = future.get();
        } catch (ExecutionException e) {
            throw e.getCause();
        } finally {
            service.shutdown();
        }

        if (result == null) {
            throw new ServiceUnavailableException("authentication result was null");
        }
        return result;
    }

    private void sacarTipoIdentificacion(String codigo) {
        for (TipoIdentificacion ti : this.tipoIdentificacion) {
            if (ti.getCodigoEquivalente().equalsIgnoreCase(codigo)) {
                this.usuarioRol.getIdUsuario().setTipoIdentificacionC(ti);
                break;
            }
        }
    }

    // difernetes roles
    public String habilitarLoginTipoUsuario() {
        this.loginTipoUsuarioRendered = true;
        return "#";
    }

    public void display() {
        this.display = true;

        RequestContext.getCurrentInstance().update(":frmFormulario");
    }

    //dirijir al usuario al menu
    public String habilitarMenuUsuario(String tipoUsuario) {
        try {
            this.loginTipoUsuarioRendered = false;
            this.columnasLoginTipoUsuario = 0;
            if (tipoUsuario.equalsIgnoreCase(Constantes.TIPOCONSULTA)) {
                this.tipoUsuario = Constantes.TIPOCONSULTA;
                this.menuConsulta = true;
                this.mostrarRegistrosAdministradorMenu();
                this.listPersonasPActivar();
                this.tablaMenuPrincipalAdministardor = true;
            } else if (tipoUsuario.equalsIgnoreCase(Constantes.TIPOADMINISTRADOR)) {
                this.tipoUsuario = Constantes.TIPOADMINISTRADOR;
                this.menuAdministrador = true;
                this.mostrarRegistrosAdministradorMenu();
                this.listPersonasPActivar();
                this.obtenerRolesPersona();
                this.tablaMenuPrincipalAdministardor = true;
            } else if (tipoUsuario.equalsIgnoreCase(Constantes.TIPOUSUARIO)) {
                this.tipoUsuario = Constantes.TIPOUSUARIO;
                this.menuUsuario = true;
                this.mostrarRegistro();
                this.tablaMenuPrincipalUsuario = true;
            }
            sessionS.getSession().get(this.idp).remove(Constantes.TIPOUSUARIOSESSION, this.tipoUsuario);
            sessionS.getSession().get(this.idp).put(Constantes.TIPOUSUARIOSESSION, this.tipoUsuario);
            this.usuarioRol = (UsuarioRol) sessionS.getSession().get(this.idp).get(Constantes.PERSONASESSION);
            this.menuRendered = true;
            this.display = false;
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", ErroresComunes.ERRORCARGARMENU));
            log.info(ErroresComunes.ERRORCARGARMENU + e.getMessage());
        }

        return "#";
    }

    public String habilitarFormularioCovid() {
        this.formularioRendered = true;
        this.botonGuardarFormulario = true;
        this.botonRegresar = true;
        this.menuRendered = false;
        this.tablaMenuPrincipalAdministardor = false;
        this.tablaMenuPrincipalUsuario = false;
        //para habiliatr calendario
        this.renderedCalendarioFormulario = false;
        this.nombreEncabezado = "";
        this.organizarPreguntasCovid();
        this.ObtenerTituloEncabezadoCovid();
        return "#";
    }

    public void organizarPreguntasCovid() {
        List<Pregunta> tempList = new ArrayList<>();
        String numeracion;
        int count = 0;
        boolean finalizar = true;
        while (finalizar) {
            for (int i = 0; this.ListPreguntas.size() > i; i++) {
                if (ordenarPreguntas[count] == this.ListPreguntas.get(i).getIdPregunta().intValue()) {
                    numeracion = String.valueOf(count + 1);
                    numeracion = numeracion + ". " + this.ListPreguntas.get(i).getEnunciado();
                    this.ListPreguntas.get(i).setEnunciado(numeracion);
                    tempList.add(this.ListPreguntas.get(i));
                    count++;
                }
                if (ordenarPreguntas.length == count) {
                    finalizar = false;
                    break;
                }
            }
        }
        if (!finalizar) {
            this.ListPreguntas = tempList;
        }
    }

    public void ObtenerTituloEncabezadoCovid() {
        for (int i = 0; this.ListPreguntas.size() > i; i++) {
            if (this.ListPreguntas.get(i).getSecEncuesta().getSecEncuesta() == 1L) {
                this.nombreEncabezado = this.ListPreguntas.get(i).getSecEncuesta().getIntroduccion();
                break;
            }
        }
    }

    public String volverMenuPrincipal() {
        try {
            fechaInicioRegistro = tabla.parse(fSystem + " 00:00:00");
            fechaFinalRegistro = tabla.parse(fSystem + " 23:59:00");
            this.renderedActivarCambioContrasena = false;
            this.nuevaContrasena = new String();
            this.confirmaContrasena = new String();
            this.identificacionContrasena = new String();
            this.contenidoDescargar = false;
            this.cambioContrasena = false;
            this.rederedActivacion = false;
            this.rederedContrasena = false;
            this.activarPersonas = false;
            this.renderedCalendarioFormulario = false;
            this.renderedTemperatura = false;
            this.tablaTemperatura = false;
            this.registroTemperatura = false;
            this.botonRegresarTablaTemperatura = false;
            this.formularioRendered = false;
            this.renderedNombreRecomendaciones = false;
            this.formularioRecomendacionRendered = false;
            this.loginTipoUsuarioRendered = false;
            this.recomendacionesNormal = false;
            this.recomendacionesRiesgo = false;
            this.formularioRegistroEnviados = false;
            this.botonGuardarFormulario = false;
            this.menuRendered = true;
            this.botonRegresar = false;
            this.consultarRegistros = false;
            this.formularioAdministradorRendered = false;
            this.formularioTablaIndividualRendered = false;
            this.pintarTituloformulario = true;
            this.pintarPreguntar = new ArrayList<>();
            this.menuAdministradorOpcionUno = false;
            this.menuAdministradorOpcionDos = false;
            this.renderedRecomendacionesCodigo = false;
            this.menuManejoRolRedered = false;
            if (tipoUsuario.equalsIgnoreCase(Constantes.TIPOADMINISTRADOR) || tipoUsuario.equalsIgnoreCase(Constantes.TIPOCONSULTA)) {
                this.mostrarRegistrosAdministradorMenu();
//                this.obtenerRolesPersona();
//                this.listPersonasPActivar();
                this.tablaMenuPrincipalAdministardor = true;
            } else {
                this.mostrarRegistro();
                this.tablaMenuPrincipalUsuario = true;
            }
            this.tablaSeguridad = false;
            this.usuarioTemp = new Usuario();
            this.fechaIngresoUsuario = null;
            this.respuestasObtenidas = new HashMap<>();
            this.respuestaValor = new Object();
            this.respuestaMultiple = new String[0];
            this.autorizo = false;
            this.presencial = new String();
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", ErroresComunes.ERRORINDEFINDIO));
            log.info(ErroresComunes.ERRORINDEFINDIO + e.getMessage());
        }

        return "#";
    }

    public String volverTabla() {
        try {
            this.formularioTablaIndividualRendered = false;
            this.renderedRecomendacionesCodigo = false;
            this.botonRegresar = true;
            this.cambioContrasena = false;
            this.rederedContrasena = false;
            this.pintarPreguntar = new ArrayList<>();
            if (tipoUsuario.equalsIgnoreCase(Constantes.TIPOADMINISTRADOR) || tipoUsuario.equalsIgnoreCase(Constantes.TIPOCONSULTA)) {
                this.mostrarRegistrosAdministrador();
//                if (this.menuAdministradorOpcionUno) {
//                    this.menuAdministradorOpcionUno = false;
//                    this.menuAdministradorOpcionDos = true;
//                } else {
//                    this.menuAdministradorOpcionUno = true;
//                    this.menuAdministradorOpcionDos = false;
//                }
                this.consultarRegistros = true;
            } else {
                this.mostrarRegistro();
                this.formularioRegistroEnviados = true;
            }
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", ErroresComunes.ERRORINDEFINDIO));
            log.info(ErroresComunes.ERRORINDEFINDIO + e.getMessage());
        }

        return "#";
    }

    public String habilitarRegistroEnviados() {
        this.formularioRegistroEnviados = true;
        this.tablaMenuPrincipalAdministardor = false;
        this.tablaMenuPrincipalUsuario = false;
        this.botonRegresar = true;
        this.menuRendered = false;
        this.mostrarRegistro();
        return "#";
    }

    public String habilitarRegistroEnviadosAdministrador() {
        this.formularioAdministradorRendered = true;
        //para habiliatr calendario
        this.renderedCalendarioFormulario = false;
        this.habilitarLabelCedulaRendered = false;
        this.botonRegresar = true;
        this.tablaMenuPrincipalAdministardor = false;
        this.tablaMenuPrincipalUsuario = false;
        this.botonGuardarFormulario = true;
        this.menuRendered = false;
        this.tablaMenuPrincipalUsuario = false;
        this.nombreEncabezado = "";
        this.organizarPreguntasCovid();
        this.ObtenerTituloEncabezadoCovid();
        return "#";
    }

    public boolean habilitarLabelCedula() {
        if (this.habilitarLabelCedulaRendered) {
            return false;
        } else {
            this.habilitarLabelCedulaRendered = true;
            return true;
        }
    }

    public void mostrarRegistro() {
        try {
            this.ListRespuestaUsuario = new ArrayList<>();
            this.listTablaResultados = new ArrayList<>();
            this.listTablaResultadosFiltro = new ArrayList<>();
            this.listTablaResultadosMenu = new ArrayList<>();
            this.listTablaResultadosFiltroMenu = new ArrayList<>();
            this.listTablaResultadoAdministrador = new ArrayList<>();
            this.listTablaResultadoAdministradorFiltro = new ArrayList<>();
            this.ListRespuestaUsuario = this.servicios.obtenerRespuestaUsuario(usuarioRol);
            for (int i = 0; ListRespuestaUsuario.size() > i; i++) {
                this.listRespuestaRecomendacion = new ArrayList<>();
                this.listRespuestaRecomendacion = this.servicios.obtenerRespuestaRecomendacion(ListRespuestaUsuario.get(i).getIdRespuesta().toString());
                if (listRespuestaRecomendacion.size() > 0 && !listRespuestaRecomendacion.isEmpty() && listRespuestaRecomendacion.get(0).getIdRecomendacion().getTipo().equalsIgnoreCase("R")) {
                    this.ListRespuestaUsuario.get(i).setColor(Constantes.CODIGOROJO);
                    this.ListRespuestaUsuario.get(i).setValidoRegistro(Constantes.NOVALIDOREGISTRO);
                    this.ListRespuestaUsuario.get(i).setStrDate(this.CambiarFechaTexto(this.ListRespuestaUsuario.get(i).getFehcaCreacion()));
                    this.ListRespuestaUsuario.get(i).setStrDateFin(this.CambiarFechaFinTexto(this.ListRespuestaUsuario.get(i).getFechaFin()));
                    this.listTablaResultados.add(this.ListRespuestaUsuario.get(i));
                } else if (listRespuestaRecomendacion.size() > 0 && !listRespuestaRecomendacion.isEmpty() && listRespuestaRecomendacion.get(0).getIdRecomendacion().getTipo().equalsIgnoreCase("N")) {
                    this.ListRespuestaUsuario.get(i).setColor(Constantes.CODIGOVERDE);
                    this.ListRespuestaUsuario.get(i).setValidoRegistro(this.validarTiempoRegistro(this.ListRespuestaUsuario.get(i).getFechaFin(), this.ListRespuestaUsuario.get(i).getPresencial()));
                    this.ListRespuestaUsuario.get(i).setStrDate(this.CambiarFechaTexto(this.ListRespuestaUsuario.get(i).getFehcaCreacion()));
                    this.ListRespuestaUsuario.get(i).setStrDateFin(this.CambiarFechaFinTexto(this.ListRespuestaUsuario.get(i).getFechaFin()));
                    this.listTablaResultados.add(this.ListRespuestaUsuario.get(i));
                }
            }
            if (this.listTablaResultados.size() > 0) {
                this.listTablaResultadosFiltro = this.listTablaResultados;
                this.listTablaResultadosFiltroMenu = this.listTablaResultados;
                this.listTablaResultadosMenu = this.listTablaResultados;
                this.listTablaResultadoAdministrador = this.listTablaResultados;
                this.listTablaResultadoAdministradorFiltro = this.listTablaResultados;
            } else {
                this.listTablaResultados = new ArrayList<>();
                this.listTablaResultadosFiltro = new ArrayList<>();
                this.listTablaResultadosMenu = new ArrayList<>();
                this.listTablaResultadosFiltroMenu = new ArrayList<>();
                this.listTablaResultadoAdministrador = new ArrayList<>();
            }
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", ErroresComunes.ERRORMOSTRARREGISTRO));
            log.info(ErroresComunes.ERRORMOSTRARREGISTRO + e.getMessage());
        }
    }

    //metodo para cambiar fecha a texto
    private String CambiarFechaTexto(Date fechaCreacion) {
        Respuesta temp = new Respuesta();
        return temp.getFormatter().format(fechaCreacion);
    }

    //metodo para cambiar fechaFin a texto
    private String CambiarFechaFinTexto(Date fechaCreacion) {
        Respuesta temp = new Respuesta();
        return temp.getFormatterFechafin().format(fechaCreacion);
    }

    //metodo para validar registro
    public String validarTiempoRegistro(Date fechaFin, String presencialF) {

        //        Calendar cal = Calendar.getInstance();
//        cal.setTime(new Date());
//        Date temp = cal.getTime();
//        long milliseconds = fechaFin.getTime() - temp.getTime();
////        int seconds = (int) (milliseconds / 1000) % 60;
////        int minutes = (int) ((milliseconds / (1000 * 60)) % 60);
////        int hours = (int) ((milliseconds / (1000 * 60 * 60)) % 24);
//        if (milliseconds >= 0) {
//            return Constantes.VALIDOREGISTRO;
//        } else {
//            return Constantes.NOVALIDOREGISTRO;
//        }
        try {
            if (presencialF.trim().equalsIgnoreCase("N")) {
                return Constantes.NOPRESENCIAL;
            } else {
                SimpleDateFormat sdformat = new SimpleDateFormat(Constantes.FORMATOFECHA);
                String textSystem = sdformat.format(new Date());
                String textIngreso = sdformat.format(fechaFin);
                Date fechaSystem = sdformat.parse(textSystem);
                Date fechaIngreso = sdformat.parse(textIngreso);
                //fecha son iguales
                if (fechaSystem.compareTo(fechaIngreso) == 0) {
                    return Constantes.VALIDOREGISTRO;
                } else if (fechaSystem.compareTo(fechaIngreso) < 0) {
                    // fecha System es antes del ingreso
                    return Constantes.NOVALIDOREGISTRO;
                } else if (fechaSystem.compareTo(fechaIngreso) > 0) {
                    //fecha System supera la del ingreso
                    return Constantes.NOVALIDOREGISTRO;
                }
            }

        } catch (ParseException ex) {
            java.util.logging.Logger.getLogger(FormularioSintomasMB.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Constantes.SINCONOCIMIENTOVALIDO;
    }

    public String habilitarConsultarRegistros() {
        this.consultarRegistros = true;
        this.botonRegresar = true;
        this.menuRendered = false;
        this.tablaMenuPrincipalAdministardor = false;
        this.tablaMenuPrincipalUsuario = false;
//        DataTable dataTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("tbladm");
//        if (dataTable != null) {
//            dataTable.setFilteredValue(listTablaResultadosFiltro);     // reset filter
//            dataTable.setValue(listTablaResultados);
//        }
        return "#";
    }

    public String mostrarRegistrosAdministrador() {
        try {
//            this.ListRespuestaUsuario = new ArrayList<>();
//            this.listTablaResultados = new ArrayList<>();
//            this.listTablaResultadosFiltro = new ArrayList<>();
//            this.listTablaResultadoAdministrador = new ArrayList<>();
//            this.listTablaResultadoAdministradorFiltro = new ArrayList<>();
//            this.listTablaResultadosFiltroMenu = new ArrayList<>();
            //this.ListRespuestaUsuario = this.servicios.obtenerRespuestaUsuarioAll();
            this.ListHistoriaTemperatura = new ArrayList<>();
            this.listTablaResultadosTemperatura = new ArrayList<>();
            this.listTablaResultadosTemperaturaFiltro = new ArrayList<>();
            this.listTablaResultadoTemperaturaAdministrador = new ArrayList<>();
            this.listTablaResultadoTemperaturaAdministradorFiltro = new ArrayList<>();
            this.listTablaResultadosTemperaturaFiltroMenu = new ArrayList<>();
//            if (this.menuAdministradorOpcionUno) {
//                this.menuAdministradorOpcionUno = false;
//                this.menuAdministradorOpcionDos = true;
//            } else {
//                this.menuAdministradorOpcionUno = true;
//                this.menuAdministradorOpcionDos = false;
//            }
            if (this.tipoUsuario.equalsIgnoreCase(Constantes.TIPOADMINISTRADOR)) {
                this.menuAdministradorOpcionUno = true;
            }
            Respuesta r = new Respuesta();
            SimpleDateFormat formatter2 = new SimpleDateFormat(Constantes.FORMATOFECHAHORASEG);
            String in = formatter2.format(fechaInicioRegistro);
            String ou = formatter2.format(fechaFinalRegistro);
            r.setFechaInicialRegistro(in);
            r.setFechaFinalRegistro(ou);
            this.ListHistoriaTemperatura = this.servicios.obtenerTemperaturaAll(r);
            for (int i = 0; ListHistoriaTemperatura.size() > i; i++) {
                this.listRespuestaRecomendacion = new ArrayList<>();
                this.listRespuestaRecomendacion = this.servicios.obtenerRespuestaRecomendacion(this.ListHistoriaTemperatura.get(i).getIdRespuesta().toString());
                if (listRespuestaRecomendacion.size() > 0 && !listRespuestaRecomendacion.isEmpty() && listRespuestaRecomendacion.get(0).getIdRecomendacion().getTipo().equalsIgnoreCase("R")) {
                    this.ListHistoriaTemperatura.get(i).setColor(Constantes.CODIGOROJO);
                    this.ListHistoriaTemperatura.get(i).setValidoRegistro(Constantes.NOVALIDOREGISTRO);
                    this.ListHistoriaTemperatura.get(i).setStrDate(this.CambiarFechaTexto(this.ListHistoriaTemperatura.get(i).getFehcaCreacion()));
                    this.ListHistoriaTemperatura.get(i).setStrDateFin(this.CambiarFechaFinTexto(this.ListHistoriaTemperatura.get(i).getFechaFin()));
                    this.ListHistoriaTemperatura.get(i).setTemperaturaExcel(this.sacarDatoTemperatura(this.ListHistoriaTemperatura.get(i).getIdUsuario().getIdentificacion(), this.ListHistoriaTemperatura.get(i).getFechaFin()));
                    this.listTablaResultadosTemperatura.add(this.ListHistoriaTemperatura.get(i));
                } else if (listRespuestaRecomendacion.size() > 0 && !listRespuestaRecomendacion.isEmpty() && listRespuestaRecomendacion.get(0).getIdRecomendacion().getTipo().equalsIgnoreCase("N")) {
                    this.ListHistoriaTemperatura.get(i).setColor(Constantes.CODIGOVERDE);
                    this.ListHistoriaTemperatura.get(i).setValidoRegistro(this.validarTiempoRegistro(this.ListHistoriaTemperatura.get(i).getFechaFin(), this.ListHistoriaTemperatura.get(i).getPresencial()));
                    this.ListHistoriaTemperatura.get(i).setStrDate(this.CambiarFechaTexto(this.ListHistoriaTemperatura.get(i).getFehcaCreacion()));
                    this.ListHistoriaTemperatura.get(i).setStrDateFin(this.CambiarFechaFinTexto(this.ListHistoriaTemperatura.get(i).getFechaFin()));
                    this.ListHistoriaTemperatura.get(i).setTemperaturaExcel(this.sacarDatoTemperatura(this.ListHistoriaTemperatura.get(i).getIdUsuario().getIdentificacion(), this.ListHistoriaTemperatura.get(i).getFechaFin()));
                    this.listTablaResultadosTemperatura.add(this.ListHistoriaTemperatura.get(i));
                }
            }
            if (this.listTablaResultadosTemperatura.size() > 0) {
                this.listTablaResultadosTemperaturaFiltro = this.listTablaResultadosTemperatura;
                this.listTablaResultadosTemperaturaFiltroMenu = this.listTablaResultadosTemperatura;
//                this.listTablaResultadosMenu = this.listTablaResultadosTemperatura;
                this.listTablaResultadoTemperaturaAdministrador = this.listTablaResultadosTemperatura;
                this.listTablaResultadoTemperaturaAdministradorFiltro = this.listTablaResultadosTemperatura;
            } else {
                this.ListHistoriaTemperatura = new ArrayList<>();
                this.listTablaResultadosTemperatura = new ArrayList<>();
                this.listTablaResultadosTemperaturaFiltro = new ArrayList<>();
                this.listTablaResultadoTemperaturaAdministrador = new ArrayList<>();
                this.listTablaResultadoTemperaturaAdministradorFiltro = new ArrayList<>();
                this.listTablaResultadosTemperaturaFiltroMenu = new ArrayList<>();
            }
            contenidoDescargar = true;
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", ErroresComunes.ERRORMOSTRARREGISTRO));
            log.info(ErroresComunes.ERRORMOSTRARREGISTRO + e.getMessage());
        }
        return "#";
    }

    public String mostrarRegistrosAdministradorMenu() {
        try {
//            this.ListRespuestaUsuario = new ArrayList<>();
//            this.listTablaResultados = new ArrayList<>();
//            this.listTablaResultadosFiltro = new ArrayList<>();
//            this.listTablaResultadoAdministrador = new ArrayList<>();
//            this.listTablaResultadoAdministradorFiltro = new ArrayList<>();
//            this.listTablaResultadosFiltroMenu = new ArrayList<>();
            //this.ListRespuestaUsuario = this.servicios.obtenerRespuestaUsuarioAll();
            ListHistoriaTemperaturaMenuP = new ArrayList<>();
            listTablaResultadosTemperaturaMenuP = new ArrayList<>();
            listTablaResultadosTemperaturaFiltroMenuP = new ArrayList<>();
            listTablaResultadoTemperaturaAdministradorMenuP = new ArrayList<>();
            listTablaResultadoTemperaturaAdministradorFiltroMenuP = new ArrayList<>();
            listTablaResultadosTemperaturaFiltroMenuMenuP = new ArrayList<>();
            this.ListHistoriaTemperaturaMenuP = this.servicios.obtenerTemperaturaAllMenu();
            if (this.sessionS.getListado().get("listadoAdministradorMenu").size() != this.ListHistoriaTemperaturaMenuP.size()) {
                for (int i = 0; ListHistoriaTemperaturaMenuP.size() > i; i++) {
                    this.ListHistoriaTemperaturaMenuP.get(i).setStrDateFin(this.CambiarFechaFinTexto(this.ListHistoriaTemperaturaMenuP.get(i).getFechaFin()));
                    this.ListHistoriaTemperaturaMenuP.get(i).setStrDate(this.CambiarFechaTexto(this.ListHistoriaTemperaturaMenuP.get(i).getFehcaCreacion()));
                }
                if (this.ListHistoriaTemperaturaMenuP.size() > 0) {
                    this.sessionS.getListado().replace("listadoAdministradorMenu", ListHistoriaTemperaturaMenuP);
                    this.listTablaResultadosTemperaturaFiltroMenuP = this.ListHistoriaTemperaturaMenuP;
                    this.listTablaResultadosTemperaturaFiltroMenuMenuP = this.ListHistoriaTemperaturaMenuP;
//                this.listTablaResultadosMenu = this.listTablaResultadosTemperatura;
                    this.listTablaResultadoTemperaturaAdministradorMenuP = this.ListHistoriaTemperaturaMenuP;
                    this.listTablaResultadoTemperaturaAdministradorFiltroMenuP = this.ListHistoriaTemperaturaMenuP;
                } else {
                    this.ListHistoriaTemperaturaMenuP = new ArrayList<>();
                    this.listTablaResultadosTemperaturaMenuP = new ArrayList<>();
                    this.listTablaResultadosTemperaturaFiltroMenuP = new ArrayList<>();
                    this.listTablaResultadoTemperaturaAdministradorMenuP = new ArrayList<>();
                    this.listTablaResultadoTemperaturaAdministradorFiltroMenuP = new ArrayList<>();
                    this.listTablaResultadosTemperaturaFiltroMenuMenuP = new ArrayList<>();
                }
            } else {
                this.listTablaResultadosTemperaturaMenuP = this.sessionS.getListado().get("listadoAdministradorMenu");
                this.listTablaResultadosTemperaturaFiltroMenuP = this.sessionS.getListado().get("listadoAdministradorMenu");
                this.listTablaResultadosTemperaturaFiltroMenuMenuP = this.sessionS.getListado().get("listadoAdministradorMenu");
//                this.listTablaResultadosMenu = this.listTablaResultadosTemperatura;
                this.listTablaResultadoTemperaturaAdministradorMenuP = this.sessionS.getListado().get("listadoAdministradorMenu");;
                this.listTablaResultadoTemperaturaAdministradorFiltroMenuP = this.sessionS.getListado().get("listadoAdministradorMenu");
            }

        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", ErroresComunes.ERRORMOSTRARREGISTRO));
            log.info(ErroresComunes.ERRORMOSTRARREGISTRO + e.getMessage());
        }
        return "#";
    }

    private String sacarDatoTemperatura(String documento, Date fechaTemperatura) {
        String temperatura = "";
        int cont = 0;
        if (fechaTemperatura == null) {
            this.tempRegistroTemperatura = servicios.obtenerTemperaturaByDocumento(documento);
            for (HistoriaTemperatura t : this.tempRegistroTemperatura) {
                if (cont == 0) {
                    temperatura += t.getTemperatura().toString();
                    cont++;
                } else {
                    temperatura += ";" + t.getTemperatura().toString();
                }
            }
        } else {
            HistoriaTemperatura his = new HistoriaTemperatura();
            his.setFechacreacion(fechaTemperatura);
            his.getIdRespuesta().getIdUsuario().setIdentificacion(documento);
            this.tempRegistroTemperatura = servicios.obtenerTemperaturaByFecha(his);
            for (HistoriaTemperatura t : this.tempRegistroTemperatura) {
                if (cont == 0) {
                    temperatura += t.getTemperatura().toString();
                    cont++;
                } else {
                    temperatura += ";" + t.getTemperatura().toString();
                }
            }
        }

        return temperatura;
    }

    public String salir() {
        try {
            this.menuAdministrador = false;
            this.menuUsuario = false;
            ec = FacesContext.getCurrentInstance().getExternalContext();
            this.externo = (boolean) sessionS.getSession().get(this.idp).get(Constantes.EXTERNO);
            sessionS.getSession().remove(this.idp);
            if (this.externo) {
                // salir cuando no es microsoft
                this.usuarioRol = new UsuarioRol();
                ec.redirect(Constantes.SALIRAPLICACIONEXTERNO);
            } else {
                ec.redirect(Constantes.SALIRAPLICACOIN);
            }

        } catch (IOException ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", ErroresComunes.ERRORSALIR));
            log.info(ErroresComunes.ERRORSALIR + ex.getMessage());
        } catch (Exception ex) {
            log.info(ex.getMessage());
        }
        return "#";
    }

    public boolean opcionesFormulario(String preguntaTipo, String render, String idPreguntas) {
        if (preguntaTipo.equalsIgnoreCase(render)) {
            this.opcionesMenuFormulario(idPreguntas);
            return true;
        } else {
            return false;
        }
    }

    public boolean opcionesMenuFormulario(String idPregunta) {
        this.ListOpcionPintar = new ArrayList<>();
        for (int i = 0; ListOpcionPregunta.size() > i; i++) {
            if (idPregunta.equalsIgnoreCase(ListOpcionPregunta.get(i).getIdPregunta().getIdPregunta().toString())) {
                this.ListOpcionPintar.add(ListOpcionPregunta.get(i));
            }
        }
        return this.ListOpcionPintar.size() > 0;
    }

    public String guardarFormulario() {
        this.presencial = renderedCalendarioFormulario ? Constantes.SI : Constantes.NO;
        this.diaObtenido = "";
        if (this.tipoUsuario.equalsIgnoreCase(Constantes.TIPOADMINISTRADOR)) {
            this.usuarioTemp = this.servicios.obtenerUsuarioPorCedula(this.usuarioTemp.getIdentificacion());
            if (this.usuarioTemp.getIdUsuario() == 0L) {
                this.usuarioTemp.setEmail("Ninguno");
            }
        }
        if (presencial.equalsIgnoreCase(Constantes.NO)) {
            this.fechaIngresoUsuario = new Date();
        }
        if (this.fechaIngresoUsuario != null && verificarHorarioEstudiante(this.tipoUsuario.equalsIgnoreCase(Constantes.TIPOUSUARIO) ? usuarioRol.getIdUsuario().getIdentificacion() : this.usuarioTemp.getIdentificacion(), this.tipoUsuario.equalsIgnoreCase(Constantes.TIPOUSUARIO) ? usuarioRol.getIdUsuario().getTipoUsuario() : this.usuarioTemp.getTipoUsuario(), this.tipoUsuario.equalsIgnoreCase(Constantes.TIPOUSUARIO) ? usuarioRol.getIdUsuario().getEmail() : this.usuarioTemp.getEmail(), presencial)) {
            this.ListRespuestaUsuario = new ArrayList<>();
            this.obtenerFechaIngreso();
            if (this.autorizo) {
                Respuesta resp = new Respuesta();
                resp.setAutorizo(Constantes.SI);
                resp.setPresencial(presencial);
                if (this.tipoUsuario.equalsIgnoreCase(Constantes.TIPOUSUARIO)) {
                    if (verificarEncuestaUsuario()) {
                        this.ListRespuestaUsuario = this.servicios.guardarRespuestaUsuario(usuarioRol, parametrosDB.get(tiempo).toString(), this.fechaIngresoUsuario, resp);
                        if (this.ListRespuestaUsuario.size() > 0) {
                            if (this.guardarRespuestas()) {
                                // activar fichet
                                SimpleDateFormat sdformat = new SimpleDateFormat(Constantes.FORMATOFECHA);
                                String textSystem = sdformat.format(new Date());
                                String fechaActivacion = sdformat.format(this.fechaIngresoUsuario);
                                if (recomendacionesNormal == true && fechaActivacion.equalsIgnoreCase(textSystem)) {
                                    if (!webService.activarPersonasEncuesta(this.usuarioRol.getIdUsuario(), this.fechaIngresoUsuario)) {
                                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Información", "Debe registrarse en el sistema biométrico con una foto"));
                                    } else {
                                        if (!this.servicios.actualizarProcesoActivacion(this.ListRespuestaUsuario.get(0))) {
                                            log.info("Error al guardar la activacion de la persona. ");
                                        }
                                    }
                                }
                            } else {
                                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", ErroresComunes.ERRORACTIVARFICHET));
                            }

                        } else {
                            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", ErroresComunes.ERRORPERSONANOREGISTRADA));
                            return "#";
                        }
                    } else {
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", ErroresComunes.ERRORTIENEENCUESTA + this.fechatexto));
                        return "#";
                    }
                } else if (this.tipoUsuario.equalsIgnoreCase(Constantes.TIPOADMINISTRADOR)) {
                    if (this.usuarioTemp.getIdUsuario() != 0L && !this.usuarioTemp.getEmail().equalsIgnoreCase("Ninguno")) {
                        UsuarioRol temp = new UsuarioRol();
                        temp.setIdUsuario(usuarioTemp);
                        if (verificarEncuestaAdministrador(temp)) {
                            this.ListRespuestaUsuario = this.servicios.guardarRespuestaUsuario(temp, parametrosDB.get(tiempo).toString(), this.fechaIngresoUsuario, resp);
                            if (this.ListRespuestaUsuario.size() > 0) {
                                if (this.guardarRespuestas()) {
                                    // activar fichet 
                                    SimpleDateFormat sdformat = new SimpleDateFormat(Constantes.FORMATOFECHA);
                                    String textSystem = sdformat.format(new Date());
                                    String fechaActivacion = sdformat.format(this.fechaIngresoUsuario);
                                    if (recomendacionesNormal == true && fechaActivacion.equalsIgnoreCase(textSystem)) {
                                        if (!webService.activarPersonasEncuesta(temp.getIdUsuario(), this.fechaIngresoUsuario)) {
                                            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Información", "Debe registrarse en el sistema biométrico con una foto"));
                                        } else {
                                            if (!this.servicios.actualizarProcesoActivacion(this.ListRespuestaUsuario.get(0))) {
                                                log.info("Error al guardar la activacion de la persona. ");
                                            }
                                        }
                                    }
                                } else {
                                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", ErroresComunes.ERRORACTIVARFICHET));
                                }
                                this.usuarioRolTemp = new UsuarioRol();
                            } else {
                                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", ErroresComunes.ERRORPERSONANOREGISTRADA));
                                return "#";
                            }
                        } else {
                            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", ErroresComunes.ERRORTIENEENCUESTA + this.fechatexto));
                            return "#";
                        }
                    } else {
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", ErroresComunes.ERRORPERSONANOREGISTRADA));
                        return "#";
                    }
                }
                this.usuarioTemp = new Usuario();
                this.fechaIngresoUsuario = null;
                this.respuestasObtenidas = new HashMap<>();
                this.respuestaValor = new Object();
                this.respuestaMultiple = new String[0];
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Información", ErroresComunes.ERRORAUTORIZACION));
                return "#";
            }
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Información", ErroresComunes.ERRORSINPERMISODIA + (this.diaObtenido.equalsIgnoreCase("Ninguno") ? "No obtenido" : this.diaObtenido)));
            return "#";
        }

        return "#";
    }

    public boolean verificarHorarioEstudiante(String documento, String tipo, String email, String presencialF) {
        try {
            if (presencialF.equalsIgnoreCase(Constantes.NO)) {
                return true;
            } else {
                if (tipo.equalsIgnoreCase(Constantes.TIPOESTUDIANTE)) {
                    this.diaObtenido = this.sacarDiaSeleccionado();
                    this.diaObtenido = sacarNumeroDia();
                    if (this.parametrosDB.get("MODELOALTERNACIA").toString().equalsIgnoreCase(Constantes.SI)) {
                        //sacar si el estudiante esta en modelo de alternacia con un if
                        ModeloAlternacia modelo = new ModeloAlternacia();
                        modelo = servicios.obtenerEstudiantesModeloAlternacia(email);
                        if (modelo != null && modelo.getIdModeloAlternacia() == 0L && !"".equals(this.segundoCorreo)) {
                            modelo = servicios.obtenerEstudiantesModeloAlternacia(this.segundoCorreo);
                        }
                        if (modelo != null && modelo.getIdModeloAlternacia() != 0L && modelo.getEstadoModelo().equalsIgnoreCase(Constantes.SI)) {
                            if (this.diaObtenido != null && !this.diaObtenido.equalsIgnoreCase("Ninguno")) {
                                if (!this.webService.obtenerHorarioEstudiante(documento, this.diaObtenido)) {
                                    this.datosPersonas = this.webService.obtenerSNFuncionario(email);
                                    if (!this.datosPersonas.isEmpty() && this.datosPersonas.containsKey("valor")) {
                                        return true;
                                    } else {
                                        return webService.obtenerSNEgresado(email);
                                    }
                                }
                            } else {
                                return false;
                            }
                        } else {
                            this.datosPersonas = this.webService.obtenerSNFuncionario(email);
                            if (!this.datosPersonas.isEmpty() && this.datosPersonas.containsKey("valor")) {
                                return true;
                            } else {
                                return webService.obtenerSNEgresado(email);
                            }
                        }

                    } else {
                        // sin modelo de alternacia
                        if (this.diaObtenido != null && !this.diaObtenido.equalsIgnoreCase("Ninguno")) {
                            if (!this.webService.obtenerHorarioEstudiante(documento, this.diaObtenido)) {
                                this.datosPersonas = this.webService.obtenerSNFuncionario(email);
                                if (!this.datosPersonas.isEmpty() && this.datosPersonas.containsKey("valor")) {
                                    return true;
                                } else {
                                    return webService.obtenerSNEgresado(email);
                                }
                            }
                        } else {
                            return false;
                        }
                    }
                }
            }
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", ErroresComunes.ERRORVERIFICARHORARIO));
            log.info(ErroresComunes.ERRORVERIFICARHORARIO + e.getMessage());
        }

        return true;
    }

    public String sacarNumeroDia() {
        switch (this.diaObtenido) {
            case "lunes": {
                return "lunes";
            }
            case "martes": {
                return "martes";
            }
            case "miércoles": {
                return "miercoles";
            }
            case "jueves": {
                return "jueves";
            }
            case "viernes": {
                return "viernes";
            }
            case "sábado": {
                return "sabado";
            }
            case "domingo": {
                return "domingo";
            }
            default: {
                return "Ninguno";
            }
        }
    }

    public String sacarDiaSeleccionado() {
        try {
            SimpleDateFormat sdformat = new SimpleDateFormat(Constantes.FORMATOFECHA);
            SimpleDateFormat diaSemana = new SimpleDateFormat(Constantes.FORMATOSEMANA);
            String Ingreso = sdformat.format(this.fechaIngresoUsuario);
            Date fechaIngreso = sdformat.parse(Ingreso);
            String diaObtenido = diaSemana.format(fechaIngreso);
            return diaObtenido;
        } catch (ParseException ex) {
            log.info(ErroresComunes.ERRORSIMPLEDATE + ex.getMessage());
        }
        return null;
    }

    private boolean verificarEncuestaUsuario() {
        this.ListRespuestaUsuario = this.servicios.obtenerRespuestaUsuario(usuarioRol);
        SimpleDateFormat sdformat = new SimpleDateFormat(Constantes.FORMATOFECHA);
        String textSystem = sdformat.format(this.fechaIngresoUsuario);
        for (Respuesta respuesta : this.ListRespuestaUsuario) {
            try {
                String textIngreso = sdformat.format(respuesta.getFechaFin());
                Date fechaSystem = sdformat.parse(textSystem);
                Date fechaIngreso = sdformat.parse(textIngreso);
                //fecha son iguales
                if (fechaSystem.compareTo(fechaIngreso) == 0) {
                    this.fechatexto = textIngreso;
                    return false;
                }
            } catch (ParseException ex) {
                log.info(ErroresComunes.ERRORSIMPLEDATE + ex.getMessage());
            }
        }
        this.ListRespuestaUsuario = new ArrayList<>();
        return true;
    }

    private boolean verificarEncuestaAdministrador(UsuarioRol temp) {
        this.ListRespuestaUsuario = this.servicios.obtenerRespuestaUsuario(temp);
        SimpleDateFormat sdformat = new SimpleDateFormat(Constantes.FORMATOFECHA);
        String textSystem = sdformat.format(this.fechaIngresoUsuario);
        for (Respuesta respuesta : this.ListRespuestaUsuario) {
            try {
                String textIngreso = sdformat.format(respuesta.getFechaFin());
                Date fechaSystem = sdformat.parse(textSystem);
                Date fechaIngreso = sdformat.parse(textIngreso);
                //fecha son iguales
                if (fechaSystem.compareTo(fechaIngreso) == 0) {
                    this.fechatexto = textIngreso;
                    return false;
                }
            } catch (ParseException ex) {
                log.info(ErroresComunes.ERRORSIMPLEDATE + ex.getMessage());
            }
        }
        this.ListRespuestaUsuario = new ArrayList<>();
        return true;
    }

    private void obtenerFechaIngreso() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        int a = cal.get(Calendar.HOUR_OF_DAY);
        int hora = 23 - a;
        int sumaHora = cal.get(Calendar.HOUR_OF_DAY) + hora;
        int minuto = 50 - cal.get(Calendar.MINUTE);
        Calendar calfin = Calendar.getInstance();
        calfin.setTime(fechaIngresoUsuario);
        calfin.set(Calendar.HOUR_OF_DAY, sumaHora);
        calfin.set(Calendar.MINUTE, minuto);
        fechaIngresoUsuario = calfin.getTime();
    }

    public boolean guardarRespuestas() {
        try {
            boolean error = true;
            for (Map.Entry<String, Object> e : this.respuestasObtenidas.entrySet()) {
                String key = e.getKey();
                Object value = e.getValue();
                detalleRespuesta.setIdRespuesta(this.ListRespuestaUsuario.get(0));
                if (this.respuestasObtenidas.get(key) instanceof String) {
                    if (key.equalsIgnoreCase("7") && this.respuestasObtenidas.get(key).toString().equals("19")) {
                        detalleRespuesta.setOtraVinculacion(this.guardarOtraVinculacion);
                    }
                    detalleRespuesta.getIdOpcionPregunta().setIdOPcionPregunta(Long.parseLong((String) this.respuestasObtenidas.get(key)));
                    if (!this.servicios.guardarDeatlleRespuesta(detalleRespuesta)) {
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", "Error al guardar las respuestas del usuario."));
                        error = false;
                        break;
                    }
                } else if (this.respuestasObtenidas.get(key) instanceof String[]) {
                    String[] temp = (String[]) this.respuestasObtenidas.get(key);
                    for (String temp1 : temp) {
                        detalleRespuesta.getIdOpcionPregunta().setIdOPcionPregunta(Long.parseLong(temp1));
                        if (!this.servicios.guardarDeatlleRespuesta(detalleRespuesta)) {
                            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", "Error al guardar las respuestas del usuario."));
                            error = false;
                            break;
                        }
                    }
                }
                detalleRespuesta = new DetalleRespuesta();
            }
            if (error) {
                if (!this.guardarRecomendacion()) {
                    return false;
                }
            }

        } catch (NumberFormatException e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", ErroresComunes.ERRORGUARDARRESPUESTAS));
            log.info(ErroresComunes.ERRORGUARDARRESPUESTAS + e.getMessage());
            return false;
        }
        return true;
    }

    public boolean guardarRecomendacion() {
        try {
            String recomendacion;
            List<OpcionPregunta> SN = new ArrayList<>();
            List<OpcionPregunta> multipleOp = new ArrayList<>();
            boolean temp2 = false;
            for (Map.Entry<String, Object> e : this.respuestasObtenidas.entrySet()) {
                String key = e.getKey();
                Object value = e.getValue();
                if (this.respuestasObtenidas.get(key) instanceof String) {
                    SN.add(this.obtenerCampoOpcion((String) value));
                } else if (this.respuestasObtenidas.get(key) instanceof String[]) {
                    String[] temp = (String[]) this.respuestasObtenidas.get(key);
                    for (String temp1 : temp) {
                        multipleOp.add(this.obtenerCampoOpcion(temp1));
                    }
                }
            }
            if (multipleOp.size() > 1) {
                this.recomendacionesRiesgo = true;
                this.guardarRecomendaciones("R");
                recomendacion = "R";
            } else if (multipleOp.get(0).getDescripcion().equalsIgnoreCase("Ninguna de las anteriores")) {
                for (int i = 0; SN.size() > i; i++) {
                    if (SN.get(i).getDescripcion().equalsIgnoreCase("SI")) {
                        temp2 = true;
                        break;
                    }
                }
                if (temp2) {
                    this.recomendacionesRiesgo = true;
                    this.guardarRecomendaciones("R");
                    recomendacion = "R";
                } else {
                    this.recomendacionesNormal = true;
                    this.guardarRecomendaciones("N");
                    recomendacion = "N";
                }
            } else {
                this.recomendacionesRiesgo = true;
                this.guardarRecomendaciones("R");
                recomendacion = "R";
            }
            listEncabezadoRecomendaciones = servicios.obtenerEncabezadoRecomendaciones();
            List<EncabezadoRecomendaciones> b = new ArrayList<>(); 
            if (recomendacion.equalsIgnoreCase("R")) {
                for (EncabezadoRecomendaciones a : listEncabezadoRecomendaciones) {
                    int resultado = a.getCodigo().toLowerCase().indexOf("riesgocovid");
                    if (resultado != -1) {
                        b.add(a);
                    }
                }
            } else if (recomendacion.equalsIgnoreCase("N")) {
                for (EncabezadoRecomendaciones a : listEncabezadoRecomendaciones) {
                    int resultado = a.getCodigo().toLowerCase().indexOf("normalcovid");
                    if (resultado != -1) {
                        b.add(a);
                    }
                }
            }
            listEncabezadoRecomendaciones = b;
            this.mostrarRecomendaciones();
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", ErroresComunes.ERRORGUARDARRECOMENDACIONES));
            log.info(ErroresComunes.ERRORGUARDARRECOMENDACIONES + e.getMessage());
            return false;
        }
        return true;
    }

    private void mostrarRecomendaciones() {
        try {
            this.formularioRendered = false;
            this.botonGuardarFormulario = false;
            this.formularioRecomendacionRendered = true;
            this.formularioAdministradorRendered = false;
            this.listRespuestaRecomendacion = this.servicios.obtenerRespuestaRecomendacion(this.ListRespuestaUsuario.get(0).getIdRespuesta().toString());
            if (listRespuestaRecomendacion.size() > 0 && !listRespuestaRecomendacion.isEmpty() && listRespuestaRecomendacion.get(0).getIdRecomendacion().getTipo().equalsIgnoreCase("R")) {
                this.colorRiesgo = Constantes.CODIGOROJO;
            } else if (listRespuestaRecomendacion.size() > 0 && !listRespuestaRecomendacion.isEmpty() && listRespuestaRecomendacion.get(0).getIdRecomendacion().getTipo().equalsIgnoreCase("N")) {
                this.colorRiesgo = Constantes.CODIGOVERDE;
                this.renderedRecomendacionesCodigo = true;
            }
            this.fechaRegistro = this.listRespuestaRecomendacion.get(0).getFechaCreacion();
            if (this.tipoUsuario.equalsIgnoreCase(Constantes.TIPOUSUARIO)) {
                this.nombreTablaIndividual = this.usuarioRol.getIdUsuario().getUsuario();
            } else if (this.tipoUsuario.equalsIgnoreCase(Constantes.TIPOADMINISTRADOR)) {
                this.nombreTablaIndividual = this.usuarioTemp.getUsuario();
                this.renderedNombreRecomendaciones = true;
            }
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", ErroresComunes.ERRORMOSTRARRECOMENDACIONES));
            log.info(ErroresComunes.ERRORMOSTRARRECOMENDACIONES + e.getMessage());
        }
    }

    private void guardarRecomendaciones(String tipo) {
        try {
            ListRecomendacion = servicios.obtenerRecomendaciones();
            for (int i = 0; ListRecomendacion.size() > i; i++) {
                if (ListRecomendacion.get(i).getTipo().equalsIgnoreCase(tipo)) {
                    respuestaRecomendacion = new RespuestaRecomendacion();
                    respuestaRecomendacion.setIdRespuesta(this.ListRespuestaUsuario.get(0));
                    respuestaRecomendacion.setIdRecomendacion(ListRecomendacion.get(i));
                    if (!(this.servicios.guardarRespuestaRecomendacion(respuestaRecomendacion))) {
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", ErroresComunes.ERRORGUARDARRECOMENDACIONES));
                        break;
                    }
                }
            }
        } catch (Exception e) {
            log.info(e.getMessage());
        }
    }

    public boolean opcionesMenuRecomendaciones(Long idEncabezadoRecomendaciones) {
        try {
            ListOpcionPintarRecomendaciones = new ArrayList<>();
            ListRecomendacion = servicios.obtenerRecomendacionesPorIdEncabezado(idEncabezadoRecomendaciones.toString());
            for (int i = 0; ListRecomendacion.size() > i; i++) {
                ListOpcionPintarRecomendaciones.add(ListRecomendacion.get(i));
            }
        } catch (Exception e) {
            log.info(e.getMessage());
        }
        return ListOpcionPintarRecomendaciones.size() > 0;
    }

    private OpcionPregunta obtenerCampoOpcion(String idOpcionPregunta) {
        try {
            for (int i = 0; this.ListOpcionPregunta.size() > i; i++) {
                if (this.ListOpcionPregunta.get(i).getIdOPcionPregunta() == Long.parseLong(idOpcionPregunta)) {
                    return this.ListOpcionPregunta.get(i);
                }
            }
        } catch (NumberFormatException e) {
            log.info(e.getMessage());
        }
        return null;
    }

    public void guardarValor(Pregunta pregunta) {
        if (pregunta.getIdPregunta().toString().equalsIgnoreCase("7")) {
            if (pregunta.getSecEncuesta().getSecEncuesta().toString().equalsIgnoreCase("1") && this.respuestaValor.equals("19") && pregunta.getTipoPregunta().equalsIgnoreCase("U")) {
                otraVinculacion = true;
            } else {
                otraVinculacion = false;
            }
        }

        if (pregunta.getIdPregunta().toString().equalsIgnoreCase("8")) {
            if (pregunta.getSecEncuesta().getSecEncuesta().toString().equalsIgnoreCase("1") && this.respuestaValor.equals("20") && pregunta.getTipoPregunta().equalsIgnoreCase("U")) {
                renderedCalendarioFormulario = true;
            } else {
                renderedCalendarioFormulario = false;
            }
        }

        if (this.respuestaMultiple != null && this.respuestaMultiple.length > 0) {
            if (this.respuestasObtenidas.containsKey(pregunta.getIdPregunta().toString())) {
                this.respuestasObtenidas.remove(pregunta.getIdPregunta().toString());
                this.respuestasObtenidas.put(pregunta.getIdPregunta().toString(), this.respuestaMultiple);
            } else {
                this.respuestasObtenidas.put(pregunta.getIdPregunta().toString(), this.respuestaMultiple);
            }
            this.respuestaMultiple = new String[0];
        } else if (this.respuestaValor instanceof String) {
            if (this.respuestasObtenidas.containsKey(pregunta.getIdPregunta().toString())) {
                this.respuestasObtenidas.remove(pregunta.getIdPregunta().toString());
                this.respuestasObtenidas.put(pregunta.getIdPregunta().toString(), (String) this.respuestaValor);
            } else {
                this.respuestasObtenidas.put(pregunta.getIdPregunta().toString(), (String) this.respuestaValor);
            }
            this.respuestaValor = new Object();
        }

    }

    //muestra los registros cuaando el usuario da click en la tabla
    public String mostrarRegistroGuardado(Respuesta usuarioRespuestas) {
        try {
            this.listDetalleRespuesta = new ArrayList<>();
            this.listDetalleRespuestaTemp = new ArrayList<>();
            this.formularioTablaIndividualRendered = true;
            this.formularioRegistroEnviados = false;
            this.consultarRegistros = false;
            this.botonRegresar = false;
//            this.menuAdministradorOpcionUno = false;
//            this.menuAdministradorOpcionDos = false;
            this.listDetalleRespuesta = this.servicios.obtenerDetalleRespuesta(usuarioRespuestas.getIdRespuesta().toString());
            this.fechaRegistro = this.listDetalleRespuesta.get(0).getIdRespuesta().getFechaFin();
            this.colorRiesgo = usuarioRespuestas.getColor();
            if (this.colorRiesgo.equalsIgnoreCase(Constantes.CODIGOVERDE)) {
                this.renderedRecomendacionesCodigo = true;
            }
            this.nombreTablaIndividual = usuarioRespuestas.getIdUsuario().getUsuario();
            for (int i = 0; this.listDetalleRespuesta.size() > i; i++) {
                if (this.listDetalleRespuesta.get(i).getIdOpcionPregunta().getIdPregunta().getTipoPregunta().equalsIgnoreCase("M")) {
                    this.listDetalleRespuestaTemp.add(this.listDetalleRespuesta.get(i));
                    this.listDetalleRespuesta.remove(i);
                    i = 0;
                }
            }
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", ErroresComunes.ERRORMOSTRARREGISTRO));
            log.info(ErroresComunes.ERRORMOSTRARREGISTRO + e.getMessage());
        }
        return "#";
    }

    public boolean pintarPreguntasMultiple(Long idPregunta) {
        boolean entre = true;
        try {
            for (int i = 0; this.pintarPreguntar.size() > i; i++) {
                if (Objects.equals(this.pintarPreguntar.get(i), idPregunta)) {
                    entre = false;
                }
            }
            if (entre) {
                this.pintarRespuestaMultiple = new ArrayList<>();
                for (int i = 0; this.listDetalleRespuestaTemp.size() > i; i++) {
                    if (Objects.equals(this.listDetalleRespuestaTemp.get(i).getIdOpcionPregunta().getIdPregunta().getIdPregunta(), idPregunta)) {
                        this.pintarRespuestaMultiple.add(this.listDetalleRespuestaTemp.get(i).getIdOpcionPregunta().getDescripcion());
                    }
                }
                this.pintarPreguntar.add(idPregunta);
            }
        } catch (Exception e) {
            log.info(e.getMessage());
        }
        return entre;
    }

    public boolean pintarTituloFormulario() {
        if (this.pintarTituloformulario) {
            this.pintarTituloformulario = false;
            return true;
        } else {
            return pintarTituloformulario;
        }
    }

    //funciones seguridad
    public void habilitarTablaSeguridad() {
        this.tablaSeguridad = true;
    }

    public void addMessage() {
        String summary = this.autorizo ? "Si autorizo" : "No autorizo";
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(summary));
    }

    public void habilitarEditarRoles() {
        this.obtenerRolesPersona();
        this.menuManejoRolRedered = true;
        this.botonRegresar = true;
        this.menuRendered = false;
        this.tablaMenuPrincipalAdministardor = false;
        this.tablaMenuPrincipalUsuario = false;
    }

    public void habilitarCambioContrasena() {
        this.renderedActivarCambioContrasena = true;
        this.botonRegresar = true;
        this.menuRendered = false;
        this.tablaMenuPrincipalAdministardor = false;
        this.tablaMenuPrincipalUsuario = false;
    }

    public void habilitarActivacionPersonas() {
        this.listPersonasPActivar();
        if (this.rederedRegistroActivacionuno) {
            this.rederedRegistroActivacionuno = false;
            this.rederedRegistroActivaciondos = true;
        } else {
            this.rederedRegistroActivacionuno = true;
            this.rederedRegistroActivaciondos = false;
        }
        this.rederedActivacion = true;
        this.activarPersonas = true;
        this.botonRegresar = true;
        this.menuRendered = false;
        this.tablaMenuPrincipalAdministardor = false;
        this.tablaMenuPrincipalUsuario = false;
    }

    public void listPersonasPActivar() {
        this.personas = new ArrayList<>();
        this.personasTabla = new ArrayList<>();
        this.personasTablaFiltro = new ArrayList<>();
        this.personasTablaFiltrotemp = new ArrayList<>();
        this.personas = this.servicios.obtenerPersonasDiaActivar();
        for (RespuestaRecomendacion p : personas) {
            if (p.getIdRespuesta().getProcesado() == null) {
                p.getIdRespuesta().setProcesadoBoo(false);
            } else if (p.getIdRespuesta().getProcesado().trim().equalsIgnoreCase(Constantes.NO)) {
                p.getIdRespuesta().setProcesadoBoo(false);
            } else if (p.getIdRespuesta().getProcesado().trim().equalsIgnoreCase(Constantes.SI)) {
                p.getIdRespuesta().setProcesadoBoo(true);
            }
            personasTabla.add(p);
        }
        this.personasTablaFiltro = this.personasTabla;
        this.personasTablaFiltrotemp = this.personasTabla;
    }

    public boolean activarFichet(RespuestaRecomendacion p) {
        if (p.getIdRespuesta().getProcesado().trim().equalsIgnoreCase(Constantes.SI)) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Información.", "Persona ya activada"));
            this.listPersonasPActivar();
            return true;
        } else {
            if (!this.webService.activarPersonasEncuesta(p.getIdRespuesta().getIdUsuario(), new Date())) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", "Debe registrarse en el sistema biométrico con una foto"));
                return false;
//                this.listPersonasPActivar();
            } else {
                if (this.servicios.actualizarProcesoActivacion(p.getIdRespuesta())) {
                    this.listPersonasPActivar();
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Información.", "Persona Activada"));
                    return true;
                } else {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", "Error al actualizar persona"));
                    return false;
                }
            }
        }
    }

    public void editarRolesPersona(UsuarioRol usuario, String rol) {
        try {
            boolean entreRol = true;
            List<UsuarioRol> temp = this.servicios.obtenerUsuarioRolporId(usuario);
            for (UsuarioRol h : temp) {
                if (h.getIdRol().getRol().equalsIgnoreCase(rol)) {
                    if (h.getIdRol().getEstado().equalsIgnoreCase("I")) {
                        if (this.cambiarEstadoRolAceptado(h)) {
                            entreRol = false;
                            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Se asigno el rol " + rol));
                        } else {
                            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", ErroresComunes.ERRORINDEFINDIO));
                        }

                    } else {
                        if (this.cambiarEstadoRolDenegado(h)) {
                            entreRol = false;
                            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Se denego el rol " + rol));
                        } else {
                            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", ErroresComunes.ERRORINDEFINDIO));
                        }
                    }
                }
            }

            if (entreRol) {
                if (!this.asignarRolPersona(usuario, rol)) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", ErroresComunes.ERRORINDEFINDIO));
                } else {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Se asigno el rol " + rol));
                }
            }
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", ErroresComunes.ERRORALCAMBIARROL));
            log.info(ErroresComunes.ERRORALCAMBIARROL + e.getMessage());
        }

    }

    private boolean cambiarEstadoRolAceptado(UsuarioRol usuario) {
        return this.servicios.actualizarRol(usuario.getIdUsuarioRol(), "A");
    }

    private boolean cambiarEstadoRolDenegado(UsuarioRol usuario) {
        return this.servicios.actualizarRol(usuario.getIdUsuarioRol(), "I");
    }

    private boolean asignarRolPersona(UsuarioRol usuario, String rol) {
        try {
            switch (rol) {
                case "Usuario": {
                    usuario.getIdRol().setIdRol(1L);
                    break;
                }
                case "Administrador": {
                    usuario.getIdRol().setIdRol(2L);
                    break;
                }
                case "Consulta": {
                    usuario.getIdRol().setIdRol(3L);
                    break;
                }
            }
            usuario.getIdRol().setEstado("A");
            return this.servicios.insertarRolPersona(usuario);
        } catch (Exception ex) {
            log.info(ex.getMessage());
            return false;
        }
    }

    public void obtenerRolesPersona() {
        this.listTablaManejoRol = new ArrayList<>();
        List<UsuarioRol> temp = this.servicios.obtenerUsuarioRolAll();
        for (int i = 0; i < temp.size(); i++) {
            List<UsuarioRol> roles = new ArrayList<>();
            Long idUsurio = temp.get(i).getIdUsuario().getIdUsuario();
            roles = this.ObtenerRolEspecifico(temp, idUsurio);
            for (UsuarioRol h : roles) {
                if (!h.getIdRol().getEstado().equalsIgnoreCase("I")) {
                    if (h.getIdRol().getRol().equalsIgnoreCase(Constantes.TIPOADMINISTRADOR)) {
                        temp.get(i).getIdUsuario().setRolAdministrador(true);
                    } else if (h.getIdRol().getRol().equalsIgnoreCase(Constantes.TIPOUSUARIO)) {
                        temp.get(i).getIdUsuario().setRolUsuario(true);
                    } else if (h.getIdRol().getRol().equalsIgnoreCase(Constantes.TIPOCONSULTA)) {
                        temp.get(i).getIdUsuario().setRolConsulta(true);
                    }
                }
            }
            if (!(Objects.equals(this.usuarioRol.getIdUsuario().getIdUsuario(), temp.get(i).getIdUsuario().getIdUsuario()))) {
                this.listTablaManejoRol.add(temp.get(i));
            }
            for (int j = 0; j < temp.size(); j++) {
                if (Objects.equals(temp.get(j).getIdUsuario().getIdUsuario(), idUsurio)) {
                    temp.remove(j);
                    j = 0;
                }
            }
            i = 0;
        }
        this.listTablaManejoRolFiltro = this.listTablaManejoRol;
    }

    private List<UsuarioRol> ObtenerRolEspecifico(List<UsuarioRol> temp, Long idUsuario) {
        List<UsuarioRol> roles = new ArrayList<>();
        for (UsuarioRol temp1 : temp) {
            if (Objects.equals(temp1.getIdUsuario().getIdUsuario(), idUsuario)) {
                roles.add(temp1);
            }
        }
        return roles;
    }

    public void cambiarRolVista() {
        try {
            this.IniciarAtributos();
            this.obtenerListados();
            this.usuarioRol = (UsuarioRol) sessionS.getSession().get(this.idp).get(Constantes.PERSONASESSION);
            this.listUsuarioRol = this.servicios.obtenerUsuarioRol(this.usuarioRol);
            this.variosRoles = (boolean) sessionS.getSession().get(this.idp).get("variosRoles");
            this.menuRendered = false;
            this.cargarMenuUnicoRol(false);
            this.habilitarLoginTipoUsuario();
        } catch (Exception ex) {
            log.info(ex.getMessage());
        }
    }

    public void habilitarTemperatura() {
        if (this.menuAdministradorOpcionUno) {
            this.menuAdministradorOpcionUno = false;
            this.menuAdministradorOpcionDos = true;
        } else {
            this.menuAdministradorOpcionUno = true;
            this.menuAdministradorOpcionDos = false;
        }
        this.botonRegresarTablaTemperatura = false;
        this.registroTemperatura = false;
        this.menuRendered = false;
        this.tablaMenuPrincipalAdministardor = false;
        this.tablaMenuPrincipalUsuario = false;
        this.mostrarRegistrosUsuarios();
        this.renderedTemperatura = true;
        this.tablaTemperatura = true;
        this.botonRegresar = true;
        this.temperaturaUsuario = 0;
        this.docTemperatura = "";
    }

    public void mostrarRegistrosUsuarios() {
        this.usuarioTemperatura = new ArrayList<>();
        this.usuarioTemperaturaFiltro = new ArrayList<>();
        this.usuarioTemperatura = servicios.obtenerUsuarioAll();
        for (int i = 0; i < this.usuarioTemperatura.size(); i++) {
            this.usuarioTemperatura.get(i).setTemperaturaExcel(this.sacarDatoTemperatura(this.usuarioTemperatura.get(i).getIdUsuario().getIdentificacion(), null));
        }
        this.usuarioTemperaturaFiltro = this.usuarioTemperatura;
        this.usuarioTemperautratemp = this.usuarioTemperatura;
    }

    public String mostrarRegistroGuardarTemperatura(Usuario usuarioRespuestas) {
        this.botonRegresar = false;
        this.tablaTemperatura = false;
        this.botonRegresarTablaTemperatura = true;
        this.registroTemperatura = true;
        this.listTemperaturaindividual(usuarioRespuestas);
        return "#";
    }

    public String guardarTemperatura() {
        try {
            if (this.temperaturaUsuario >= Double.parseDouble(parametrosDB.get("TEMPERATURAMIN").toString()) && this.temperaturaUsuario < Double.parseDouble(parametrosDB.get("TEMPERATURAMAX").toString())) {
                SimpleDateFormat sdformat = new SimpleDateFormat(Constantes.FORMATOFECHA);
                String textSystem = sdformat.format(new Date());
                Date fechaSystem = sdformat.parse(textSystem);
                boolean entre = false;
                List<Respuesta> temp = servicios.obtenerRespuestaUsuarioByDocumento(docTemperatura);
                for (Respuesta h : temp) {
                    String textIngreso = sdformat.format(h.getFechaFin());
                    Date fechaIngreso = sdformat.parse(textIngreso);
                    if (fechaSystem.compareTo(fechaIngreso) == 0) {
                        HistoriaTemperatura temperatura = new HistoriaTemperatura();
                        temperatura.getIdRespuesta().setIdRespuesta(h.getIdRespuesta());
                        temperatura.setTemperatura(this.temperaturaUsuario);
                        List<RespuestaRecomendacion> riesgos = new ArrayList<>();
                        if (servicios.guardarTemperatura(temperatura)) {
                            riesgos = servicios.obtenerEstadoRiesgo(h.getIdRespuesta().toString());
                            for (RespuestaRecomendacion riesgo : riesgos) {
                                if (riesgo.getIdRecomendacion().getTipo().equalsIgnoreCase("R")) {
                                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "", "La persona tiene un nivel de riesgo en Rojo"));
                                    break;
                                }
                            }
                            entre = true;
                            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Se guardo correctamente la temperatura"));
                            break;
                        }

                    }
                }
                if (!entre) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", ErroresComunes.ERRORENTRADATEMPERATURA));
                } else {
                    this.listTemperaturaindividual(null);
//                this.habilitarTemperatura();
                }
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "", "La temperatura debe estar entre " + parametrosDB.get("TEMPERATURAMIN").toString() + "y " + parametrosDB.get("TEMPERATURAMAX").toString()));
            }
        } catch (ParseException ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", ErroresComunes.ERRORTEMPERATURADOS));
        }
        return "#";
    }

    public void listTemperaturaindividual(Usuario usuarioRespuestas) {
        if ("".equals(this.docTemperatura)) {
            this.tempRegistroTemperatura = new ArrayList<>();
            this.tempRegistroTemperatura = servicios.obtenerTemperaturaByDocumento(usuarioRespuestas.getIdentificacion());
            this.docTemperatura = usuarioRespuestas.getIdentificacion();
        } else {
            this.tempRegistroTemperatura = new ArrayList<>();
            this.tempRegistroTemperatura = servicios.obtenerTemperaturaByDocumento(docTemperatura);
        }
        if (this.tempRegistroTemperatura == null) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", ErroresComunes.EERORTEMPERATURA));
            this.volverMenuPrincipal();
        } else {
            for (int i = 0; i < this.tempRegistroTemperatura.size(); i++) {
                this.tempRegistroTemperatura.get(i).setStrDate(this.obtenerFechaTemperatura(this.tempRegistroTemperatura.get(i).getFechacreacion()));
            }
        }
    }

    private String obtenerFechaTemperatura(Date fechaCreacion) {
        SimpleDateFormat formatter = new SimpleDateFormat(Constantes.FORMATOFECHA);
        return formatter.format(fechaCreacion);
    }

    public void regresarTablaTemperatura() {
        this.botonRegresar = true;
        this.tablaTemperatura = true;
        this.botonRegresarTablaTemperatura = false;
        this.registroTemperatura = false;
        if (this.menuAdministradorOpcionUno) {
            this.menuAdministradorOpcionUno = false;
            this.menuAdministradorOpcionDos = true;
        } else {
            this.menuAdministradorOpcionUno = true;
            this.menuAdministradorOpcionDos = false;
        }
        this.mostrarRegistrosUsuarios();
        this.docTemperatura = "";
        this.temperaturaUsuario = 0;
    }

    //VALIDAR SI VA A LA U O NO
    public String habilitarCampoCalendario() {
        if (presencial.equalsIgnoreCase(Constantes.NO)) {
            renderedCalendarioFormulario = false;
        } else {
            renderedCalendarioFormulario = true;
        }
        return "#";
    }

    public String cambiarContrasena() {
        if (!"".equals(nuevaContrasena) && !"".equals(confirmaContrasena)) {
            if (nuevaContrasena.equals(confirmaContrasena)) {
                Usuario temp = new Usuario();
                Utilidades h = new Utilidades();
                temp.setContrasena(h.Encriptar(nuevaContrasena));
                temp.setIdUsuario(this.usuarioRol.getIdUsuario().getIdUsuario());
                if (servicios.cambiarContraseña(temp)) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Información", "Contraseña actualizada. "));
                    this.salir();
                } else {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", "No se pudo actualizar contraseña."));
                }
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", "Contraseña incorrecta."));
            }
        }
        return "#";
    }

    public String cambiarContrasenaAdmin() {
        if (!"".equals(nuevaContrasena) && !"".equals(confirmaContrasena)) {
            Usuario temp = servicios.obtenerUsuarioPorCedula(identificacionContrasena);
            if (temp.getIdUsuario() != 0L) {
                if (nuevaContrasena.equals(confirmaContrasena)) {
                    Utilidades h = new Utilidades();
                    temp.setContrasena(h.Encriptar(nuevaContrasena));
                    if (servicios.cambiarContraseña(temp)) {
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Información", "Contraseña actualizada. "));
                        this.nuevaContrasena = new String();
                        this.confirmaContrasena = new String();
                        this.identificacionContrasena = new String();
                    } else {
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", "No se pudo actualizar contraseña."));
                    }
                } else {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", "Contraseña incorrecta."));
                }
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", "Persona no encontrada"));
            }

        }
        return "#";
    }

    public String habilitarCambioContraseña() {
        this.cambioContrasena = true;
        this.rederedContrasena = true;
        this.botonRegresar = true;
        this.menuRendered = false;
        this.tablaMenuPrincipalAdministardor = false;
        this.tablaMenuPrincipalUsuario = false;
        return "#";
    }

    public MenuView getMenu() {
        return menu;
    }

    public void setMenu(MenuView menu) {
        this.menu = menu;
    }

    public boolean isMenuRendered() {
        return menuRendered;
    }

    public void setMenuRendered(boolean menuRendered) {
        this.menuRendered = menuRendered;
    }

    public boolean isFormularioRendered() {
        return formularioRendered;
    }

    public void setFormularioRendered(boolean formularioRendered) {
        this.formularioRendered = formularioRendered;
    }

    public boolean isBotonRegresar() {
        return botonRegresar;
    }

    public void setBotonRegresar(boolean botonRegresar) {
        this.botonRegresar = botonRegresar;
    }

    public boolean isFormularioRegistroEnviados() {
        return formularioRegistroEnviados;
    }

    public void setFormularioRegistroEnviados(boolean formularioRegistroEnviados) {
        this.formularioRegistroEnviados = formularioRegistroEnviados;
    }

    public boolean isConsultarRegistros() {
        return consultarRegistros;
    }

    public void setConsultarRegistros(boolean consultarRegistros) {
        this.consultarRegistros = consultarRegistros;
    }

    public boolean isBotonGuardarFormulario() {
        return botonGuardarFormulario;
    }

    public void setBotonGuardarFormulario(boolean botonGuardarFormulario) {
        this.botonGuardarFormulario = botonGuardarFormulario;
    }

    public boolean isFormularioRecomendacionRendered() {
        return formularioRecomendacionRendered;
    }

    public void setFormularioRecomendacionRendered(boolean formularioRecomendacionRendered) {
        this.formularioRecomendacionRendered = formularioRecomendacionRendered;
    }

    public boolean isRecomendacionesRiesgo() {
        return recomendacionesRiesgo;
    }

    public void setRecomendacionesRiesgo(boolean recomendacionesRiesgo) {
        this.recomendacionesRiesgo = recomendacionesRiesgo;
    }

    public boolean isRecomendacionesNormal() {
        return recomendacionesNormal;
    }

    public void setRecomendacionesNormal(boolean recomendacionesNormal) {
        this.recomendacionesNormal = recomendacionesNormal;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Servicios getServicios() {
        return servicios;
    }

    public void setServicios(Servicios servicios) {
        this.servicios = servicios;
    }

    public UsuarioRol getUsuarioRol() {
        return usuarioRol;
    }

    public void setUsuarioRol(UsuarioRol usuarioRol) {
        this.usuarioRol = usuarioRol;
    }

    public DetalleRespuesta getDetalleRespuesta() {
        return detalleRespuesta;
    }

    public void setDetalleRespuesta(DetalleRespuesta detalleRespuesta) {
        this.detalleRespuesta = detalleRespuesta;
    }

    public RespuestaRecomendacion getRespuestaRecomendacion() {
        return respuestaRecomendacion;
    }

    public void setRespuestaRecomendacion(RespuestaRecomendacion respuestaRecomendacion) {
        this.respuestaRecomendacion = respuestaRecomendacion;
    }

    public List<Pregunta> getListPreguntas() {
        return ListPreguntas;
    }

    public void setListPreguntas(List<Pregunta> ListPreguntas) {
        this.ListPreguntas = ListPreguntas;
    }

    public List<Rol> getListRoles() {
        return ListRoles;
    }

    public void setListRoles(List<Rol> ListRoles) {
        this.ListRoles = ListRoles;
    }

    public List<OpcionPregunta> getListOpcionPregunta() {
        return ListOpcionPregunta;
    }

    public void setListOpcionPregunta(List<OpcionPregunta> ListOpcionPregunta) {
        this.ListOpcionPregunta = ListOpcionPregunta;
    }

    public List<OpcionPregunta> getListOpcionPintar() {
        return ListOpcionPintar;
    }

    public void setListOpcionPintar(List<OpcionPregunta> ListOpcionPintar) {
        this.ListOpcionPintar = ListOpcionPintar;
    }

    public List<Respuesta> getListRespuestaUsuario() {
        return ListRespuestaUsuario;
    }

    public void setListRespuestaUsuario(List<Respuesta> ListRespuestaUsuario) {
        this.ListRespuestaUsuario = ListRespuestaUsuario;
    }

    public List<Recomendaciones> getListRecomendacion() {
        return ListRecomendacion;
    }

    public void setListRecomendacion(List<Recomendaciones> ListRecomendacion) {
        this.ListRecomendacion = ListRecomendacion;
    }

    public List<RespuestaRecomendacion> getListRespuestaRecomendacion() {
        return listRespuestaRecomendacion;
    }

    public void setListRespuestaRecomendacion(List<RespuestaRecomendacion> listRespuestaRecomendacion) {
        this.listRespuestaRecomendacion = listRespuestaRecomendacion;
    }

    public HashMap<String, Object> getRespuestasObtenidas() {
        return respuestasObtenidas;
    }

    public void setRespuestasObtenidas(HashMap<String, Object> respuestasObtenidas) {
        this.respuestasObtenidas = respuestasObtenidas;
    }

    public Object getRespuestaValor() {
        return respuestaValor;
    }

    public void setRespuestaValor(Object respuestaValor) {
        this.respuestaValor = respuestaValor;
    }

    public String[] getRespuestaMultiple() {
        return respuestaMultiple;
    }

    public void setRespuestaMultiple(String[] respuestaMultiple) {
        this.respuestaMultiple = respuestaMultiple;
    }

    public List<Respuesta> getListTablaResultados() {
        return listTablaResultados;
    }

    public void setListTablaResultados(List<Respuesta> listTablaResultados) {
        this.listTablaResultados = listTablaResultados;
    }

    public List<DetalleRespuesta> getListDetalleRespuesta() {
        return listDetalleRespuesta;
    }

    public void setListDetalleRespuesta(List<DetalleRespuesta> listDetalleRespuesta) {
        this.listDetalleRespuesta = listDetalleRespuesta;
    }

    public boolean isFormularioTablaIndividualRendered() {
        return formularioTablaIndividualRendered;
    }

    public void setFormularioTablaIndividualRendered(boolean formularioTablaIndividualRendered) {
        this.formularioTablaIndividualRendered = formularioTablaIndividualRendered;
    }

    public List<DetalleRespuesta> getListDetalleRespuestaTemp() {
        return listDetalleRespuestaTemp;
    }

    public void setListDetalleRespuestaTemp(List<DetalleRespuesta> listDetalleRespuestaTemp) {
        this.listDetalleRespuestaTemp = listDetalleRespuestaTemp;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public List<Long> getPintarPreguntar() {
        return pintarPreguntar;
    }

    public void setPintarPreguntar(List<Long> pintarPreguntar) {
        this.pintarPreguntar = pintarPreguntar;
    }

    public List<String> getPintarRespuestaMultiple() {
        return pintarRespuestaMultiple;
    }

    public void setPintarRespuestaMultiple(List<String> pintarRespuestaMultiple) {
        this.pintarRespuestaMultiple = pintarRespuestaMultiple;
    }

    public String getColorRiesgo() {
        return colorRiesgo;
    }

    public void setColorRiesgo(String colorRiesgo) {
        this.colorRiesgo = colorRiesgo;
    }

    public List<UsuarioRol> getListUsuarioRol() {
        return listUsuarioRol;
    }

    public void setListUsuarioRol(List<UsuarioRol> listUsuarioRol) {
        this.listUsuarioRol = listUsuarioRol;
    }

    public boolean isLoginTipoUsuarioRendered() {
        return loginTipoUsuarioRendered;
    }

    public void setLoginTipoUsuarioRendered(boolean loginTipoUsuarioRendered) {
        this.loginTipoUsuarioRendered = loginTipoUsuarioRendered;
    }

    public boolean isFormularioAdministradorRendered() {
        return formularioAdministradorRendered;
    }

    public void setFormularioAdministradorRendered(boolean formularioAdministradorRendered) {
        this.formularioAdministradorRendered = formularioAdministradorRendered;
    }

    public String getTipoUsuario() {
        return tipoUsuario;
    }

    public void setTipoUsuario(String tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }

    public Usuario getUsuarioTemp() {
        return usuarioTemp;
    }

    public void setUsuarioTemp(Usuario usuarioTemp) {
        this.usuarioTemp = usuarioTemp;
    }

    public UsuarioRol getUsuarioRolTemp() {
        return usuarioRolTemp;
    }

    public void setUsuarioRolTemp(UsuarioRol usuarioRolTemp) {
        this.usuarioRolTemp = usuarioRolTemp;
    }

    public List<UsuarioRol> getListUsuarioRolTempAdministrador() {
        return listUsuarioRolTempAdministrador;
    }

    public void setListUsuarioRolTempAdministrador(List<UsuarioRol> listUsuarioRolTempAdministrador) {
        this.listUsuarioRolTempAdministrador = listUsuarioRolTempAdministrador;
    }

    public boolean isHabilitarLabelCedulaRendered() {
        return habilitarLabelCedulaRendered;
    }

    public void setHabilitarLabelCedulaRendered(boolean habilitarLabelCedulaRendered) {
        this.habilitarLabelCedulaRendered = habilitarLabelCedulaRendered;
    }

    public boolean isExterno() {
        return externo;
    }

    public void setExterno(boolean externo) {
        this.externo = externo;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNombreTablaIndividual() {
        return nombreTablaIndividual;
    }

    public void setNombreTablaIndividual(String nombreTablaIndividual) {
        this.nombreTablaIndividual = nombreTablaIndividual;
    }

    public List<Parametro> getParametros() {
        return parametros;
    }

    public void setParametros(List<Parametro> parametros) {
        this.parametros = parametros;
    }

    public boolean isPintarTituloformulario() {
        return pintarTituloformulario;
    }

    public void setPintarTituloformulario(boolean pintarTituloformulario) {
        this.pintarTituloformulario = pintarTituloformulario;
    }

    public HashMap<String, Object> getParametrosDB() {
        return parametrosDB;
    }

    public void setParametrosDB(HashMap<String, Object> parametrosDB) {
        this.parametrosDB = parametrosDB;
    }

    public boolean isMenuAdministrador() {
        return menuAdministrador;
    }

    public void setMenuAdministrador(boolean menuAdministrador) {
        this.menuAdministrador = menuAdministrador;
    }

    public boolean isMenuUsuario() {
        return menuUsuario;
    }

    public void setMenuUsuario(boolean menuUsuario) {
        this.menuUsuario = menuUsuario;
    }

    public List<Respuesta> getListTablaResultadosFiltro() {
        return listTablaResultadosFiltro;
    }

    public void setListTablaResultadosFiltro(List<Respuesta> listTablaResultadosFiltro) {
        this.listTablaResultadosFiltro = listTablaResultadosFiltro;
    }

    public String getNombreEncabezado() {
        return nombreEncabezado;
    }

    public void setNombreEncabezado(String nombreEncabezado) {
        this.nombreEncabezado = nombreEncabezado;
    }

    public Date getFechaIngresoUsuario() {
        return fechaIngresoUsuario;
    }

    public void setFechaIngresoUsuario(Date fechaIngresoUsuario) {
        this.fechaIngresoUsuario = fechaIngresoUsuario;
    }

    public Date getFechaMinDate() {
        return fechaMinDate;
    }

    public void setFechaMinDate(Date fechaMinDate) {
        this.fechaMinDate = fechaMinDate;
    }

    public Date getFechaMaxDate() {
        return fechaMaxDate;
    }

    public void setFechaMaxDate(Date fechaMaxDate) {
        this.fechaMaxDate = fechaMaxDate;
    }

    public boolean isTablaMenuPrincipalUsuario() {
        return tablaMenuPrincipalUsuario;
    }

    public void setTablaMenuPrincipalUsuario(boolean tablaMenuPrincipalUsuario) {
        this.tablaMenuPrincipalUsuario = tablaMenuPrincipalUsuario;
    }

    public boolean isTablaMenuPrincipalAdministardor() {
        return tablaMenuPrincipalAdministardor;
    }

    public void setTablaMenuPrincipalAdministardor(boolean tablaMenuPrincipalAdministardor) {
        this.tablaMenuPrincipalAdministardor = tablaMenuPrincipalAdministardor;
    }

    public List<Respuesta> getListTablaResultadosMenu() {
        return listTablaResultadosMenu;
    }

    public void setListTablaResultadosMenu(List<Respuesta> listTablaResultadosMenu) {
        this.listTablaResultadosMenu = listTablaResultadosMenu;
    }

    public List<Respuesta> getListTablaResultadosFiltroMenu() {
        return listTablaResultadosFiltroMenu;
    }

    public void setListTablaResultadosFiltroMenu(List<Respuesta> listTablaResultadosFiltroMenu) {
        this.listTablaResultadosFiltroMenu = listTablaResultadosFiltroMenu;
    }

    public boolean isMenuAdministradorOpcionUno() {
        return menuAdministradorOpcionUno;
    }

    public void setMenuAdministradorOpcionUno(boolean menuAdministradorOpcionUno) {
        this.menuAdministradorOpcionUno = menuAdministradorOpcionUno;
    }

    public boolean isMenuAdministradorOpcionDos() {
        return menuAdministradorOpcionDos;
    }

    public void setMenuAdministradorOpcionDos(boolean menuAdministradorOpcionDos) {
        this.menuAdministradorOpcionDos = menuAdministradorOpcionDos;
    }

    public List<Respuesta> getListTablaResultadoAdministrador() {
        return listTablaResultadoAdministrador;
    }

    public void setListTablaResultadoAdministrador(List<Respuesta> listTablaResultadoAdministrador) {
        this.listTablaResultadoAdministrador = listTablaResultadoAdministrador;
    }

    public List<Respuesta> getListTablaResultadoAdministradorFiltro() {
        return listTablaResultadoAdministradorFiltro;
    }

    public void setListTablaResultadoAdministradorFiltro(List<Respuesta> listTablaResultadoAdministradorFiltro) {
        this.listTablaResultadoAdministradorFiltro = listTablaResultadoAdministradorFiltro;
    }

    public HashMap<String, String> getDatosPersonas() {
        return datosPersonas;
    }

    public void setDatosPersonas(HashMap<String, String> datosPersonas) {
        this.datosPersonas = datosPersonas;
    }

    public boolean isRenderedNombreRecomendaciones() {
        return renderedNombreRecomendaciones;
    }

    public void setRenderedNombreRecomendaciones(boolean renderedNombreRecomendaciones) {
        this.renderedNombreRecomendaciones = renderedNombreRecomendaciones;
    }

    public boolean isRenderedRecomendacionesCodigo() {
        return renderedRecomendacionesCodigo;
    }

    public void setRenderedRecomendacionesCodigo(boolean renderedRecomendacionesCodigo) {
        this.renderedRecomendacionesCodigo = renderedRecomendacionesCodigo;
    }

    public boolean isMenuSeguridadRendered() {
        return menuSeguridadRendered;
    }

    public void setMenuSeguridadRendered(boolean menuSeguridadRendered) {
        this.menuSeguridadRendered = menuSeguridadRendered;
    }

    public String getLabelNumeroDocumentoTabla() {
        return labelNumeroDocumentoTabla;
    }

    public void setLabelNumeroDocumentoTabla(String labelNumeroDocumentoTabla) {
        this.labelNumeroDocumentoTabla = labelNumeroDocumentoTabla;
    }

    public boolean isTablaBuscarNumeroDocumento() {
        return tablaBuscarNumeroDocumento;
    }

    public void setTablaBuscarNumeroDocumento(boolean tablaBuscarNumeroDocumento) {
        this.tablaBuscarNumeroDocumento = tablaBuscarNumeroDocumento;
    }

    public boolean isTablaSeguridad() {
        return tablaSeguridad;
    }

    public void setTablaSeguridad(boolean tablaSeguridad) {
        this.tablaSeguridad = tablaSeguridad;
    }

    public boolean isMenuConsulta() {
        return menuConsulta;
    }

    public void setMenuConsulta(boolean menuConsulta) {
        this.menuConsulta = menuConsulta;
    }

    public boolean isBotonAdministrador() {
        return botonAdministrador;
    }

    public void setBotonAdministrador(boolean botonAdministrador) {
        this.botonAdministrador = botonAdministrador;
    }

    public boolean isBotonUsuario() {
        return botonUsuario;
    }

    public void setBotonUsuario(boolean botonUsuario) {
        this.botonUsuario = botonUsuario;
    }

    public boolean isBotonConsulta() {
        return botonConsulta;
    }

    public void setBotonConsulta(boolean botonConsulta) {
        this.botonConsulta = botonConsulta;
    }

    public int getColumnasLoginTipoUsuario() {
        return columnasLoginTipoUsuario;
    }

    public void setColumnasLoginTipoUsuario(int columnasLoginTipoUsuario) {
        this.columnasLoginTipoUsuario = columnasLoginTipoUsuario;
    }

    public String getFechatexto() {
        return fechatexto;
    }

    public void setFechatexto(String fechatexto) {
        this.fechatexto = fechatexto;
    }

    public boolean isAutorizo() {
        return autorizo;
    }

    public void setAutorizo(boolean autorizo) {
        this.autorizo = autorizo;
    }

    public String getAdm() {
        return adm;
    }

    public void setAdm(String adm) {
        this.adm = adm;
    }

    public String getDiaObtenido() {
        return diaObtenido;
    }

    public void setDiaObtenido(String diaObtenido) {
        this.diaObtenido = diaObtenido;
    }

    public boolean isMenuManejoRolRedered() {
        return menuManejoRolRedered;
    }

    public void setMenuManejoRolRedered(boolean menuManejoRolRedered) {
        this.menuManejoRolRedered = menuManejoRolRedered;
    }

    public List<UsuarioRol> getListTablaManejoRol() {
        return listTablaManejoRol;
    }

    public void setListTablaManejoRol(List<UsuarioRol> listTablaManejoRol) {
        this.listTablaManejoRol = listTablaManejoRol;
    }

    public List<UsuarioRol> getListTablaManejoRolFiltro() {
        return listTablaManejoRolFiltro;
    }

    public void setListTablaManejoRolFiltro(List<UsuarioRol> listTablaManejoRolFiltro) {
        this.listTablaManejoRolFiltro = listTablaManejoRolFiltro;
    }

    public boolean isVariosRoles() {
        return variosRoles;
    }

    public void setVariosRoles(boolean variosRoles) {
        this.variosRoles = variosRoles;
    }

    public boolean isRenderedTemperatura() {
        return renderedTemperatura;
    }

    public void setRenderedTemperatura(boolean renderedTemperatura) {
        this.renderedTemperatura = renderedTemperatura;
    }

    public boolean isTablaTemperatura() {
        return tablaTemperatura;
    }

    public void setTablaTemperatura(boolean tablaTemperatura) {
        this.tablaTemperatura = tablaTemperatura;
    }

    public boolean isRegistroTemperatura() {
        return registroTemperatura;
    }

    public void setRegistroTemperatura(boolean registroTemperatura) {
        this.registroTemperatura = registroTemperatura;
    }

    public double getTemperaturaUsuario() {
        return temperaturaUsuario;
    }

    public void setTemperaturaUsuario(double temperaturaUsuario) {
        this.temperaturaUsuario = temperaturaUsuario;
    }

    public boolean isBotonRegresarTablaTemperatura() {
        return botonRegresarTablaTemperatura;
    }

    public void setBotonRegresarTablaTemperatura(boolean botonRegresarTablaTemperatura) {
        this.botonRegresarTablaTemperatura = botonRegresarTablaTemperatura;
    }

    public List<HistoriaTemperatura> getTempRegistroTemperatura() {
        return tempRegistroTemperatura;
    }

    public void setTempRegistroTemperatura(List<HistoriaTemperatura> tempRegistroTemperatura) {
        this.tempRegistroTemperatura = tempRegistroTemperatura;
    }

    public String getDocTemperatura() {
        return docTemperatura;
    }

    public void setDocTemperatura(String docTemperatura) {
        this.docTemperatura = docTemperatura;
    }

    public List<TipoIdentificacion> getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    public void setTipoIdentificacion(List<TipoIdentificacion> tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    public String getSegundoCorreo() {
        return segundoCorreo;
    }

    public void setSegundoCorreo(String segundoCorreo) {
        this.segundoCorreo = segundoCorreo;
    }

    public boolean isRenderedCalendarioFormulario() {
        return renderedCalendarioFormulario;
    }

    public void setRenderedCalendarioFormulario(boolean renderedCalendarioFormulario) {
        this.renderedCalendarioFormulario = renderedCalendarioFormulario;
    }

    public String getPresencial() {
        return presencial;
    }

    public void setPresencial(String presencial) {
        this.presencial = presencial;
    }

    public List<Respuesta> getListHistoriaTemperatura() {
        return ListHistoriaTemperatura;
    }

    public void setListHistoriaTemperatura(List<Respuesta> ListHistoriaTemperatura) {
        this.ListHistoriaTemperatura = ListHistoriaTemperatura;
    }

    public List<Respuesta> getListTablaResultadosTemperatura() {
        return listTablaResultadosTemperatura;
    }

    public void setListTablaResultadosTemperatura(List<Respuesta> listTablaResultadosTemperatura) {
        this.listTablaResultadosTemperatura = listTablaResultadosTemperatura;
    }

    public List<Respuesta> getListTablaResultadosTemperaturaFiltro() {
        return listTablaResultadosTemperaturaFiltro;
    }

    public void setListTablaResultadosTemperaturaFiltro(List<Respuesta> listTablaResultadosTemperaturaFiltro) {
        this.listTablaResultadosTemperaturaFiltro = listTablaResultadosTemperaturaFiltro;
    }

    public List<Respuesta> getListTablaResultadoTemperaturaAdministrador() {
        return listTablaResultadoTemperaturaAdministrador;
    }

    public void setListTablaResultadoTemperaturaAdministrador(List<Respuesta> listTablaResultadoTemperaturaAdministrador) {
        this.listTablaResultadoTemperaturaAdministrador = listTablaResultadoTemperaturaAdministrador;
    }

    public List<Respuesta> getListTablaResultadoTemperaturaAdministradorFiltro() {
        return listTablaResultadoTemperaturaAdministradorFiltro;
    }

    public void setListTablaResultadoTemperaturaAdministradorFiltro(List<Respuesta> listTablaResultadoTemperaturaAdministradorFiltro) {
        this.listTablaResultadoTemperaturaAdministradorFiltro = listTablaResultadoTemperaturaAdministradorFiltro;
    }

    public List<Respuesta> getListTablaResultadosTemperaturaFiltroMenu() {
        return listTablaResultadosTemperaturaFiltroMenu;
    }

    public void setListTablaResultadosTemperaturaFiltroMenu(List<Respuesta> listTablaResultadosTemperaturaFiltroMenu) {
        this.listTablaResultadosTemperaturaFiltroMenu = listTablaResultadosTemperaturaFiltroMenu;
    }

    public String getNuevaContrasena() {
        return nuevaContrasena;
    }

    public void setNuevaContrasena(String nuevaContrasena) {
        this.nuevaContrasena = nuevaContrasena;
    }

    public String getConfirmaContrasena() {
        return confirmaContrasena;
    }

    public void setConfirmaContrasena(String confirmaContrasena) {
        this.confirmaContrasena = confirmaContrasena;
    }

    public boolean isCambioContrasena() {
        return cambioContrasena;
    }

    public void setCambioContrasena(boolean cambioContrasena) {
        this.cambioContrasena = cambioContrasena;
    }

    public boolean isRederedContrasena() {
        return rederedContrasena;
    }

    public void setRederedContrasena(boolean rederedContrasena) {
        this.rederedContrasena = rederedContrasena;
    }

    public boolean isRederedActivacion() {
        return rederedActivacion;
    }

    public void setRederedActivacion(boolean rederedActivacion) {
        this.rederedActivacion = rederedActivacion;
    }

    public boolean isActivarPersonas() {
        return activarPersonas;
    }

    public void setActivarPersonas(boolean activarPersonas) {
        this.activarPersonas = activarPersonas;
    }

    public List<RespuestaRecomendacion> getPersonas() {
        return personas;
    }

    public void setPersonas(List<RespuestaRecomendacion> personas) {
        this.personas = personas;
    }

    public List<RespuestaRecomendacion> getPersonasTabla() {
        return personasTabla;
    }

    public void setPersonasTabla(List<RespuestaRecomendacion> personasTabla) {
        this.personasTabla = personasTabla;
    }

    public boolean isDisplay() {
        return display;
    }

    public void setDisplay(boolean display) {
        this.display = display;
    }

    public List<RespuestaRecomendacion> getPersonasTablaFiltro() {
        return personasTablaFiltro;
    }

    public void setPersonasTablaFiltro(List<RespuestaRecomendacion> personasTablaFiltro) {
        this.personasTablaFiltro = personasTablaFiltro;
    }

    public String getTipoError() {
        return tipoError;
    }

    public void setTipoError(String tipoError) {
        this.tipoError = tipoError;
    }

    public String getIdp() {
        return idp;
    }

    public void setIdp(String idp) {
        this.idp = idp;
    }

    public boolean isRederedRegistroActivacionuno() {
        return rederedRegistroActivacionuno;
    }

    public void setRederedRegistroActivacionuno(boolean rederedRegistroActivacionuno) {
        this.rederedRegistroActivacionuno = rederedRegistroActivacionuno;
    }

    public boolean isRederedRegistroActivaciondos() {
        return rederedRegistroActivaciondos;
    }

    public void setRederedRegistroActivaciondos(boolean rederedRegistroActivaciondos) {
        this.rederedRegistroActivaciondos = rederedRegistroActivaciondos;
    }

    public List<RespuestaRecomendacion> getPersonasTablaFiltrotemp() {
        return personasTablaFiltrotemp;
    }

    public void setPersonasTablaFiltrotemp(List<RespuestaRecomendacion> personasTablaFiltrotemp) {
        this.personasTablaFiltrotemp = personasTablaFiltrotemp;
    }

    public List<Respuesta> getListHistoriaTemperaturaMenuP() {
        return ListHistoriaTemperaturaMenuP;
    }

    public void setListHistoriaTemperaturaMenuP(List<Respuesta> ListHistoriaTemperaturaMenuP) {
        this.ListHistoriaTemperaturaMenuP = ListHistoriaTemperaturaMenuP;
    }

    public List<Respuesta> getListTablaResultadosTemperaturaMenuP() {
        return listTablaResultadosTemperaturaMenuP;
    }

    public void setListTablaResultadosTemperaturaMenuP(List<Respuesta> listTablaResultadosTemperaturaMenuP) {
        this.listTablaResultadosTemperaturaMenuP = listTablaResultadosTemperaturaMenuP;
    }

    public List<Respuesta> getListTablaResultadosTemperaturaFiltroMenuP() {
        return listTablaResultadosTemperaturaFiltroMenuP;
    }

    public void setListTablaResultadosTemperaturaFiltroMenuP(List<Respuesta> listTablaResultadosTemperaturaFiltroMenuP) {
        this.listTablaResultadosTemperaturaFiltroMenuP = listTablaResultadosTemperaturaFiltroMenuP;
    }

    public List<Respuesta> getListTablaResultadoTemperaturaAdministradorMenuP() {
        return listTablaResultadoTemperaturaAdministradorMenuP;
    }

    public void setListTablaResultadoTemperaturaAdministradorMenuP(List<Respuesta> listTablaResultadoTemperaturaAdministradorMenuP) {
        this.listTablaResultadoTemperaturaAdministradorMenuP = listTablaResultadoTemperaturaAdministradorMenuP;
    }

    public List<Respuesta> getListTablaResultadoTemperaturaAdministradorFiltroMenuP() {
        return listTablaResultadoTemperaturaAdministradorFiltroMenuP;
    }

    public void setListTablaResultadoTemperaturaAdministradorFiltroMenuP(List<Respuesta> listTablaResultadoTemperaturaAdministradorFiltroMenuP) {
        this.listTablaResultadoTemperaturaAdministradorFiltroMenuP = listTablaResultadoTemperaturaAdministradorFiltroMenuP;
    }

    public List<Respuesta> getListTablaResultadosTemperaturaFiltroMenuMenuP() {
        return listTablaResultadosTemperaturaFiltroMenuMenuP;
    }

    public void setListTablaResultadosTemperaturaFiltroMenuMenuP(List<Respuesta> listTablaResultadosTemperaturaFiltroMenuMenuP) {
        this.listTablaResultadosTemperaturaFiltroMenuMenuP = listTablaResultadosTemperaturaFiltroMenuMenuP;
    }

    public String getfSystem() {
        return fSystem;
    }

    public void setfSystem(String fSystem) {
        this.fSystem = fSystem;
    }

    public Date getFechaInicioRegistro() {
        return fechaInicioRegistro;
    }

    public void setFechaInicioRegistro(Date fechaInicioRegistro) {
        this.fechaInicioRegistro = fechaInicioRegistro;
    }

    public Date getFechaFinalRegistro() {
        return fechaFinalRegistro;
    }

    public void setFechaFinalRegistro(Date fechaFinalRegistro) {
        this.fechaFinalRegistro = fechaFinalRegistro;
    }

    public boolean isContenidoDescargar() {
        return contenidoDescargar;
    }

    public void setContenidoDescargar(boolean contenidoDescargar) {
        this.contenidoDescargar = contenidoDescargar;
    }

    public List<Respuesta> getUsuarioTemperatura() {
        return usuarioTemperatura;
    }

    public void setUsuarioTemperatura(List<Respuesta> usuarioTemperatura) {
        this.usuarioTemperatura = usuarioTemperatura;
    }

    public List<Respuesta> getUsuarioTemperaturaFiltro() {
        return usuarioTemperaturaFiltro;
    }

    public void setUsuarioTemperaturaFiltro(List<Respuesta> usuarioTemperaturaFiltro) {
        this.usuarioTemperaturaFiltro = usuarioTemperaturaFiltro;
    }

    public List<Respuesta> getUsuarioTemperautratemp() {
        return usuarioTemperautratemp;
    }

    public void setUsuarioTemperautratemp(List<Respuesta> usuarioTemperautratemp) {
        this.usuarioTemperautratemp = usuarioTemperautratemp;
    }

    public String getIdentificacionContrasena() {
        return identificacionContrasena;
    }

    public void setIdentificacionContrasena(String identificacionContrasena) {
        this.identificacionContrasena = identificacionContrasena;
    }

    public boolean isRenderedActivarCambioContrasena() {
        return renderedActivarCambioContrasena;
    }

    public void setRenderedActivarCambioContrasena(boolean renderedActivarCambioContrasena) {
        this.renderedActivarCambioContrasena = renderedActivarCambioContrasena;
    }

    public boolean isOtraVinculacion() {
        return otraVinculacion;
    }

    public void setOtraVinculacion(boolean otraVinculacion) {
        this.otraVinculacion = otraVinculacion;
    }

    public SimpleDateFormat getTabla() {
        return tabla;
    }

    public void setTabla(SimpleDateFormat tabla) {
        this.tabla = tabla;
    }

    public SimpleDateFormat getTablaI() {
        return tablaI;
    }

    public void setTablaI(SimpleDateFormat tablaI) {
        this.tablaI = tablaI;
    }

    public int[] getOrdenarPreguntas() {
        return ordenarPreguntas;
    }

    public void setOrdenarPreguntas(int[] ordenarPreguntas) {
        this.ordenarPreguntas = ordenarPreguntas;
    }

    public String getGuardarOtraVinculacion() {
        return guardarOtraVinculacion;
    }

    public void setGuardarOtraVinculacion(String guardarOtraVinculacion) {
        this.guardarOtraVinculacion = guardarOtraVinculacion;
    }

    public List<Recomendaciones> getListOpcionPintarRecomendaciones() {
        return ListOpcionPintarRecomendaciones;
    }

    public void setListOpcionPintarRecomendaciones(List<Recomendaciones> ListOpcionPintarRecomendaciones) {
        this.ListOpcionPintarRecomendaciones = ListOpcionPintarRecomendaciones;
    }

    public List<EncabezadoRecomendaciones> getListEncabezadoRecomendaciones() {
        return listEncabezadoRecomendaciones;
    }

    public void setListEncabezadoRecomendaciones(List<EncabezadoRecomendaciones> listEncabezadoRecomendaciones) {
        this.listEncabezadoRecomendaciones = listEncabezadoRecomendaciones;
    }

}
