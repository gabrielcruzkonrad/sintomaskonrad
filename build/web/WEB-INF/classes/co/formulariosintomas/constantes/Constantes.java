/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.formulariosintomas.constantes;

/**
 *
 * @author cesar.chacon
 */
public class Constantes {
    
    //desarrollo
    public static final String SALIRAPLICACOIN = "https://login.microsoftonline.com/common/oauth2/v2.0/logout?post_logout_redirect_uri=http://localhost:8080/formularioSintomas/";
    public static final String SALIRAPLICACIONEXTERNO = "http://localhost:8080/formularioSintomas/";
    public static final String INICIOAPLICACION = "https://login.windows.net/konradlorenz.edu.co/oauth2/authorize?response_type=code&scope=directory.read.all&response_mode=form_post&redirect_uri=http%3A%2F%2Flocalhost%3A8080%2FformularioSintomas%2Ffaces%2Fvistas%2FformularioSintomas.xhtml&client_id=e3eb7dbd-4f65-4e05-88cd-a322bb761f36&resource=https%3a%2f%2fgraph.microsoft.com&state=8a3efb91-088f-4da9-ba17-394183620ca0&nonce=05d62cd9-9baf-46ac-a8dc-9649b9e7a185";
    public static final String INICIOPRINCIPAL = "http://localhost:8080/formularioSintomas/faces/vistas/formularioSintomas.xhtml";

//produccion
//    public static final String SALIRAPLICACOIN = "https://login.microsoftonline.com/common/oauth2/v2.0/logout?post_logout_redirect_uri=https://serviciosweb.konradlorenz.edu.co/SintomasKonrad/";
//    public static final String SALIRAPLICACIONEXTERNO = "https://serviciosweb.konradlorenz.edu.co/SintomasKonrad/";
//    public static final String INICIOAPLICACION = "https://login.windows.net/konradlorenz.edu.co/oauth2/authorize?response_type=code&scope=directory.read.all&response_mode=form_post&redirect_uri=https%3A%2F%2Fserviciosweb.konradlorenz.edu.co%2FSintomasKonrad%2Ffaces%2Fvistas%2FformularioSintomas.xhtml&client_id=e3eb7dbd-4f65-4e05-88cd-a322bb761f36&resource=https%3a%2f%2fgraph.microsoft.com&state=acff1928-dee4-4c1e-b13c-f9989b019baf&nonce=ada89b72-baa7-4366-b76f-a114d8fecbc2";
//    public static final String INICIOPRINCIPAL = "https://serviciosweb.konradlorenz.edu.co/SintomasKonrad/faces/vistas/formularioSintomas.xhtml";
//    
    public static final String URLGETBYEMAIL = "https://serviciosweb.konradlorenz.edu.co/financiero/api/persona/getByEmail?email=";
    public static final String URLESCOLARISGETBYEMAIL = "http://reportes.konradlorenz.edu.co:8189/ServiciosEscolaris/webresources/Estudiantes/byEmail/";
    public static final String URLGETBYDOCUMENTO = "http://reportes.konradlorenz.edu.co:8189/ServiciosEscolaris/webresources/HorarioEstudiantes/";
    
    public static final String AUTENTICACIONFICHET = "i7eSt1h1je8rxH8rDiVqiuqZ-LRoh-Q9XYYJzurqRRHqI2Eb-cli6pLb9rF6Azs5F-HcISjQpwXQjqNMAsqgnte_RqVrmRn8GIxsLhs_1q6xRkhe17XDrV9rwdd2l88FlGOoZa08jQppOXpoC8DLTN3GlZjNtG5LD15GThpxWBhM7LjknPM8JIEaSrFLTajdoFEpuDUveiJO3wXVtHgY";
    public static final String CLIENDID = "e3eb7dbd-4f65-4e05-88cd-a322bb761f36";
    public static final String CLIENTSECRET = "yoMfwh12PNMFP.H0C6D._rL8j.K_T4I42O";
    public static final String TENANT = "konradlorenz.edu.co";
    public static final String AUTHORITY = "https://login.windows.net/";
    public static final String TIPOUSUARIO = "USUARIO";
    public static final String TIPOADMINISTRADOR = "ADMINISTRADOR";
    public static final String TIPOCONSULTA = "CONSULTA";
    public static final String ESTADOACTIVO = "A";
    public static final String ESTADOINACTIVO = "I";
    public static final String INICIOPAGINA = "inicioPagina";
    public static final String PERSONASESSION = "persona";
    public static final String EXTERNO = "externo";
    public static final String MENUSESSION = "menu";
    public static final String TIPOUSUARIOSESSION = "tipoUsuario";
    public static final String VALIDOREGISTRO = "Válido";
    public static final String NOVALIDOREGISTRO = "No Válido";
    public static final String SINCONOCIMIENTOVALIDO = "Sin Conocimiento";
    public static final String ESTUDIANTE = "estudiantes";
    public static final String FUNCIONARIO = "funcionarios";
    public static final String IDENTIFICACION = "identificacion";
    public static final String EGRESADO = "egresados";
    public static final String IDENTIFICACIONNIT = "nit";
    public static final String MENSAJE = "mensaje";
    public static final String TIPOPERSONA = "tipopersona";
    public static final String TIPOESTUDIANTE = "E";
    public static final String TIPOFUNCIONARIO = "F";
    public static final String TIPOEXTERNO = "X";
    public static final String TIPOGRADUADO = "G";
    public static final String TIPOIDENTIFICACION = "tipoIdentificacion";
    public static final String CODIGOROJO = "red";
    public static final String CODIGOVERDE = "green";
    public static final String FORMATOFECHA = "yyyy-MM-dd";
    public static final String FORMATOFECHAHORA = "dd/MM/yyyy HH:mm";
    public static final String FORMATOFECHAHORASEG = "dd/MM/yyyy HH:mm:ss";
    public static final String FORMATOFECHAREC = "dd/MM/yyyy";
    public static final String FORMATOSEMANA = "EEEE";
    public static final String SI = "S";
    public static final String NO = "N";
    public static final String NOPRESENCIAL = "No Presencial";
//    public static final String ACTIVARUSUARIO = "http://192.168.14.6:9000/BioSec.asmx/AddPermission";
    public static final String ACTIVARUSUARIO = "http://192.168.14.6:60009/api/FaceHandSet/FaceIssue";
    public static final String ELIMINARUSUARIO = "http://192.168.14.6:60009/api/FaceHandSet/FaceUnregister";
    public static final String MODIFICARUSUARIO = "http://192.168.14.6:60009/api/FaceHandSet/FaceModify";
//    public static final String LISTADOUSUARIO = "http://192.168.14.6:9000/BioSec.asmx/GetPersonas";
    public static final String LISTADOUSUARIO = "http://192.168.14.6:60009/api/DmsOpenRecord/GetByCustom";
    public static final String ACTUALIZARLISTADOS = "http://192.168.14.6:60009/api/QueueMessage/SendUpdateSyncTokenMessage";
    public static final String PRIMERAPELLIDO = "primerApellido";
    public static final String SEGUNDOAPELLIDO = "segundoApellido";
    public static final String NOMBRERAZONSOCIAL = "nombreRazonSocial";
    public static final String NOMBRE = "nombre";
    public static final String ESCOLARISNOMBRE = "nombres";
    public static final String ESCOLARISAPELLIDOS = "apellidos";

    //cargue bioseguridad
    public static final String ROOTSTATEFACEISSUE = "RootStateFaceIssue";
    public static final String STATE = "State";
    public static final String OK = "200.0";

    //obtener personas bioseguridad
    public static final String RECORDS = "Records";
    public static final String BIOTEMPERATURA = "BodyTemperature";
    public static final String BIOIDENTIFICACION = "StaffNo";
    public static final String BIOTIPOIDENTIFICACION = "TipoIdentificacion";
    public static final String BIOFECHAMARCACION = "OpenDate";
    public static final String BIODEVICE = "DevNo";
                                            
}
