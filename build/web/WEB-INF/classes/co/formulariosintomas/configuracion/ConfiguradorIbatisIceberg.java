/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.formulariosintomas.configuracion;

import java.io.IOException;
import java.io.Reader;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.log4j.Logger;

/**
 *
 * @author jsuspes
 */
public class ConfiguradorIbatisIceberg {

    public static final ConfiguradorIbatisIceberg configuradorIbatis = new ConfiguradorIbatisIceberg();
    protected static Logger log = Logger.getLogger(ConfiguradorIbatisIceberg.class);
    private SqlSessionFactory sqlSessionFactory;

    private ConfiguradorIbatisIceberg() {
        super();
    }

    public static ConfiguradorIbatisIceberg getInstance() {
        return configuradorIbatis;
    }

    public void configurar(String ambiente) {

        //log.info("Inicia Configuracion ");
        try {
            String resource = "configuration-iceberg.xml";
            Reader reader = Resources.getResourceAsReader(resource);

            this.sqlSessionFactory = new SqlSessionFactoryBuilder().build(reader, ambiente);
            log.info("SqlSession conexion a iceberg configurado OK!! ");

        } catch (IOException e) {
            log.error(e.getMessage());
        }
    }

    public SqlSessionFactory getSqlSessionFactory() {
        return sqlSessionFactory;
    }
}
