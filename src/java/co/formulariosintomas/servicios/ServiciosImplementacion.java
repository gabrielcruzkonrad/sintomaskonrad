/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.formulariosintomas.servicios;

import co.formulariosintomas.bean.DetalleRespuesta;
import co.formulariosintomas.bean.EncabezadoRecomendaciones;
import co.formulariosintomas.bean.HistoriaTemperatura;
import co.formulariosintomas.bean.ModeloAlternacia;
import co.formulariosintomas.bean.OpcionPregunta;
import co.formulariosintomas.bean.Parametro;
import co.formulariosintomas.bean.Pregunta;
import co.formulariosintomas.bean.Recomendaciones;
import co.formulariosintomas.bean.Respuesta;
import co.formulariosintomas.bean.RespuestaRecomendacion;
import co.formulariosintomas.bean.Rol;
import co.formulariosintomas.bean.TipoIdentificacion;
import co.formulariosintomas.bean.Usuario;
import co.formulariosintomas.bean.UsuarioRol;
import co.formulariosintomas.constantes.Constantes;
import co.formulariosintomas.manager.ManagerSintomas;
import co.formulariosintomas.utils.Utilidades;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author David
 */
public class ServiciosImplementacion implements Servicios {

    private final static Logger log = Logger.getLogger(ServiciosImplementacion.class);

    @Override
    public List<Recomendaciones> obtenerRecomendaciones() {
        List<Recomendaciones> recomendaciones = new ArrayList();
        try {
            recomendaciones = (List<Recomendaciones>) ManagerSintomas.getManager().obtenerListado("obtenerRecomendaciones");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return recomendaciones;
    }

    @Override
    public List<Recomendaciones> obtenerRecomendacionesPorIdEncabezado(String idEncabezado) {
        List<Recomendaciones> recomendaciones = new ArrayList();
        try {
            recomendaciones = (List<Recomendaciones>) ManagerSintomas.getManager().obtenerListado("obtenerRecomendacionesPorIdEncabezado", idEncabezado);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return recomendaciones;
    }

    @Override
    public List<Pregunta> obtenerPregunta() {
        List<Pregunta> preguntas = new ArrayList();
        try {
            preguntas = (List<Pregunta>) ManagerSintomas.getManager().obtenerListado("obtenerPreguntas");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return preguntas;
    }

    @Override
    public List<OpcionPregunta> obtenerOpcionPreguntaList() {
        List<OpcionPregunta> opcionpreguntas = new ArrayList();
        try {
            opcionpreguntas = (List<OpcionPregunta>) ManagerSintomas.getManager().obtenerListado("obtenerOpcionPreguntasList");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return opcionpreguntas;
    }

    @Override
    public List<Rol> obtenerRol() {
        List<Rol> roles = new ArrayList();
        try {
            roles = (List<Rol>) ManagerSintomas.getManager().obtenerListado("obtenerRol");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return roles;
    }

    @Override
    public List<OpcionPregunta> obtenerOpcionPregunta() {
        List<OpcionPregunta> preguntas = new ArrayList();
        try {
            preguntas = (List<OpcionPregunta>) ManagerSintomas.getManager().obtenerListado("obtenerOpcionPreguntas");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return preguntas;
    }

    @Override
    public boolean guardarDeatlleRespuesta(DetalleRespuesta detalle) {
        try {
            ManagerSintomas.getManager().insertarRegistro("insertarDetalleRespuesta", detalle);
            return true;
        } catch (Exception e) {
            log.error(e.getMessage());
            return false;
        }
    }

    @Override
    public void guardarUsuarioRol(UsuarioRol usuarioRol) {

    }

    @Override
    public List<UsuarioRol> obtenerUsuarioRol(UsuarioRol usuario) {
        List<UsuarioRol> persona = new ArrayList<>();
        try {
            persona = (List<UsuarioRol>) ManagerSintomas.getManager().obtenerListado("obtenerUsuarioRol", usuario.getIdUsuario().getEmail());
            if (persona == null) {
                persona = new ArrayList<>();
                persona.add(new UsuarioRol());
                persona.get(0).setIdUsuarioRol(0L);
                persona.get(0).getIdUsuario().setEmail(usuario.getIdUsuario().getEmail());
                persona.get(0).getIdUsuario().setUsuario(usuario.getIdUsuario().getUsuario());
            }
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return persona;
    }

    @Override
    public List<UsuarioRol> obtenerUsuarioRolAll() {
        List<UsuarioRol> persona = new ArrayList<>();
        try {
            persona = (List<UsuarioRol>) ManagerSintomas.getManager().obtenerListado("obtenerUsuarioRolAll");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return persona;
    }

    @Override
    public List<UsuarioRol> obtenerUsuarioRolporId(UsuarioRol usuario) {
        List<UsuarioRol> persona = new ArrayList<>();
        try {
            persona = (List<UsuarioRol>) ManagerSintomas.getManager().obtenerListado("obtenerUsuarioRolporId", usuario.getIdUsuario().getIdUsuario().toString());
            if (persona == null) {
                persona = new ArrayList<>();
                persona.add(new UsuarioRol());
                persona.get(0).setIdUsuarioRol(0L);
            }
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return persona;
    }

    @Override
    public UsuarioRol obtenerUsuario(UsuarioRol usuario) {
        Usuario persona = new Usuario();
        try {
            persona = (Usuario) ManagerSintomas.getManager().obtenerRegistro("obtenerUsuario", usuario.getIdUsuario());
            if (persona == null || persona.getIdUsuario() == 0) {
                persona = new Usuario();
                persona.setIdUsuario(0L);
                usuario.setIdUsuario(persona);
            } else {
                usuario.setIdUsuario(persona);
            }
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return usuario;
    }

    @Override
    public boolean obtenerUsuarioExterno(UsuarioRol usuario) {
        Usuario persona = new Usuario();
        try {
            persona = (Usuario) ManagerSintomas.getManager().obtenerRegistro("obtenerUsuario", usuario.getIdUsuario());
            if (persona == null || persona.getIdUsuario() == 0) {
                return false;
            } else {
                return true;
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            return false;
        }
    }

    @Override
    public List<Respuesta> guardarRespuestaUsuario(UsuarioRol usuario, String tiempo, Date fechaIngreso, Respuesta resp) {
        List<Respuesta> respuesta = new ArrayList<>();
        try {
            resp.setIdUsuario(usuario.getIdUsuario());
//            //Establecemos la fecha que deseamos en un Calendario
//            Calendar cal = Calendar.getInstance();
//            cal.setTime(temp.getFechaFin());
//
//            //Desplegamos la fecha
//            Date tempDate = cal.getTime();
//            System.out.println("Fecha actual: " + tempDate);
//
//            //Le cambiamos la hora y minutos
//            cal.set(Calendar.HOUR, cal.get(Calendar.HOUR) + Integer.parseInt(tiempo));
//            tempDate = cal.getTime();
//            System.out.println("Hora modificada: " + tempDate);           
            resp.setFechaFin(fechaIngreso);
            resp.setProcesado(Constantes.NO);
            ManagerSintomas.getManager().insertarRegistro("insertarRespuesta", resp);
            respuesta = this.obtenerRespuestaUsuario(usuario);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return respuesta;
    }

    @Override
    public boolean guardarRespuestaRecomendacion(RespuestaRecomendacion respuestaRecomendacion) {
        try {
            ManagerSintomas.getManager().insertarRegistro("insertarRespuestaRecomendacion", respuestaRecomendacion);
            return true;
        } catch (Exception e) {
            log.error(e.getMessage());
            return false;
        }
    }

    @Override
    public List<Respuesta> obtenerRespuestaUsuario(UsuarioRol usuario) {
        List<Respuesta> respuesta = new ArrayList<>();
        try {
            respuesta = (List<Respuesta>) ManagerSintomas.getManager().obtenerListado("obtenerIdRespuesta", usuario.getIdUsuario().getIdUsuario().toString());
            if (respuesta == null) {
                respuesta = new ArrayList<>();
            }
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return respuesta;
    }

    @Override
    public List<Respuesta> obtenerRespuestaUsuarioByDocumento(String documento) {
        List<Respuesta> respuesta = new ArrayList<>();
        try {
            respuesta = (List<Respuesta>) ManagerSintomas.getManager().obtenerListado("obtenerIdRespuestaByDocumento", documento);
            if (respuesta == null) {
                respuesta = new ArrayList<>();
            }
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return respuesta;
    }

    @Override
    public List<RespuestaRecomendacion> obtenerEstadoRiesgo(String idRespuesta) {
        List<RespuestaRecomendacion> respuesta = new ArrayList<>();
        try {
            respuesta = (List<RespuestaRecomendacion>) ManagerSintomas.getManager().obtenerListado("obtenerEstadoRiesgo", idRespuesta);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return respuesta;
    }

    @Override
    public List<Respuesta> obtenerRespuestaUsuarioAll() {
        List<Respuesta> respuesta = new ArrayList<>();
        try {
            respuesta = (List<Respuesta>) ManagerSintomas.getManager().obtenerListado("obtenerIdRespuestaAll");
            if (respuesta == null) {
                respuesta = new ArrayList<>();
            }
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return respuesta;
    }

    @Override
    public List<Respuesta> obtenerTemperaturaAll(Respuesta r) {
        List<Respuesta> respuesta = new ArrayList<>();
        try {
            respuesta = (List<Respuesta>) ManagerSintomas.getManager().obtenerListado("obtenerTemperaturaAll", r);
            if (respuesta == null) {
                respuesta = new ArrayList<>();
            }
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return respuesta;
    }

    @Override
    public List<Respuesta> obtenerTemperaturaAllMenu() {
        List<Respuesta> respuesta = new ArrayList<>();
        try {
            respuesta = (List<Respuesta>) ManagerSintomas.getManager().obtenerListado("obtenerTemperaturaAllMenu");
            if (respuesta == null) {
                respuesta = new ArrayList<>();
            }
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return respuesta;
    }

    @Override
    public List<RespuestaRecomendacion> obtenerRespuestaRecomendacion(String idRepuesta) {
        List<RespuestaRecomendacion> respuestaRecomendaciones = new ArrayList<>();
        try {
            respuestaRecomendaciones = (List<RespuestaRecomendacion>) ManagerSintomas.getManager().obtenerListado("obtenerRespuestasRecomendaciones", idRepuesta);
            if (respuestaRecomendaciones == null) {
                respuestaRecomendaciones = new ArrayList<>();
            }
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return respuestaRecomendaciones;
    }

    @Override
    public List<DetalleRespuesta> obtenerDetalleRespuesta(String idRespuesta) {
        List<DetalleRespuesta> persona = new ArrayList<>();
        try {
            persona = (List<DetalleRespuesta>) ManagerSintomas.getManager().obtenerListado("obtenerDetalleRespuestas", idRespuesta);
            if (persona == null) {
                persona = new ArrayList<>();
            }
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return persona;
    }

    @Override
    public Usuario obtenerUsuarioPorCedula(String identificacion) {
        Usuario persona = new Usuario();
        try {
            persona = (Usuario) ManagerSintomas.getManager().obtenerRegistro("obtenerUsuarioCedula", identificacion);
            if (persona == null) {
                persona = new Usuario();
                persona.setIdUsuario(0L);
                persona.setIdentificacion(identificacion);
            }
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return persona;
    }

    @Override
    public List<Respuesta> obtenerUsuarioAll() {
        List<Respuesta> persona = new ArrayList<>();
        try {
            persona = (List<Respuesta>) ManagerSintomas.getManager().obtenerListado("obtenerUsuarioAll");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return persona;
    }

    @Override
    public boolean guardarPersonaExterna(UsuarioRol usuario) {
        try {
            UsuarioRol usuarioPersona = new UsuarioRol();
            boolean usuarioEnt = this.obtenerUsuarioExterno(usuario);
            if (usuarioEnt == false) {
                ManagerSintomas.getManager().insertarRegistro("insertarUsuario", usuario.getIdUsuario());
                usuarioPersona = this.obtenerUsuario(usuario);
                usuario.getIdUsuario().setIdUsuario(usuarioPersona.getIdUsuario().getIdUsuario());
                usuario.setEstado(Constantes.ESTADOACTIVO);
                usuario.setIdRol(new Rol());
                usuario.getIdRol().setIdRol(1L);
                ManagerSintomas.getManager().insertarRegistro("insertarUsuarioRol", usuario);
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            log.error(e.getMessage());
            return false;
        }
    }

    @Override
    public boolean insertarRolPersona(UsuarioRol usuario) {
        try {
            ManagerSintomas.getManager().insertarRegistro("insertarUsuarioRol", usuario);
            return true;
        } catch (Exception ex) {
            log.info(ex.getMessage());
            return false;
        }
    }

    @Override
    public boolean actualizarRol(Long idUsuario, String metodo) {
        try {
            switch (metodo) {
                case "A": {
                    ManagerSintomas.getManager().actualizarRegistro("actualizarEstadoRolAceptado", idUsuario);
                    break;
                }
                case "I": {
                    ManagerSintomas.getManager().actualizarRegistro("actualizarEstadoRolDenegado", idUsuario);
                    break;
                }
            }
            return true;
        } catch (Exception e) {
            log.error(e.getMessage());
            return false;
        }
    }

    @Override
    public void guardarUsuario(UsuarioRol usuario) {
        try {
            UsuarioRol temp = new UsuarioRol();
//            usuario.getIdUsuario().setIdentificacion("12390");
            ManagerSintomas.getManager().insertarRegistro("insertarUsuario", usuario.getIdUsuario());
            temp = this.obtenerUsuario(usuario);
            usuario.getIdUsuario().setIdUsuario(temp.getIdUsuario().getIdUsuario());
            usuario.setEstado(Constantes.ESTADOACTIVO);
            ManagerSintomas.getManager().insertarRegistro("insertarUsuarioRol", usuario);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public Usuario validarPersonaExterna(Usuario usuario) {
        Usuario persona = new Usuario();
        try {
            persona = (Usuario) ManagerSintomas.getManager().obtenerRegistro("obtenerUsuario", usuario);
            if (persona != null) {
                Utilidades desencriptar = new Utilidades();
                persona.setContrasena(desencriptar.Desencriptar(persona.getContrasena()));
                if (persona.getContrasena().equals(usuario.getContrasena())) {
                    persona.setErrorUsuario("Ninguna");
                    return persona;
                } else {
                    persona.setErrorUsuario("Datos incorrectos");
                    return persona;
                }
            } else {
                persona = new Usuario();
                persona.setErrorUsuario("Persona no existe");
                return persona;
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            persona.setErrorUsuario("Error inesperado");
            return persona;
        }
    }

    @Override
    public List<Parametro> obtenerParametros() {
        List<Parametro> parametros = new ArrayList();
        try {
            parametros = (List<Parametro>) ManagerSintomas.getManager().obtenerListado("obtenerParametro");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return parametros;
    }

    @Override
    public boolean guardarTemperatura(HistoriaTemperatura temperatura) {
        try {
            ManagerSintomas.getManager().insertarRegistro("insertarHistoriaTemperatura", temperatura);
            return true;
        } catch (Exception e) {
            log.error(e.getMessage());
            return false;
        }
    }

    @Override
    public List<HistoriaTemperatura> obtenerTemperaturaByDocumento(String doc) {
        List<HistoriaTemperatura> temperatura = new ArrayList();
        try {
            temperatura = (List<HistoriaTemperatura>) ManagerSintomas.getManager().obtenerListado("obtenerTemperaturaByUsuario", doc);
        } catch (Exception e) {
            log.error(e.getMessage());
            return null;
        }
        return temperatura;
    }

    @Override
    public List<HistoriaTemperatura> obtenerTemperaturaByFecha(HistoriaTemperatura his) {
        List<HistoriaTemperatura> temperatura = new ArrayList();
        try {
            temperatura = (List<HistoriaTemperatura>) ManagerSintomas.getManager().obtenerListado("obtenerTemperaturaByFecha", his);
        } catch (Exception e) {
            log.error(e.getMessage());
            return null;
        }
        return temperatura;
    }

    @Override
    public ModeloAlternacia obtenerEstudiantesModeloAlternacia(String email) {
        ModeloAlternacia persona = new ModeloAlternacia();
        try {
            persona = (ModeloAlternacia) ManagerSintomas.getManager().obtenerRegistro("obtenerEstudianteModeloAlternacia", email);
            if (persona == null) {
                persona = new ModeloAlternacia();
                persona.setIdModeloAlternacia(0L);
            }
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return persona;
    }

    @Override
    public boolean cambiarContraseña(Usuario contrasena) {
        try {
            ManagerSintomas.getManager().actualizarRegistro("ActualizarContrasena", contrasena);
            return true;
        } catch (Exception e) {
            log.error(e.getMessage());
            return false;
        }
    }

    @Override
    public List<RespuestaRecomendacion> obtenerPersonasDia() {
        List<RespuestaRecomendacion> respuestaRecomendaciones = new ArrayList<>();
        try {
            respuestaRecomendaciones = (List<RespuestaRecomendacion>) ManagerSintomas.getManager().obtenerListado("obtenerPersonasDia");
            if (respuestaRecomendaciones == null) {
                respuestaRecomendaciones = new ArrayList<>();
            }
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return respuestaRecomendaciones;
    }

    @Override
    public List<RespuestaRecomendacion> obtenerPersonasDiaActivar() {
        List<RespuestaRecomendacion> respuestaRecomendaciones = new ArrayList<>();
        try {
            respuestaRecomendaciones = (List<RespuestaRecomendacion>) ManagerSintomas.getManager().obtenerListado("obtenerPersonasDiaActivar");
            if (respuestaRecomendaciones == null) {
                respuestaRecomendaciones = new ArrayList<>();
            }
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return respuestaRecomendaciones;
    }

    @Override
    public List<TipoIdentificacion> obtenerTipoIdentifacion() {
        List<TipoIdentificacion> identifacion = new ArrayList();
        try {
            identifacion = (List<TipoIdentificacion>) ManagerSintomas.getManager().obtenerListado("obtenerTipoIdentificacion");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return identifacion;
    }

    @Override
    public boolean actualizarProcesoActivacion(Respuesta respuesta) {
        try {
            respuesta.setProcesado(Constantes.SI);
            ManagerSintomas.getManager().actualizarRegistro("ActualizarProcesoActivacion", respuesta);
            return true;
        } catch (Exception e) {
            log.error(e.getMessage());
            return false;
        }
    }

    @Override
    public List<EncabezadoRecomendaciones> obtenerEncabezadoRecomendaciones() {
        List<EncabezadoRecomendaciones> encabezado = new ArrayList<>();
        try {
            encabezado = (List<EncabezadoRecomendaciones>) ManagerSintomas.getManager().obtenerListado("obtenerEncabezadoRecomendaciones", "");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return encabezado;
    }

}
