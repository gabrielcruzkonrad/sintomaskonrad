/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.formulariosintomas.listeners;

/**
 *
 * @author jsuspes
 */
import co.formulariosintomas.configuracion.ConfiguradorIbatisIceberg;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import org.apache.log4j.Logger;

/**
 *
 * @author jsuspes
 */
public class ContextoAplicacionListenerIceberg implements ServletContextListener {

    protected static Logger log = Logger.getLogger(ContextoAplicacionListener.class);

    /**
     * Propiedad rutaContexto de la clase [ ContextoAplicacionListener.java ]
     *
     * @desc: guarda ela ruta fisica del contexto de la aplicacion
     */
    private String rutaContexto = "";

    public ContextoAplicacionListenerIceberg() {
        // TODO Auto-generated constructor stub
    }

    /**
     * @see ServletContextListener#contextInitialized(ServletContextEvent)
     */
    public void contextInitialized(ServletContextEvent arg0) {
        // TODO Auto-generated method stub

        // Carga la configuracion de Ibatis
        ConfiguradorIbatisIceberg.getInstance().configurar("conexion-iceberg");

        //hallamos la ruta del contexto de la aplicacion 
        this.rutaContexto = arg0.getServletContext().getRealPath("/");

        try {
            Properties prop = new Properties();
            FileInputStream file = new FileInputStream(this.rutaContexto + "WEB-INF/classes/" + "datasource-iceberg.properties");
            prop.load(file);
            file.close();
        } catch (FileNotFoundException e) {
            log.error(e);
        } catch (IOException e) {
            log.error(e);
        }

        //guardamos esta ruta para usarla en otros modulos
        ContextoAplicacionIceberg pContextoAplicacionIceberg = ContextoAplicacionIceberg.getInstance();
        pContextoAplicacionIceberg.setRutaContexto(this.rutaContexto);
    }

    @Override
    public void contextDestroyed(ServletContextEvent arg0) {
        // TODO Auto-generated method stub

    }
}
