/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.formulariosintomas.manager;

import co.formulariosintomas.dao.DaoIceberg;
import co.formulariosintomas.dao.DaoIceberg;
import java.util.List;

/**
 *
 * @author jsuspes
 */
public class ManagerIceberg extends ManagerStandard {

    private static final ManagerIceberg managerIceberg = new ManagerIceberg();

    /**
     * @type Constructor de la clase FacadeGeneral
     * @name FacadeGeneral
     * @return void
     * @desc Crea una instancia de FacadeGeneral
     */
    private ManagerIceberg() {
        super();
    }

    /**
     * @type Mï¿½todo de la clase ManagerBP
     * @name getManager
     * @return ManagerBP
     * @desc permite obtener la instancia del objeto ManagerBP (singlenton)
     */
    public static ManagerIceberg getManager() {
        return managerIceberg;
    }

    public List obtenerListado(String sqlName, Object objeto) throws Exception {
        DaoIceberg serviciosDao = new DaoIceberg();
        return (List) serviciosDao.obtenerListado(sqlName, objeto);
    }

    public List obtenerListado(String sqlName) throws Exception {
        DaoIceberg serviciosDao = new DaoIceberg();
        return (List) serviciosDao.obtenerListado(sqlName);
    }

    public Object ejecutarProcedimiento(String sqlName) throws Exception {
        DaoIceberg serviciosDao = new DaoIceberg();
        return serviciosDao.ejecutarProcedimiento(sqlName);
    }

    public Object obtenerRegistro(String sqlName, Object object) throws Exception {
        DaoIceberg serviciosDao = new DaoIceberg();
        return serviciosDao.obtenerRegistro(sqlName, object);
    }

    public Object obtenerRegistro(String sqlName) throws Exception {
        DaoIceberg serviciosDao = new DaoIceberg();
        return serviciosDao.obtenerRegistro(sqlName);
    }

    public Object insertarRegistro(String qryName, Object objeto)
            throws Exception {
        DaoIceberg serviciosDao = new DaoIceberg();
        return serviciosDao.insertarRegistro(qryName, objeto);
    }

    public Object actualizarRegistro(String qryName, Object objeto)
            throws Exception {
        DaoIceberg serviciosDao = new DaoIceberg();
        return serviciosDao.actualizarRegistro(qryName, objeto);
    }

    public void borrarRegistro(String sqlName, Object object) throws Exception {
        DaoIceberg serviciosDao = new DaoIceberg();
        serviciosDao.borrarRegistro(sqlName, object);
    }
}
