/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.formulariosintomas.bean;

import java.util.Date;

/**
 *
 * @author David
 */
public class Recomendaciones {

    private Long idRecomendaciones;
    private String codigo;
    private String enunciado;
    private String tipo;
    private String estado;
    private Date fechaCreacion;
    private Date fechaModificaciones;
    private EncabezadoRecomendaciones idEncabezadoPregunta;

    public Recomendaciones() {
        this.fechaCreacion = new Date();
        this.fechaModificaciones = new Date();
        this.idEncabezadoPregunta = new EncabezadoRecomendaciones();
    }

    public Long getIdRecomendaciones() {
        return idRecomendaciones;
    }

    public void setIdRecomendaciones(Long idRecomendaciones) {
        this.idRecomendaciones = idRecomendaciones;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getEnunciado() {
        return enunciado;
    }

    public void setEnunciado(String enunciado) {
        this.enunciado = enunciado;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Date getFechaModificaciones() {
        return fechaModificaciones;
    }

    public void setFechaModificaciones(Date fechaModificaciones) {
        this.fechaModificaciones = fechaModificaciones;
    }

    public EncabezadoRecomendaciones getIdEncabezadoPregunta() {
        return idEncabezadoPregunta;
    }

    public void setIdEncabezadoPregunta(EncabezadoRecomendaciones idEncabezadoPregunta) {
        this.idEncabezadoPregunta = idEncabezadoPregunta;
    }

}
