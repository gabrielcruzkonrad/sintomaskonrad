/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.formulariosintomas.bean;

import java.util.Date;

/**
 *
 * @author David
 */
public class OpcionPregunta {

    private Long idOpcionPregunta;
    private Pregunta idPregunta;
    private String codigo;
    private String descripcion;
    private String estado;
    private Date fechaCreacion;
    private Date fechaModificacion;

    public OpcionPregunta() {
        this.fechaCreacion = new Date();
        this.fechaModificacion = new Date();
        this.idPregunta = new Pregunta();
    }

    public Long getIdOPcionPregunta() {
        return idOpcionPregunta;
    }

    public void setIdOPcionPregunta(Long idOpcionPregunta) {
        this.idOpcionPregunta = idOpcionPregunta;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public Pregunta getIdPregunta() {
        return idPregunta;
    }

    public void setIdPregunta(Pregunta idPregunta) {
        this.idPregunta = idPregunta;
    }

}
