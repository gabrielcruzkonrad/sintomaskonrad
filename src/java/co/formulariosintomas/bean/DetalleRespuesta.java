/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.formulariosintomas.bean;

import java.util.Date;

/**
 *
 * @author David
 */
public class DetalleRespuesta {

    private Long idDetalleRespuesta;
    private Respuesta idRespuesta;
    private OpcionPregunta idOpcionPregunta;
    private Date fechaCreacion;
    private String otraVinculacion;

    public DetalleRespuesta() {
        this.fechaCreacion = new Date();
        this.idOpcionPregunta = new OpcionPregunta();
        this.idRespuesta = new Respuesta();
    }

    public Long getIdDetalleRespuesta() {
        return idDetalleRespuesta;
    }

    public void setIdDetalleRespuesta(Long idDetalleRespuesta) {
        this.idDetalleRespuesta = idDetalleRespuesta;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Respuesta getIdRespuesta() {
        return idRespuesta;
    }

    public void setIdRespuesta(Respuesta idRespuesta) {
        this.idRespuesta = idRespuesta;
    }

    public OpcionPregunta getIdOpcionPregunta() {
        return idOpcionPregunta;
    }

    public void setIdOpcionPregunta(OpcionPregunta idOpcionPregunta) {
        this.idOpcionPregunta = idOpcionPregunta;
    }

    public String getOtraVinculacion() {
        return otraVinculacion;
    }

    public void setOtraVinculacion(String otraVinculacion) {
        this.otraVinculacion = otraVinculacion;
    }

}
