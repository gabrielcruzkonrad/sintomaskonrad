/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.formulariosintomas.bean;

import java.util.Date;

/**
 *
 * @author David
 */
public class RespuestaRecomendacion {

    private Long idRespuestaRecomendacion;
    private Respuesta idRespuesta;
    private Recomendaciones idRecomendacion;
    private Date fechaCreacion;

    public RespuestaRecomendacion() {
        this.fechaCreacion = new Date();
        this.idRespuesta = new Respuesta();
        this.idRecomendacion = new Recomendaciones();
    }

    public Long getIdRespuestaRecomendacion() {
        return idRespuestaRecomendacion;
    }

    public void setIdRespuestaRecomendacion(Long idRespuestaRecomendacion) {
        this.idRespuestaRecomendacion = idRespuestaRecomendacion;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Respuesta getIdRespuesta() {
        return idRespuesta;
    }

    public void setIdRespuesta(Respuesta idRespuesta) {
        this.idRespuesta = idRespuesta;
    }

    public Recomendaciones getIdRecomendacion() {
        return idRecomendacion;
    }

    public void setIdRecomendacion(Recomendaciones idRecomendacion) {
        this.idRecomendacion = idRecomendacion;
    }

}
