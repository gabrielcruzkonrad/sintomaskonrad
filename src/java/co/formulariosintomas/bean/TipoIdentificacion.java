/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.formulariosintomas.bean;

/**
 *
 * @author cesard.chacond
 */
public class TipoIdentificacion {
    
    private Long secTIpoIdentificacion;
    private String codigo;
    private String descripcion;
    private String codigoEquivalente;

    public Long getSecTIpoIdentificacion() {
        return secTIpoIdentificacion;
    }

    public void setSecTIpoIdentificacion(Long secTIpoIdentificacion) {
        this.secTIpoIdentificacion = secTIpoIdentificacion;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCodigoEquivalente() {
        return codigoEquivalente;
    }

    public void setCodigoEquivalente(String codigoEquivalente) {
        this.codigoEquivalente = codigoEquivalente;
    }
    
    
}
