/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.formulariosintomas.bean;

import co.formulariosintomas.constantes.Constantes;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.DefaultSubMenu;
import org.primefaces.model.menu.MenuModel;

/**
 *
 * @author David
 */
public class MenuView {

    private MenuModel model;
    private String tipoUsuario;

    public MenuView(String tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
        this.init();
    }

    public MenuView() {

    }

    public void init() {
        model = new DefaultMenuModel();
        DefaultSubMenu secondSubmenu = new DefaultSubMenu();
        secondSubmenu.setLabel("Menu Principal");
        secondSubmenu.setStyle("background: gray;");
        if (tipoUsuario.equalsIgnoreCase(Constantes.TIPOUSUARIO)) {
            this.menuUsuarios(secondSubmenu);
        } else {
            this.menuAdministrador(secondSubmenu);
        }
    }

    private void menuUsuarios(DefaultSubMenu secondSubmenu) {
        DefaultMenuItem item = new DefaultMenuItem();
        item.setValue("Formulario COVID 19");
//        item.setIcon("fa fa-undefined");
        item.setCommand("#{formularioSintomasMB.habilitarFormularioCovid()}");
        item.setUpdate(":frmFormulario");
        secondSubmenu.getElements().add(item);
        item = new DefaultMenuItem();
        item.setValue("Registros Enviados");
//        item.setIcon("fa fa-times");
        item.setCommand("#{formularioSintomasMB.habilitarRegistroEnviados()}");
        item.setUpdate(":frmFormulario");
        secondSubmenu.getElements().add(item);
        item = new DefaultMenuItem();
        item.setValue("Salir");
        // item.setHref(Constantes.SALIRAPLICACOIN);
        item.setCommand("#{formularioSintomasMB.salir()}");
//        item.setIcon("fa fa-sertec-spanish-icon");
        item.setUpdate(":frmFormulario");
        secondSubmenu.getElements().add(item);
        model.getElements().add(secondSubmenu);
    }

    private void menuAdministrador(DefaultSubMenu secondSubmenu) {
        DefaultMenuItem item = new DefaultMenuItem();
        item.setValue("Formulario COVID 19");
//        item.setIcon("fa fa-undefined");
        item.setCommand("#{formularioSintomasMB.habilitarRegistroEnviadosAdministrador()}");
        item.setUpdate(":frmFormulario");
        secondSubmenu.getElements().add(item);
        item = new DefaultMenuItem();
        item.setValue("Consultar Registros");
//        item.setIcon("fa fa-times");
        item.setCommand("#{formularioSintomasMB.habilitarConsultarRegistros()}");
        item.setUpdate(":frmFormulario");
        secondSubmenu.getElements().add(item);
        item = new DefaultMenuItem();
        item.setValue("Salir");
        item.setCommand("#{formularioSintomasMB.salir()}");
//        item.setIcon("fa fa-sertec-spanish-icon");
        item.setUpdate(":frmFormulario");
        secondSubmenu.getElements().add(item);
        model.getElements().add(secondSubmenu);
    }

    public MenuModel getModel() {
        return model;
    }

    public void save() {
        addMessage("Success", "Data saved");
    }

    public void update() {
        addMessage("Success", "Data updated");
    }

    public void delete() {
        addMessage("Success", "Data deleted");
    }

    public void addMessage(String summary, String detail) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, detail);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
}
