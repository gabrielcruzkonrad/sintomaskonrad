/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.formulariosintomas.bean;

import java.util.Date;

/**
 *
 * @author David
 */
public class EncabezadoRecomendaciones {

    private Long idEncabezadoPregunta;
    private String codigo;
    private String titulo;
    private String estado;
    private Date fechaCreacion;
    private Date fechaModificacion;

    public EncabezadoRecomendaciones() {
        this.fechaCreacion = new Date();
        this.fechaModificacion = new Date();
    }

    public Long getIdEncabezadoPregunta() {
        return idEncabezadoPregunta;
    }

    public void setIdEncabezadoPregunta(Long idEncabezadoPregunta) {
        this.idEncabezadoPregunta = idEncabezadoPregunta;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

   
}
