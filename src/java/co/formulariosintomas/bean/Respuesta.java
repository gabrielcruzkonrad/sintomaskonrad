/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.formulariosintomas.bean;

import co.formulariosintomas.constantes.Constantes;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author David
 */
public class Respuesta {

    private Long idRespuesta;
    private Usuario idUsuario;
    private Date fechaInicio;
    private Date fechaFin;
    private Date fehcaCreacion;
    //valido registro
    private String validoRegistro;
    //Codigo Color
    private String color;
    //cmabiar texto la fecha
    private SimpleDateFormat formatter = new SimpleDateFormat(Constantes.FORMATOFECHAHORA);
    private SimpleDateFormat formatterFechafin = new SimpleDateFormat(Constantes.FORMATOFECHA);
    private String strDate;
    private String strDateFin;
    private String autorizo;
    private String presencial;
    private String procesado;
    private boolean procesadoBoo;

    private String fechaInicialRegistro;
    private String fechaFinalRegistro;
    private String temperaturaExcel;

    public Respuesta() {
        this.idUsuario = new Usuario();
        this.fechaInicio = new Date();
        this.fechaFin = new Date();
        this.fehcaCreacion = new Date();
        this.procesadoBoo = false;
    }

    public Long getIdRespuesta() {
        return idRespuesta;
    }

    public void setIdRespuesta(Long idRespuesta) {
        this.idRespuesta = idRespuesta;
    }

    public Usuario getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Usuario idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public Date getFehcaCreacion() {
        return fehcaCreacion;
    }

    public void setFehcaCreacion(Date fehcaCreacion) {
        this.fehcaCreacion = fehcaCreacion;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getValidoRegistro() {
        return validoRegistro;
    }

    public void setValidoRegistro(String validoRegistro) {
        this.validoRegistro = validoRegistro;
    }

    public SimpleDateFormat getFormatter() {
        return formatter;
    }

    public void setFormatter(SimpleDateFormat formatter) {
        this.formatter = formatter;
    }

    public String getStrDate() {
        return strDate;
    }

    public void setStrDate(String strDate) {
        this.strDate = strDate;
    }

    public String getStrDateFin() {
        return strDateFin;
    }

    public void setStrDateFin(String strDateFin) {
        this.strDateFin = strDateFin;
    }

    public SimpleDateFormat getFormatterFechafin() {
        return formatterFechafin;
    }

    public void setFormatterFechafin(SimpleDateFormat formatterFechafin) {
        this.formatterFechafin = formatterFechafin;
    }

    public String getAutorizo() {
        return autorizo;
    }

    public void setAutorizo(String autorizo) {
        this.autorizo = autorizo;
    }

    public String getPresencial() {
        return presencial;
    }

    public void setPresencial(String presencial) {
        this.presencial = presencial;
    }

    public String getProcesado() {
        return procesado;
    }

    public void setProcesado(String procesado) {
        this.procesado = procesado;
    }

    public boolean isProcesadoBoo() {
        return procesadoBoo;
    }

    public void setProcesadoBoo(boolean procesadoBoo) {
        this.procesadoBoo = procesadoBoo;
    }

    public String getFechaInicialRegistro() {
        return fechaInicialRegistro;
    }

    public void setFechaInicialRegistro(String fechaInicialRegistro) {
        this.fechaInicialRegistro = fechaInicialRegistro;
    }

    public String getFechaFinalRegistro() {
        return fechaFinalRegistro;
    }

    public void setFechaFinalRegistro(String fechaFinalRegistro) {
        this.fechaFinalRegistro = fechaFinalRegistro;
    }

    public String getTemperaturaExcel() {
        return temperaturaExcel;
    }

    public void setTemperaturaExcel(String temperaturaExcel) {
        this.temperaturaExcel = temperaturaExcel;
    }

}
