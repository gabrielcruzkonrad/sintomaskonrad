/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.formulariosintomas.bean;

import java.util.Date;

/**
 *
 * @author cesard.chacond
 */
public class UsuarioFichet {

    private String Identifiacion;
    private String Nombres;
    private String TipoIdentificacion;
    private Date FechaMarcacion;
    private String Temperature;
    private String Device;

    public UsuarioFichet(String Identifiacion, String TipoIdentificacion, Date FechaMarcacion, String Temperature, String Device) {
        this.Identifiacion = Identifiacion;
        this.TipoIdentificacion = TipoIdentificacion;
        this.FechaMarcacion = FechaMarcacion;
        this.Temperature = Temperature;
        this.Device = Device;
    }

    
    public String getIdentifiacion() {
        return Identifiacion;
    }

    public void setIdentifiacion(String Identifiacion) {
        this.Identifiacion = Identifiacion;
    }

    public String getNombres() {
        return Nombres;
    }

    public void setNombres(String Nombres) {
        this.Nombres = Nombres;
    }

    public String getTipoIdentificacion() {
        return TipoIdentificacion;
    }

    public void setTipoIdentificacion(String TipoIdentificacion) {
        this.TipoIdentificacion = TipoIdentificacion;
    }

    public Date getFechaMarcacion() {
        return FechaMarcacion;
    }

    public void setFechaMarcacion(Date FechaMarcacion) {
        this.FechaMarcacion = FechaMarcacion;
    }

    public String getTemperature() {
        return Temperature;
    }

    public void setTemperature(String Temperature) {
        this.Temperature = Temperature;
    }

    public String getDevice() {
        return Device;
    }

    public void setDevice(String Device) {
        this.Device = Device;
    }

    
}
