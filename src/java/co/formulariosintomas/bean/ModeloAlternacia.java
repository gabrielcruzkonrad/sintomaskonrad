/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.formulariosintomas.bean;

/**
 *
 * @author cesard.chacond
 */
public class ModeloAlternacia {
    
    private Long idModeloAlternacia;
    private String usuario;
    private String email;
    private String codigo;
    private String programa;
    private String estadoModelo;

    public Long getIdModeloAlternacia() {
        return idModeloAlternacia;
    }

    public void setIdModeloAlternacia(Long idModeloAlternacia) {
        this.idModeloAlternacia = idModeloAlternacia;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getPrograma() {
        return programa;
    }

    public void setPrograma(String programa) {
        this.programa = programa;
    }

    public String getEstadoModelo() {
        return estadoModelo;
    }

    public void setEstadoModelo(String estadoModelo) {
        this.estadoModelo = estadoModelo;
    }
    
    
}
