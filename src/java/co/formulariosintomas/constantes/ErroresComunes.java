/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.formulariosintomas.constantes;

/**
 *
 * @author cesard.chacond
 */
public class ErroresComunes {
    
    public static final String ERRORLOGIN = "No puedo ingresar no puedo obtener la sesion de la persona. ";
    public static final String ERRORLOGININDEFINIDO = "Error al inicio FormularioSintomasMB. ";
    public static final String ERRORINDEFINDIO = "Error inesperado Comuniquese a la Universidad. ";
    public static final String ERRORCARGARMENU = "Error al carga menu. ";
    public static final String ERRORMS = "Error en el login de microsoft. ";
    public static final String ERRORMOSTRARREGISTRO = "Error al obtener la información. ";
    public static final String ERRORSALIR = "Error al cerrar sesión. ";
    public static final String ERRORPERSONANOREGISTRADA = "La persona no esta registrada. ";
    public static final String ERRORTIENEENCUESTA = "Usted ya tiene una encuesta registrada para el día  ";
    public static final String ERRORAUTORIZACION = "La persona debe aceptar el tratamiento de datos. ";
    public static final String ERRORSINPERMISODIA = "La persona no tiene permiso para ir el día ";
    public static final String ERRORVERIFICARHORARIO = "Error al verificar su horario de la Universidad. ";
    public static final String ERRORSIMPLEDATE = "Error de fecha sin especificar. ";
    public static final String ERRORGUARDARRESPUESTAS = "Error al guardar las respuestas del usuario. ";
    public static final String ERRORGUARDARRECOMENDACIONES = "Error al guardar recomendaciones. ";
    public static final String ERRORMOSTRARRECOMENDACIONES = "Error al mostrar recomendaciones. ";
    public static final String ERRORALCAMBIARROL = "Error al cambiar rol al usuario. ";
    public static final String EERORTEMPERATURA =   "Error al consultar temperatura. ";
    public static final String ERRORTEMPERATURADOS = "Error al guardar temperatura. ";
    public static final String ERRORENTRADATEMPERATURA = "Error el usuario no tiene registro de encuesta para el día de hoy. ";
    public static final String ERRORRANGOTEMPERATURA = "";
    public static final String ERRORACTIVARFICHET = "Error al activar ingreso a la Universidad por favor comuniquese al correo cesard.chacond@konradlorenz.edu.co ";
}
