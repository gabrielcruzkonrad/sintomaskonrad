/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.formulariosintomas.job;

import co.formulariosintomas.mb.WebService;
import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 *
 * @author Cesar
 */
public class VerificarTemperaturaJob implements Job {
    private final static Logger log = Logger.getLogger(VerificarTemperaturaJob.class);
    private WebService servicio = new WebService();
        @Override
        public void execute(JobExecutionContext ctx) throws JobExecutionException {
            log.info("Verificar temperatura");
            this.servicio.verificacionTemperatura();
        }
}
