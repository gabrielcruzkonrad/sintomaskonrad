/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.formulariosintomas.utils;

import co.formulariosintomas.bean.HistoriaTemperatura;
import co.formulariosintomas.bean.Respuesta;
import co.formulariosintomas.bean.Usuario;
import co.formulariosintomas.bean.UsuarioRol;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author cesard.chacond
 */
public class Singleton {

    private static Singleton miconfigurador;
    private HashMap<String, HashMap<String,Object>> session;
    private HashMap<String, HashMap<String,UsuarioRol>> activarFichet;
    private HashMap<String, List<Respuesta>> listado;
    
    public static Singleton getConfigurador() {

        if (miconfigurador == null) {
            miconfigurador = new Singleton();
        }
        return miconfigurador;
    }

    private Singleton() {
        session = new HashMap<>();
        activarFichet = new HashMap<>();
        listado = new HashMap<>();
    }

    public HashMap<String, HashMap<String, Object>> getSession() {
        return session;
    }

    public void setSession(HashMap<String, HashMap<String, Object>> session) {
        this.session = session;
    }

    public HashMap<String, HashMap<String, UsuarioRol>> getActivarFichet() {
        return activarFichet;
    }

    public void setActivarFichet(HashMap<String, HashMap<String, UsuarioRol>> activarFichet) {
        this.activarFichet = activarFichet;
    }

    public HashMap<String, List<Respuesta>> getListado() {
        return listado;
    }

    public void setListado(HashMap<String, List<Respuesta>> listado) {
        this.listado = listado;
    }
 
    
}
