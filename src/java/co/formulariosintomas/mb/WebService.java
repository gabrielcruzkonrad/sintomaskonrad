/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.formulariosintomas.mb;

import co.formulariosintomas.bean.HistoriaTemperatura;
import co.formulariosintomas.bean.RespuestaRecomendacion;
import co.formulariosintomas.bean.TipoIdentificacion;
import co.formulariosintomas.bean.Usuario;
import co.formulariosintomas.bean.UsuarioFichet;
import co.formulariosintomas.constantes.Constantes;
import co.formulariosintomas.servicios.Servicios;
import co.formulariosintomas.servicios.ServiciosImplementacion;
import co.formulariosintomas.utils.Singleton;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import org.springframework.stereotype.Controller;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.UnrecoverableKeyException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.json.Json;
import javax.json.JsonObject;
import javax.net.ssl.SSLContext;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.apache.http.ParseException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicNameValuePair;

/**
 *
 * @author David
 */
@Controller
public class WebService {

    /**
     * This is a sample web service operation
     *
     * @param email
     */
    private JSONObject jsonObject = new JSONObject();
    private String valor;
    private String identificacion;
    private String tipoPersona;
    private String tipoidentificacion;
    private String nombre;
    private String primerApellido;
    private String segundoApellido;
    private List<TipoIdentificacion> tipoIdentificacion;
    private HashMap<String, String> datosPersona;
    private HashMap<String, List<UsuarioFichet>> informacion;
    private Servicios servicios;
    private final static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(WebService.class);
    private JSONObject json;
    SimpleDateFormat sdformat = new SimpleDateFormat(Constantes.FORMATOFECHA);
    SimpleDateFormat sdformat2 = new SimpleDateFormat(Constantes.FORMATOFECHAHORA);
    SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    public static int PRETTY_PRINT_INDENT_FACTOR = 4;

    public WebService() {
        tipoPersona = "";
        servicios = new ServiciosImplementacion();
        datosPersona = new HashMap<>();
        informacion = new HashMap<>();
        tipoIdentificacion = new ArrayList<>();
        json = new JSONObject();
        tipoIdentificacion = servicios.obtenerTipoIdentifacion();
    }

    public HashMap ObtenerDatosPersonaServicio(String email) {
        try {
            this.datosPersona = new HashMap<>();
            if (this.obtenerDatosPersona(email)) {
                log.info(valor);
                log.info("Tengo URL web Service y valores");
            } else {
                log.info("Error al encontrar persona");
                this.datosPersona.put(Constantes.MENSAJE, "Error al obtener Service");
                return this.datosPersona;
            }
            log.info("Obtener Parametro Service");
            if (this.sacarParametro()) {
                log.info("Obtuve parametros Service");
                if (this.identificacion != null) {
                    this.datosPersona.put(Constantes.IDENTIFICACION, this.identificacion);
                    this.datosPersona.put(Constantes.TIPOPERSONA, this.tipoPersona);
                    this.datosPersona.put(Constantes.TIPOIDENTIFICACION, this.tipoidentificacion);
                    this.datosPersona.put(Constantes.PRIMERAPELLIDO, this.primerApellido);
                    this.datosPersona.put(Constantes.SEGUNDOAPELLIDO, this.segundoApellido);
                    this.datosPersona.put(Constantes.NOMBRE, this.nombre);
                } else {
                    //segunda verificacion en escolaris
                    if (this.obtenerDatosPersonaEscolaris(email)) {
                        if (this.sacarParametroEscolaris()) {
                            if (this.identificacion != null) {
                                this.datosPersona.put(Constantes.IDENTIFICACION, this.identificacion);
                                this.datosPersona.put(Constantes.TIPOPERSONA, this.tipoPersona);
                                this.datosPersona.put(Constantes.TIPOIDENTIFICACION, this.tipoidentificacion);
                                this.datosPersona.put(Constantes.ESCOLARISNOMBRE, this.nombre);
                                this.datosPersona.put(Constantes.ESCOLARISAPELLIDOS, this.primerApellido);
                            } else {
                                this.datosPersona.put(Constantes.MENSAJE, "Registro no encontrado");
                            }
                        } else {
                            log.info("Error al obtener Servicio");
                            this.datosPersona.put(Constantes.MENSAJE, "Error al obtener su identificación");
                        }
                    } else {
                        log.info("Error al encontrar persona");
                        this.datosPersona.put(Constantes.MENSAJE, "Error al obtener Service");
                        return this.datosPersona;
                    }
                }
            } else {
                log.info("Error al obtener Servicio");
                this.datosPersona.put(Constantes.MENSAJE, "Error al obtener su identificación");
            }
        } catch (KeyManagementException | KeyStoreException | NoSuchAlgorithmException | UnrecoverableKeyException ex) {
            log.info("Error web service" + ex.getMessage());
            this.datosPersona.put(Constantes.MENSAJE, "Error al obtener Service");
            return this.datosPersona;
        }
        return this.datosPersona;

    }

    private boolean sacarParametro() {
        try {
            Gson gson = new Gson();
            Map map = new HashMap();
            Map mapTemp = new HashMap();
            map = (Map) gson.fromJson(valor, map.getClass());
            List<Map> tmp = new ArrayList<>();
            Map datos = new HashMap();
            boolean est = false;
            boolean egresado = false;
            if (map.containsKey(Constantes.EGRESADO)) {
                tmp = (List<Map>) map.get(Constantes.EGRESADO);
                if (!tmp.isEmpty()) {
                    datos = tmp.get(0);
                    if (datos.containsKey(Constantes.IDENTIFICACION)) {
                        this.identificacion = (String) datos.get(Constantes.IDENTIFICACION);
                        egresado = true;
                        this.tipoPersona = Constantes.TIPOGRADUADO;
                        if (datos.containsKey(Constantes.TIPOIDENTIFICACION)) {
                            mapTemp = (Map) datos.get(Constantes.TIPOIDENTIFICACION);
                            this.tipoidentificacion = (String) mapTemp.get(Constantes.TIPOIDENTIFICACION);
                        }
                    }
                    if (datos.containsKey(Constantes.PRIMERAPELLIDO)) {
                        this.primerApellido = (String) datos.get(Constantes.PRIMERAPELLIDO);
                    }
                    if (datos.containsKey(Constantes.SEGUNDOAPELLIDO)) {
                        this.segundoApellido = (String) datos.get(Constantes.SEGUNDOAPELLIDO);
                    }
                    if (datos.containsKey(Constantes.NOMBRERAZONSOCIAL)) {
                        this.nombre = (String) datos.get(Constantes.NOMBRERAZONSOCIAL);
                    }
                }
            }
            if (map.containsKey(Constantes.ESTUDIANTE) && egresado == false) {
                tmp = (List<Map>) map.get(Constantes.ESTUDIANTE);
                if (!tmp.isEmpty()) {
                    datos = tmp.get(0);
                    if (datos.containsKey(Constantes.IDENTIFICACION)) {
                        this.identificacion = (String) datos.get(Constantes.IDENTIFICACION);
                        est = true;
                        this.tipoPersona = Constantes.TIPOESTUDIANTE;
                        if (datos.containsKey(Constantes.TIPOIDENTIFICACION)) {
                            mapTemp = (Map) datos.get(Constantes.TIPOIDENTIFICACION);
                            this.tipoidentificacion = (String) mapTemp.get(Constantes.TIPOIDENTIFICACION);
                        }
                    }
                    if (datos.containsKey(Constantes.NOMBRERAZONSOCIAL)) {
                        this.nombre = (String) datos.get(Constantes.NOMBRERAZONSOCIAL);
                    }
                    if (datos.containsKey(Constantes.PRIMERAPELLIDO)) {
                        this.primerApellido = (String) datos.get(Constantes.PRIMERAPELLIDO);
                    }
                    if (datos.containsKey(Constantes.SEGUNDOAPELLIDO)) {
                        this.segundoApellido = (String) datos.get(Constantes.SEGUNDOAPELLIDO);
                    }
                }

            }
            if (map.containsKey(Constantes.FUNCIONARIO)) {
                tmp = (List<Map>) map.get(Constantes.FUNCIONARIO);
                if (!tmp.isEmpty()) {
                    datos = tmp.get(0);
                    if (est == false && egresado == false) {
                        if (datos.containsKey(Constantes.IDENTIFICACIONNIT)) {
                            this.identificacion = (String) datos.get(Constantes.IDENTIFICACIONNIT);
                            this.tipoPersona = Constantes.TIPOFUNCIONARIO;
                            if (datos.containsKey(Constantes.TIPOIDENTIFICACION)) {
                                mapTemp = (Map) datos.get(Constantes.TIPOIDENTIFICACION);
                                this.tipoidentificacion = (String) mapTemp.get(Constantes.TIPOIDENTIFICACION);
                            }
                            if (datos.containsKey(Constantes.NOMBRE)) {
                                this.nombre = (String) datos.get(Constantes.NOMBRE);
                            }
                            if (datos.containsKey(Constantes.PRIMERAPELLIDO)) {
                                this.primerApellido = (String) datos.get(Constantes.PRIMERAPELLIDO);
                            }
                            if (datos.containsKey(Constantes.SEGUNDOAPELLIDO)) {
                                this.segundoApellido = (String) datos.get(Constantes.SEGUNDOAPELLIDO);
                            }
                        } else {
                            return false;
                        }
                    } else {
                        if (datos.containsKey(Constantes.IDENTIFICACIONNIT)) {
                            String nit = (String) datos.get(Constantes.IDENTIFICACIONNIT);
                            if (!(this.identificacion.equalsIgnoreCase(nit))) {
                                return false;
                            }
                            if (!(est && egresado)) {
                                this.tipoPersona = Constantes.TIPOESTUDIANTE;
                            } else {
                                this.tipoPersona = Constantes.TIPOFUNCIONARIO;
                            }
                            if (datos.containsKey(Constantes.PRIMERAPELLIDO)) {
                                this.primerApellido = (String) datos.get(Constantes.PRIMERAPELLIDO);
                            }
                            if (datos.containsKey(Constantes.SEGUNDOAPELLIDO)) {
                                this.segundoApellido = (String) datos.get(Constantes.SEGUNDOAPELLIDO);
                            }
                            if (datos.containsKey(Constantes.NOMBRE)) {
                                this.nombre = (String) datos.get(Constantes.NOMBRE);
                            }
                        } else {
                            return false;
                        }
                    }
                }

            }
            return true;
        } catch (JsonSyntaxException e) {
            log.info("Error JSON" + e.getMessage());
            return false;
        }
    }

    private boolean sacarParametroEscolaris() {
        try {
            Gson gson = new Gson();
            Map map = new HashMap();
            map = gson.fromJson(valor, map.getClass());
            this.identificacion = (String) map.get("identificacion");
            this.nombre = (String) map.get(Constantes.ESCOLARISNOMBRE);
            this.primerApellido = (String) map.get(Constantes.ESCOLARISAPELLIDOS);
            this.tipoidentificacion = (String) map.get(Constantes.TIPOIDENTIFICACION);
            this.tipoPersona = "E";
            return true;
        } catch (JsonSyntaxException e) {
            log.info("Error JSON" + e.getMessage());
            return false;
        }
    }

    public String obtenerToken(){
        try {
            SSLContext sslContext = SSLContext.getInstance("TLSv1.2");
            sslContext.init(null, null, new SecureRandom());
            
            SSLConnectionSocketFactory f = new SSLConnectionSocketFactory(sslContext, new String[]{"TLSv1.2"}, null,
                    SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
            
            Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory>create()
                    .register("http", PlainConnectionSocketFactory.getSocketFactory())
                    .register("https", f)
                    .build();
            
            PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager(socketFactoryRegistry);
            
            CloseableHttpClient client = HttpClients
                    .custom()
                    .setSSLSocketFactory(f)
                    .setConnectionManager(cm)
                    .build();
            
            HttpPost tokenRequest = new HttpPost("http://192.168.14.6:60009/api/Token");
            List<NameValuePair> data =new ArrayList<NameValuePair>();
            data.add(new BasicNameValuePair("grant_type", "client_credentials"));
            data.add(new BasicNameValuePair("client_id", "23609829-E683-435B-9191-B8698A0E881D"));
            data.add(new BasicNameValuePair("client_secret", "9854"));
            data.add(new BasicNameValuePair("Scope", "Scope1"));
            tokenRequest.setEntity(new UrlEncodedFormEntity(data));
            CloseableHttpResponse tokenResponse = client.execute(tokenRequest);
            
            JsonObject tokenObject = Json.createReader(tokenResponse.getEntity().getContent()).readObject();
            String accestoken = tokenObject.getString("access_token");
            Integer expiresIn = tokenObject.getInt("expires_in");
            String token_type = tokenObject.getString("token_type");
            
            log.info("token: " +accestoken);
            log.info("tiempo: " +expiresIn);
            log.info("token_type: " +token_type);
            
            return accestoken;
        } catch (KeyManagementException | IOException | NoSuchAlgorithmException ex) {
            log.info(ex.getMessage());
        }
        return "#";
    }
    public boolean obtenerDatosPersona(String email) throws KeyManagementException,
            UnrecoverableKeyException, NoSuchAlgorithmException, KeyStoreException {
        SSLContext sslContext = SSLContext.getInstance("TLSv1.2");
        sslContext.init(null, null, new SecureRandom());

        SSLConnectionSocketFactory f = new SSLConnectionSocketFactory(sslContext, new String[]{"TLSv1.2"}, null,
                SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

        Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory>create()
                .register("http", PlainConnectionSocketFactory.getSocketFactory())
                .register("https", f)
                .build();

        PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager(socketFactoryRegistry);

        CloseableHttpClient client = HttpClients
                .custom()
                .setSSLSocketFactory(f)
                .setConnectionManager(cm)
                .build();

        HttpGet request = new HttpGet(Constantes.URLGETBYEMAIL + email);
        CloseableHttpResponse response;
        try {
            response = client.execute(request);
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                // return it as a String
                String result = EntityUtils.toString(entity);
                valor = result;
                log.info("Paso 4" + result);
                try {
                    jsonObject = new JSONObject(valor);
                    log.info("json " + jsonObject);
                    return true;
                } catch (JSONException ex) {
                    log.info("Error parse " + ex.getMessage());
                    this.datosPersona.put(Constantes.MENSAJE, "Error al obtener Service JSON");
                }
            }
        } catch (IOException ex) {
            log.info("Error servicio " + ex.getMessage());
        }
        return false;
    }

    public boolean obtenerDatosPersonaEscolaris(String email) throws KeyManagementException,
            UnrecoverableKeyException, NoSuchAlgorithmException, KeyStoreException {
        SSLContext sslContext = SSLContext.getInstance("TLSv1.2");
        sslContext.init(null, null, new SecureRandom());

        SSLConnectionSocketFactory f = new SSLConnectionSocketFactory(sslContext, new String[]{"TLSv1.2"}, null,
                SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

        Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory>create()
                .register("http", PlainConnectionSocketFactory.getSocketFactory())
                .register("https", f)
                .build();

        PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager(socketFactoryRegistry);

        CloseableHttpClient client = HttpClients
                .custom()
                .setSSLSocketFactory(f)
                .setConnectionManager(cm)
                .build();

        HttpGet request = new HttpGet(Constantes.URLESCOLARISGETBYEMAIL + email);
        log.info("Paso 1");
        CloseableHttpResponse response;
        try {
            response = client.execute(request);
            log.info("Paso 2");
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                log.info("Paso 3");
                // return it as a String
                String result = EntityUtils.toString(entity);
                valor = result;
                log.info("Paso 4" + result);
                try {
                    jsonObject = new JSONObject(valor);
                    log.info("json " + jsonObject);
                    return true;
                } catch (JSONException ex) {
                    log.info("Error parse " + ex.getMessage());
                    this.datosPersona.put(Constantes.MENSAJE, "Error al obtener Service JSON");
                }
            }
            log.info("Paso 2.1");
        } catch (IOException ex) {
            log.info("Error servicio " + ex.getMessage());
        }
        return false;
    }

    public boolean obtenerHorarioEstudiante(String documento, String diaSeleccionado) {
        try {
            SSLContext sslContext = SSLContext.getInstance("TLSv1.2");
            sslContext.init(null, null, new SecureRandom());

            SSLConnectionSocketFactory f = new SSLConnectionSocketFactory(sslContext, new String[]{"TLSv1.2"}, null,
                    SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

            Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory>create()
                    .register("http", PlainConnectionSocketFactory.getSocketFactory())
                    .register("https", f)
                    .build();

            PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager(socketFactoryRegistry);

            CloseableHttpClient client = HttpClients
                    .custom()
                    .setSSLSocketFactory(f)
                    .setConnectionManager(cm)
                    .build();

            HttpGet request = new HttpGet(Constantes.URLGETBYDOCUMENTO + documento);
            CloseableHttpResponse response;
            response = client.execute(request);
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                // return it as a String
                String result = EntityUtils.toString(entity);
                valor = result;
                return this.ObtenerDiasClase(diaSeleccionado);
            }
        } catch (KeyManagementException | NoSuchAlgorithmException ex) {
            log.info("Error web service" + ex.getMessage());
        } catch (IOException ex) {
            log.info(ex.getMessage());
        }
        return false;
    }

    private boolean ObtenerDiasClase(String diaSeleccionado) {
        try {
            Gson gson = new Gson();
            List<Map> map = new ArrayList<Map>();
            map = gson.fromJson(valor, map.getClass());
            for (Map temp : map) {
                String descripcionDia = (String) temp.get("descripcionDia");
                if (descripcionDia.equalsIgnoreCase(diaSeleccionado)) {
                    return true;
                }
            }
            return false;
        } catch (JsonSyntaxException e) {
            log.info("Error consultar dia clase Estudiante" + e.getMessage());
            return false;
        }
    }

    public HashMap obtenerSNFuncionario(String email) {
        try {
            this.datosPersona = new HashMap<>();
            SSLContext sslContext = SSLContext.getInstance("TLSv1.2");
            sslContext.init(null, null, new SecureRandom());

            SSLConnectionSocketFactory f = new SSLConnectionSocketFactory(sslContext, new String[]{"TLSv1.2"}, null,
                    SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

            Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory>create()
                    .register("http", PlainConnectionSocketFactory.getSocketFactory())
                    .register("https", f)
                    .build();

            PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager(socketFactoryRegistry);

            CloseableHttpClient client = HttpClients
                    .custom()
                    .setSSLSocketFactory(f)
                    .setConnectionManager(cm)
                    .build();

            HttpGet request = new HttpGet(Constantes.URLGETBYEMAIL + email);
            CloseableHttpResponse response;
            try {
                response = client.execute(request);
                HttpEntity entity = response.getEntity();
                if (entity != null) {
                    // return it as a String
                    String result = EntityUtils.toString(entity);
                    valor = result;
                    try {
                        jsonObject = new JSONObject(valor);
                        Gson gson = new Gson();
                        Map map = new HashMap();
                        Map mapTemp = new HashMap();
                        map = (Map) gson.fromJson(valor, map.getClass());
                        List<Map> tmp = new ArrayList<>();
                        if (map.containsKey(Constantes.FUNCIONARIO)) {
                            tmp = (List<Map>) map.get(Constantes.FUNCIONARIO);
                            if (!tmp.isEmpty() && !tmp.get(0).isEmpty()) {
                                this.datosPersona.put("valor", "true");
                                return this.datosPersona;
                            }
                        }
                    } catch (JSONException ex) {
                        this.datosPersona.put(Constantes.MENSAJE, "Error al obtener Service JSON");
                    }
                }
            } catch (IOException ex) {
                this.datosPersona.put(Constantes.MENSAJE, "Error al obtener Service JSON");
            }
        } catch (NoSuchAlgorithmException | KeyManagementException ex) {
            this.datosPersona.put(Constantes.MENSAJE, "Error al obtener Service JSON");
        }
        return this.datosPersona;
    }

    public boolean obtenerSNEgresado(String email) {
        try {
            SSLContext sslContext = SSLContext.getInstance("TLSv1.2");
            sslContext.init(null, null, new SecureRandom());

            SSLConnectionSocketFactory f = new SSLConnectionSocketFactory(sslContext, new String[]{"TLSv1.2"}, null,
                    SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

            Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory>create()
                    .register("http", PlainConnectionSocketFactory.getSocketFactory())
                    .register("https", f)
                    .build();

            PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager(socketFactoryRegistry);

            CloseableHttpClient client = HttpClients
                    .custom()
                    .setSSLSocketFactory(f)
                    .setConnectionManager(cm)
                    .build();

            HttpGet request = new HttpGet(Constantes.URLGETBYEMAIL + email);
            CloseableHttpResponse response;
            try {
                response = client.execute(request);
                HttpEntity entity = response.getEntity();
                if (entity != null) {
                    // return it as a String
                    String result = EntityUtils.toString(entity);
                    valor = result;
                    try {
                        jsonObject = new JSONObject(valor);
                        Gson gson = new Gson();
                        Map map = new HashMap();
                        map = (Map) gson.fromJson(valor, map.getClass());
                        List<Map> tmp = new ArrayList<>();
                        if (map.containsKey(Constantes.EGRESADO)) {
                            tmp = (List<Map>) map.get(Constantes.EGRESADO);
                            if (!tmp.isEmpty() && !tmp.get(0).isEmpty()) {
                                return true;
                            }
                        }
                    } catch (JSONException ex) {
                        this.datosPersona.put(Constantes.MENSAJE, "Error al obtener Service JSON");
                    }
                }
            } catch (IOException ex) {
                this.datosPersona.put(Constantes.MENSAJE, "Error al obtener Service JSON");
            }
        } catch (NoSuchAlgorithmException | KeyManagementException ex) {
            this.datosPersona.put(Constantes.MENSAJE, "Error al obtener Service JSON");
        }
        return false;
    }

    //fichet
    public boolean enviarEntradaUsuario(Usuario usuario, Date activacion) {
        try {
            //si es administrador debo consultar tipo identificacion
            if (usuario.getTipoUsuario().equalsIgnoreCase("X")) {
                this.identificacion = usuario.getIdentificacion();
                this.tipoidentificacion = usuario.getTipoIdentificacionC().getCodigo();
            } else {
                this.datosPersona = new HashMap<>();
                this.datosPersona = ObtenerDatosPersonaServicio(usuario.getEmail());
                String apellido = "";
                if (this.datosPersona.containsKey(Constantes.MENSAJE)) {
                    return false;
                } else {
                    if (this.datosPersona.containsKey(Constantes.ESCOLARISNOMBRE)) {
                        this.identificacion = this.datosPersona.get(Constantes.IDENTIFICACION);
                        this.nombre = this.datosPersona.get(Constantes.ESCOLARISNOMBRE);
                        apellido = this.datosPersona.get(Constantes.ESCOLARISAPELLIDOS);
                        this.tipoidentificacion = sacarTipoIdentificacion(this.datosPersona.get(Constantes.TIPOIDENTIFICACION));
                    } else {
                        this.identificacion = this.datosPersona.get(Constantes.IDENTIFICACION);
                        this.nombre = this.datosPersona.get(Constantes.NOMBRE);
                        apellido = this.datosPersona.get(Constantes.PRIMERAPELLIDO) + " " + this.datosPersona.get(Constantes.SEGUNDOAPELLIDO);
                        this.tipoidentificacion = sacarTipoIdentificacion(this.datosPersona.get(Constantes.TIPOIDENTIFICACION));
                    }
                }
            }

            String textInicio = sdformat.format(activacion);

            SSLContext sslContext = SSLContext.getInstance("TLSv1.2");
            sslContext.init(null, null, new SecureRandom());

            SSLConnectionSocketFactory f = new SSLConnectionSocketFactory(sslContext, new String[]{"TLSv1.2"}, null,
                    SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

            Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory>create()
                    .register("http", PlainConnectionSocketFactory.getSocketFactory())
                    .register("https", f)
                    .build();

            PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager(socketFactoryRegistry);

            CloseableHttpClient client = HttpClients
                    .custom()
                    .setSSLSocketFactory(f)
                    .setConnectionManager(cm)
                    .build();

            HttpPost httppost = new HttpPost(Constantes.ACTIVARUSUARIO);
//            httppost.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
//            httppost.addHeader(HttpHeaders.AUTHORIZATION, "Bearer " + "cSJK0uZoa4xLUB9MQWlKGSho7Fuzd8KRxDHj8OLwCmWDFgBvoK5lydMdA5zVOS7V57MNsnanoB6PsOZjM0g45FkXR3gde7flSq6I5lqOKDEzcC-4pcAHnXi_R0GA278-de1NM9aBUuIHc8I8O4difqvSw6nTBFzgqsBcVcDAzcN0g_ourXHvWI9flajM-plJgcpucv__OLt-SZKBQXZLfw");
            CloseableHttpResponse response;

//            List<NameValuePair> params = new ArrayList<NameValuePair>(2);
//            params.add(new BasicNameValuePair("identificacion", this.identificacion));
//            params.add(new BasicNameValuePair("nombres", this.nombre));
//            params.add(new BasicNameValuePair("apellidos", apellido));
//            params.add(new BasicNameValuePair("tipo_identificacion", this.tipoidentificacion));
//            params.add(new BasicNameValuePair("fecha_activacion", textInicio));
//            params.add(new BasicNameValuePair("fecha_expiracion", textInicio));
//            httppost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
            String token = this.obtenerToken();
            StringEntity params = new StringEntity(
                    "{"
                    + "    \"OperType\": 0,"
                    + "    \"Staff\": { "
                    + "        \"StaffNo\": \"" + this.tipoidentificacion.trim() + this.identificacion.trim() + "\","
                    + "        \"OrganizationId\": \"2CA922CA-4C69-4011-80DD-151555D1A0AB\","
                    + "        \"Gid\": \"CF295F43-4FD1-484F-B3D6-D9B0885204CB\""
                    + "    },"
                    + "    \"BeginTime\": \"" + textInicio + "\","
                    + "    \"EndTime\": \"" + textInicio + "\","
                    + "    \"AuthorizeDeviceNo\": [1,2,3,4],"
                    + "    \"ProjectGids\": [\"CF295F43-4FD1-484F-B3D6-D9B0885204CB\"]"
                    + "}"
            );

            httppost.addHeader("content-type", "application/json; charset=utf-8");
            httppost.addHeader("Authorization", "Bearer "+ token);
            httppost.setEntity(params);
            response = client.execute(httppost);
            HttpEntity entity = response.getEntity();
            if (response.getStatusLine().getStatusCode() == 200) {
                if (entity != null) {
                    String result = EntityUtils.toString(entity);
                    //                    JSONObject xmlJSONObj = XML.toJSONObject(result);
//                    String jsonPrettyPrintString = xmlJSONObj.toString(PRETTY_PRINT_INDENT_FACTOR);
//                    System.out.println(jsonPrettyPrintString);
                    Gson gson = new Gson();
                    Map map = new HashMap();
                    map = (Map) gson.fromJson(result, map.getClass());
                    if (map.containsKey(Constantes.STATE)) {
                        String state = map.get(Constantes.STATE).toString();
                        if (state.equalsIgnoreCase(Constantes.OK)) {
                            this.actualizarUsuario();
                            return true;
                        } else {
                            if (this.actualizarEntradaUsuario(textInicio)) {
                                this.actualizarUsuario();
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        } catch (KeyManagementException | NoSuchAlgorithmException | UnsupportedEncodingException ex) {
            log.info("Error Al Activar Usuario " + ex.getMessage());
            return false;
        } catch (IOException ex) {
            log.info("Error al activar Usuario " + ex.getMessage());
            return false;
        }
    }

    //fichet
    public boolean actualizarEntradaUsuario(String textInicio) {
        try {

            SSLContext sslContext = SSLContext.getInstance("TLSv1.2");
            sslContext.init(null, null, new SecureRandom());

            SSLConnectionSocketFactory f = new SSLConnectionSocketFactory(sslContext, new String[]{"TLSv1.2"}, null,
                    SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

            Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory>create()
                    .register("http", PlainConnectionSocketFactory.getSocketFactory())
                    .register("https", f)
                    .build();

            PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager(socketFactoryRegistry);

            CloseableHttpClient client = HttpClients
                    .custom()
                    .setSSLSocketFactory(f)
                    .setConnectionManager(cm)
                    .build();

            HttpPost httppost = new HttpPost(Constantes.MODIFICARUSUARIO);
//            httppost.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
//            httppost.addHeader(HttpHeaders.AUTHORIZATION, "Bearer " + "cSJK0uZoa4xLUB9MQWlKGSho7Fuzd8KRxDHj8OLwCmWDFgBvoK5lydMdA5zVOS7V57MNsnanoB6PsOZjM0g45FkXR3gde7flSq6I5lqOKDEzcC-4pcAHnXi_R0GA278-de1NM9aBUuIHc8I8O4difqvSw6nTBFzgqsBcVcDAzcN0g_ourXHvWI9flajM-plJgcpucv__OLt-SZKBQXZLfw");
            CloseableHttpResponse response;

//            List<NameValuePair> params = new ArrayList<NameValuePair>(2);
//            params.add(new BasicNameValuePair("identificacion", this.identificacion));
//            params.add(new BasicNameValuePair("nombres", this.nombre));
//            params.add(new BasicNameValuePair("apellidos", apellido));
//            params.add(new BasicNameValuePair("tipo_identificacion", this.tipoidentificacion));
//            params.add(new BasicNameValuePair("fecha_activacion", textInicio));
//            params.add(new BasicNameValuePair("fecha_expiracion", textInicio));
//            httppost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
            String token = this.obtenerToken();
            StringEntity params = new StringEntity(
                    "{"
                    + "    \"OperType\": 1,"
                    + "    \"Staff\": { "
                    + "        \"StaffNo\": \"" + this.tipoidentificacion.trim() + this.identificacion.trim() + "\","
                    + "        \"OrganizationId\": \"2CA922CA-4C69-4011-80DD-151555D1A0AB\","
                    + "        \"Gid\": \"CF295F43-4FD1-484F-B3D6-D9B0885204CB\""
                    + "    },"
                    + "    \"BeginTime\": \"" + textInicio + "\","
                    + "    \"EndTime\": \"" + textInicio + "\","
                    + "    \"AuthorizeDeviceNo\": [1,2,3,4],"
                    + "    \"ProjectGids\": [\"CF295F43-4FD1-484F-B3D6-D9B0885204CB\"]"
                    + "}"
            );

            httppost.addHeader("content-type", "application/json; charset=utf-8");
                        httppost.addHeader("Authorization", "Bearer "+token);
            httppost.setEntity(params);
            response = client.execute(httppost);
            HttpEntity entity = response.getEntity();
            if (response.getStatusLine().getStatusCode() == 200) {
                if (entity != null) {
                    String result = EntityUtils.toString(entity);
                    //                    JSONObject xmlJSONObj = XML.toJSONObject(result);
//                    String jsonPrettyPrintString = xmlJSONObj.toString(PRETTY_PRINT_INDENT_FACTOR);
//                    System.out.println(jsonPrettyPrintString);
                    Gson gson = new Gson();
                    Map map = new HashMap();
                    map = (Map) gson.fromJson(result, map.getClass());
                    if (map.containsKey(Constantes.STATE)) {
                        String state = map.get(Constantes.STATE).toString();
                        return state.equalsIgnoreCase(Constantes.OK);
                    }
                }
            }
            return false;
        } catch (KeyManagementException | NoSuchAlgorithmException | UnsupportedEncodingException ex) {
            log.info("Error Al actualizar Usuario " + ex.getMessage());
            return false;
        } catch (IOException ex) {
            log.info("Error al actualizar Usuario " + ex.getMessage());
            return false;
        }
    }

    public void actualizarUsuario() {
        try {

            SSLContext sslContext = SSLContext.getInstance("TLSv1.2");
            sslContext.init(null, null, new SecureRandom());

            SSLConnectionSocketFactory f = new SSLConnectionSocketFactory(sslContext, new String[]{"TLSv1.2"}, null,
                    SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

            Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory>create()
                    .register("http", PlainConnectionSocketFactory.getSocketFactory())
                    .register("https", f)
                    .build();

            PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager(socketFactoryRegistry);

            CloseableHttpClient client = HttpClients
                    .custom()
                    .setSSLSocketFactory(f)
                    .setConnectionManager(cm)
                    .build();

            HttpPost httppost = new HttpPost(Constantes.ACTUALIZARLISTADOS);
//            httppost.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
//            httppost.addHeader(HttpHeaders.AUTHORIZATION, "Bearer " + "cSJK0uZoa4xLUB9MQWlKGSho7Fuzd8KRxDHj8OLwCmWDFgBvoK5lydMdA5zVOS7V57MNsnanoB6PsOZjM0g45FkXR3gde7flSq6I5lqOKDEzcC-4pcAHnXi_R0GA278-de1NM9aBUuIHc8I8O4difqvSw6nTBFzgqsBcVcDAzcN0g_ourXHvWI9flajM-plJgcpucv__OLt-SZKBQXZLfw");
            CloseableHttpResponse response;

//            List<NameValuePair> params = new ArrayList<NameValuePair>(2);
//            params.add(new BasicNameValuePair("identificacion", this.identificacion));
//            params.add(new BasicNameValuePair("nombres", this.nombre));
//            params.add(new BasicNameValuePair("apellidos", apellido));
//            params.add(new BasicNameValuePair("tipo_identificacion", this.tipoidentificacion));
//            params.add(new BasicNameValuePair("fecha_activacion", textInicio));
//            params.add(new BasicNameValuePair("fecha_expiracion", textInicio));
//            httppost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
            String token = this.obtenerToken();
            StringEntity params = new StringEntity(
                    "{"

                    + "}"
            );

            httppost.addHeader("content-type", "application/json; charset=utf-8");
            httppost.addHeader("Authorization", "Bearer "+ token);
            httppost.setEntity(params);
            response = client.execute(httppost);
            HttpEntity entity = response.getEntity();
            if (response.getStatusLine().getStatusCode() == 200) {
                log.info("Actualizacion correcta torniquetes ");
            }
        } catch (KeyManagementException | NoSuchAlgorithmException | UnsupportedEncodingException ex) {
            log.info(ex.getMessage());
        } catch (IOException ex) {
            log.info(ex.getMessage());
        }
    }

    private String sacarTipoIdentificacion(String codigo) {
        for (TipoIdentificacion ti : this.tipoIdentificacion) {
            if (ti.getCodigoEquivalente().equalsIgnoreCase(codigo) || ti.getCodigo().equalsIgnoreCase(codigo)) {
                return ti.getCodigo();
            }
        }
        return null;
    }

    //activacion todas las noches
    public void activarPersonasNoche() {
        Singleton session = Singleton.getConfigurador();
        session.getSession().clear();
        List<RespuestaRecomendacion> personas = new ArrayList<>();
        personas = this.servicios.obtenerPersonasDia();
        for (RespuestaRecomendacion p : personas) {
            if (p.getIdRespuesta().getProcesado().trim().equalsIgnoreCase("N")) {
                if (!enviarEntradaUsuario(p.getIdRespuesta().getIdUsuario(), new Date())) {
                    log.info("Error al Activar persona " + p.getIdRespuesta().getIdUsuario().getUsuario() + "Identificacion " + p.getIdRespuesta().getIdUsuario().getIdentificacion());
                } else {
                    if (!this.servicios.actualizarProcesoActivacion(p.getIdRespuesta())) {
                        log.info("Error al actualizar " + p.getIdRespuesta().getIdUsuario().getUsuario() + "Identificacion " + p.getIdRespuesta().getIdUsuario().getIdentificacion());
                    }
                }
            }
        }
    }

    //activacion de persona con encuesta
    public boolean activarPersonasEncuesta(Usuario usuario, Date activacion) {
        if (!enviarEntradaUsuario(usuario, activacion)) {
            log.info("Error al Activar persona " + usuario.getUsuario() + "Identificacion " + usuario.getIdentificacion());
            return false;
        }
        return true;
    }

    //fichet
    public HashMap listadoPersonas(Date fechaInicio, Date fechafinal) {
        informacion = new HashMap<>();
        try {
            String textInicio = sdformat.format(fechaInicio);
            String textFinal = sdformat.format(fechafinal);

            SSLContext sslContext = SSLContext.getInstance("TLSv1.2");
            sslContext.init(null, null, new SecureRandom());

            SSLConnectionSocketFactory f = new SSLConnectionSocketFactory(sslContext, new String[]{"TLSv1.2"}, null,
                    SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

            Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory>create()
                    .register("http", PlainConnectionSocketFactory.getSocketFactory())
                    .register("https", f)
                    .build();

            PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager(socketFactoryRegistry);

            CloseableHttpClient client = HttpClients
                    .custom()
                    .setSSLSocketFactory(f)
                    .setConnectionManager(cm)
                    .build();

            HttpPost request = new HttpPost(Constantes.LISTADOUSUARIO);
            //request.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
//            request.setHeader(HttpHeaders.AUTHORIZATION, "Bearer " + Constantes.AUTENTICACIONFICHET);
            CloseableHttpResponse response;
            String token = this.obtenerToken();
            StringEntity params = new StringEntity(
                    "{"
                    + "    \"PageSize\": 10000,"
                    + "    \"CurrentPage\": 1,"
                    + "    \"OrderBy\": \"OpenDate\","
                    + "    \"OrderType\": true,"
                    + "    \"where\": \"OpenDate BETWEEN '" + textInicio + " 00:00:00'AND '" + textFinal + " 23:55:00'\""     
                    + "}"
            );

            request.addHeader("content-type", "application/json; charset=utf-8");
            request.addHeader("Authorization", "Bearer "+ token);
            request.setEntity(params);
            try {
                response = client.execute(request);
                HttpEntity entity = response.getEntity();
                if (entity != null) {
                    String result = EntityUtils.toString(entity);
                    if (response.getStatusLine().getStatusCode() == 200) {
                        try {
//                        JSONObject xmlJSONObj = XML.toJSONObject(result);
//                        String jsonPrettyPrintString = xmlJSONObj.toString(PRETTY_PRINT_INDENT_FACTOR);
//                        System.out.println(jsonPrettyPrintString);
                            Gson gson = new Gson();
                            Map map = new HashMap();
                            map = (Map) gson.fromJson(result, map.getClass());
                            List<Map> tmp = new ArrayList<>();
                            if (map.containsKey(Constantes.RECORDS)) {
                                tmp = (List<Map>) map.get(Constantes.RECORDS);
                                for (Map t : tmp) {
                                    String temperatura = t.get(Constantes.BIOTEMPERATURA).toString();
                                    String identificacions = t.get(Constantes.BIOIDENTIFICACION).toString();
                                    String fecha = t.get(Constantes.BIOFECHAMARCACION).toString();
                                    Date fechaa = input.parse(fecha);
                                    String device = t.get(Constantes.BIODEVICE).toString();
                                    String[] parte = this.sacarParametrosServicio(identificacions);
                                    String tipo = parte[0];
                                    try {
                                        identificacions = parte[1];
                                    } catch (NullPointerException e) {
                                        identificacions = parte[0];
                                    }

                                    UsuarioFichet uf = new UsuarioFichet(identificacions, tipo, fechaa, temperatura, device);
                                    if (informacion.containsKey(identificacions)) {
                                        informacion.get(identificacions).add(uf);
                                    } else {
                                        informacion.put(identificacions, new ArrayList<>());
                                        informacion.get(identificacions).add(uf);
                                    }

                                }
                            }

                        } catch (java.text.ParseException je) {
                            log.info(je.getMessage());
                        }
                    }
                }
            } catch (IOException ex) {
                log.info("Error al consultar servicio " + ex.getMessage());
                return informacion;
            } catch (ParseException ex) {
                log.info(ex.getMessage());
            }
            return informacion;
        } catch (NoSuchAlgorithmException | KeyManagementException | UnsupportedEncodingException ex) {
            log.info("Error al consultar servicio " + ex.getMessage());
            return informacion;
        }
    }

    public String[] sacarParametrosServicio(String parametro) {
        String[] parts = parametro.split("A");
        String[] parts2 = new String[2];
        if (!(parts.length > 1)) {
            parts = parametro.split("I");
            if (!(parts.length > 1)) {
                parts = parametro.split("E");
                if (!(parts.length > 1)) {
                    parts = parametro.split("C");
                    if (parts.length > 1) {
                        parts2[0] = "CC";
                        parts2[1] = parts[2];
                    }
                } else {
                    parts2[0] = "CE";
                    parts2[1] = parts[1];
                }
            } else {
                parts2[0] = "TI";
                parts2[1] = parts[1];
            }
        } else {
            parts2[0] = "PA";
            parts2[1] = parts[1];
        }
        return parts2;
    }

    public void verificacionTemperatura() {
        informacion = this.listadoPersonas(new Date(), new Date());
        if (!informacion.isEmpty()) {
            List<RespuestaRecomendacion> personas = new ArrayList<>();
            personas = this.servicios.obtenerPersonasDia();
            for (RespuestaRecomendacion temp : personas) {
                try {
                    if (informacion.containsKey(temp.getIdRespuesta().getIdUsuario().getIdentificacion())) {
                        if (informacion.get(temp.getIdRespuesta().getIdUsuario().getIdentificacion()).size() > 1) {
                            for (int x = 0; x < informacion.get(temp.getIdRespuesta().getIdUsuario().getIdentificacion()).size(); x++) {
                                for (int i = 0; i < informacion.get(temp.getIdRespuesta().getIdUsuario().getIdentificacion()).size() - x - 1; i++) {
                                    if (informacion.get(temp.getIdRespuesta().getIdUsuario().getIdentificacion()).get(i + 1).getFechaMarcacion().compareTo(
                                            informacion.get(temp.getIdRespuesta().getIdUsuario().getIdentificacion()).get(i).getFechaMarcacion()) < 0) {
                                        UsuarioFichet tmp = informacion.get(temp.getIdRespuesta().getIdUsuario().getIdentificacion()).get(i + 1);
                                        informacion.get(temp.getIdRespuesta().getIdUsuario().getIdentificacion()).set(i + 1, informacion.get(temp.getIdRespuesta().getIdUsuario().getIdentificacion()).get(i));
                                        informacion.get(temp.getIdRespuesta().getIdUsuario().getIdentificacion()).set(i, tmp);
                                    }
                                }
                            }
                        }
                        for (UsuarioFichet us : informacion.get(temp.getIdRespuesta().getIdUsuario().getIdentificacion())) {
                            List<HistoriaTemperatura> ht = new ArrayList<>();
                            ht = servicios.obtenerTemperaturaByDocumento(temp.getIdRespuesta().getIdUsuario().getIdentificacion());
                            if (ht.isEmpty() && ht.size() == 0) {
                                HistoriaTemperatura guardarTemp = new HistoriaTemperatura(us, temp.getIdRespuesta());
                                if (!servicios.guardarTemperatura(guardarTemp)) {
                                    log.info("Error al guardar temperatura " + temp.getIdRespuesta().getIdUsuario().getIdentificacion());
                                }
                            } else {
                                String fechaCreacion = input.format(ht.get(0).getFechacreacion());
                                ht.get(0).setFechacreacion(input.parse(fechaCreacion));
                                if (us.getFechaMarcacion().compareTo(ht.get(0).getFechacreacion()) > 0) {
                                    HistoriaTemperatura guardarTemp = new HistoriaTemperatura(us, temp.getIdRespuesta());
                                    if (!servicios.guardarTemperatura(guardarTemp)) {
                                        log.info("Error al guardar temperatura " + temp.getIdRespuesta().getIdUsuario().getIdentificacion());
                                    }
                                }
                            }
                        }
                    }
                } catch (java.text.ParseException ex) {
                    log.info(ex.getMessage());
                }
            }
        }
    }

    public String getTipoidentificacion() {
        return tipoidentificacion;
    }

    public void setTipoidentificacion(String tipoidentificacion) {
        this.tipoidentificacion = tipoidentificacion;
    }

    public JSONObject getJsonObject() {
        return jsonObject;
    }

    public void setJsonObject(JSONObject jsonObject) {
        this.jsonObject = jsonObject;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getTipoPersona() {
        return tipoPersona;
    }

    public void setTipoPersona(String tipoPersona) {
        this.tipoPersona = tipoPersona;
    }

    public HashMap<String, String> getDatosPersona() {
        return datosPersona;
    }

    public void setDatosPersona(HashMap<String, String> datosPersona) {
        this.datosPersona = datosPersona;
    }

    public HashMap<String, List<UsuarioFichet>> getInformacion() {
        return informacion;
    }

    public void setInformacion(HashMap<String, List<UsuarioFichet>> informacion) {
        this.informacion = informacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPrimerApellido() {
        return primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public String getSegundoApellido() {
        return segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public JSONObject getJson() {
        return json;
    }

    public void setJson(JSONObject json) {
        this.json = json;
    }

    public List<TipoIdentificacion> getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    public void setTipoIdentificacion(List<TipoIdentificacion> tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

}
