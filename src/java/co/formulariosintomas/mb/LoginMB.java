/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.formulariosintomas.mb;

import co.formulariosintomas.bean.EncabezadoRecomendaciones;
import co.formulariosintomas.bean.Usuario;
import co.formulariosintomas.bean.UsuarioRol;
import co.formulariosintomas.constantes.Constantes;
import co.formulariosintomas.servicios.Servicios;
import co.formulariosintomas.servicios.ServiciosImplementacion;
import co.formulariosintomas.utils.Utilidades;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.apache.log4j.Logger;

/**
 *
 * @author David Chacon
 */
@ManagedBean
@ViewScoped
@WebServlet(name = "LoginMB")
@Controller
public class LoginMB {

    /**
     * Creates a new instance of FormularioSintomasMB
     */
    private final static Logger log = Logger.getLogger(LoginMB.class);
    private boolean loginRendered;
    private boolean registroExternosRendered;
    private boolean ingresoExternosRendered;
    private String URL;
    private Servicios servicios;

    //registro externos
    private String nombres;
    private String apellidos;
    private Usuario persona;
    private Usuario personaTemp;
    private Utilidades util;
    //singleton
    HttpSession session;
    private WebService service;

    @PostConstruct
    public void init() {
        servicios = new ServiciosImplementacion();
//        service = new WebService();
//        service.obtenerToken();
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        HttpServletResponse res = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
        this.URL = Constantes.INICIOAPLICACION;
        this.loginRendered = true;
        this.registroExternosRendered = false;
        this.ingresoExternosRendered = false;

        //registro externos
        this.nombres = "";
        this.apellidos = "";
        this.persona = new Usuario();
        this.personaTemp = new Usuario();
    }

    public String guardarRegistroExternos() {
        try {
            if (this.apellidos != null && this.nombres != null) {
                if (this.persona.getEmail() != null && this.persona.getContrasena() != null && this.persona.getIdentificacion() != null) {
                    this.persona.setIdentificacion(this.persona.getIdentificacion().trim());
                    this.persona.setEmail(this.persona.getEmail().trim());
                    this.persona.setUsuario(nombres.toLowerCase().trim() + " " + apellidos.toLowerCase().trim());
                    this.persona.setEstado(Constantes.ESTADOACTIVO);
                    this.persona.setTipoUsuario(Constantes.TIPOEXTERNO);
                    Utilidades encriptar = new Utilidades();
                    this.persona.setContrasena(encriptar.Encriptar(this.persona.getContrasena()));
                    UsuarioRol usuario = new UsuarioRol();
                    usuario.setIdUsuario(persona);
                    if (this.servicios.guardarPersonaExterna(usuario)) {
                        this.ingresoExternosRendered = true;
                        this.registroExternosRendered = false;
                        this.persona = new Usuario();
                        this.apellidos = new String();
                        this.nombres = new String();
                    } else {
                        //error al guardar
                        UsuarioRol usuarioD = new UsuarioRol();
                        usuarioD = this.servicios.obtenerUsuario(usuario);
                        if (usuarioD.getIdUsuario().getIdUsuario() == 0L) {
                            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", "Error al guardar datos"));
                        } else {
                            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Información.", "La persona ya se encuentra registrada"));
                        }

                    }

                } else {
                    //mensaje flata correo o contraseña
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", "Falta correo o contraseña"));
                }
            } else {
                //mensaje falta informacion
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", "Falta información"));
            }
        } catch (Exception e) {
            log.info(e.getMessage());
        }

        return "#";
    }

    public String olvidoContraseña() {
        return "#";
    }

    public String habilitarIngreso() {
        this.ingresoExternosRendered = true;
        this.registroExternosRendered = false;
        this.loginRendered = false;
        return "#";
    }

    public String registroExterno() {
        this.loginRendered = false;
        this.registroExternosRendered = true;
        return "#";
    }

    public String volverInicioPrincipal() {
        this.loginRendered = true;
        this.registroExternosRendered = false;
        this.ingresoExternosRendered = false;
        return "#";
    }

    public String validarIngresoApp() {
        try {
            if (this.persona.getEmail() != null && this.persona.getContrasena() != null) {
                this.persona = this.servicios.validarPersonaExterna(persona);
                if (this.persona.getErrorUsuario().equalsIgnoreCase("Ninguna")) {
                    ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
                    ec.redirect(ec.getRequestContextPath() + "/faces/vistas/formularioSintomas.xhtml?tipo=usuario&id=" + this.persona.getIdUsuario() + "&idp=" + this.persona.getIdentificacion());
                } else {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", this.persona.getErrorUsuario()));
                }
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", "Faltan Datos"));
            }

            return "#";
        } catch (UnsupportedEncodingException ex) {
            log.info(ex.getMessage());
        } catch (IOException ex) {
            log.info(ex.getMessage());
        }
        return "#";
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public Usuario getPersona() {
        return persona;
    }

    public void setPersona(Usuario persona) {
        this.persona = persona;
    }

    public boolean isLoginRendered() {
        return loginRendered;
    }

    public void setLoginRendered(boolean loginRendered) {
        this.loginRendered = loginRendered;
    }

    public String getURL() {
        return URL;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }

    public boolean isRegistroExternosRendered() {
        return registroExternosRendered;
    }

    public void setRegistroExternosRendered(boolean registroExternosRendered) {
        this.registroExternosRendered = registroExternosRendered;
    }

    public boolean isIngresoExternosRendered() {
        return ingresoExternosRendered;
    }

    public void setIngresoExternosRendered(boolean ingresoExternosRendered) {
        this.ingresoExternosRendered = ingresoExternosRendered;
    }

    public Usuario getPersonaTemp() {
        return personaTemp;
    }

    public void setPersonaTemp(Usuario personaTemp) {
        this.personaTemp = personaTemp;
    }

}
